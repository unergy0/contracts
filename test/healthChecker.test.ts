import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { deployFixture } from "./fixtures";
import { expect } from "chai";
import { formatAccessControlError } from "../utils/helpers";
import {
  CLAIM_SERVICE_SIGNER_ADDRESS,
  HEALTH_CHECKER_ROLE,
} from "../utils/constants";
import { ethers } from "hardhat";
import { wallet } from "../utils/wallet";

describe("Health Checker", () => {
  describe("Health Check", () => {
    it("Should failed if not authorized", async () => {
      const { healthChecker, user1 } = await loadFixture(deployFixture);

      await expect(
        healthChecker.connect(user1).healthCheck()
      ).to.be.revertedWith(
        formatAccessControlError(user1.address, HEALTH_CHECKER_ROLE)
      );
    });

    it("Should emit the `HealthCheck` event", async () => {
      const { healthChecker } = await loadFixture(deployFixture);

      const healthCheckerAdmin = await ethers.getImpersonatedSigner(
        CLAIM_SERVICE_SIGNER_ADDRESS
      );

      await expect(
        healthChecker.connect(healthCheckerAdmin).healthCheck()
      ).to.emit(healthChecker, "HealthCheck");
    });
  });

  describe("Access", () => {
    it("Should grant the `HEALTH_CHECKER_ROLE` to the `CLAIM_SERVICE_SIGNER_ADDRESS`", async () => {
      const { healthChecker } = await loadFixture(deployFixture);

      const healthCheckerAdmin = await ethers.getImpersonatedSigner(
        CLAIM_SERVICE_SIGNER_ADDRESS
      );

      expect(
        await healthChecker.hasRole(
          HEALTH_CHECKER_ROLE,
          healthCheckerAdmin.address
        )
      ).to.be.true;
    });

    it("Should allow the `DEFAULT_ADMIN_ROLE` to grant the `CLAIM_SERVICE_SIGNER_ADDRESS` role to another account", async () => {
      const { healthChecker } = await loadFixture(deployFixture);

      const randomUser = await wallet.getSignerWithBalance();

      await healthChecker.grantRole(HEALTH_CHECKER_ROLE, randomUser.address);

      const hasRole = await healthChecker.hasRole(
        HEALTH_CHECKER_ROLE,
        randomUser.address
      );

      expect(hasRole).to.be.true;
    });

    it("Should allow the `DEFAULT_ADMIN_ROLE` to revoke the `CLAIM_SERVICE_SIGNER_ADDRESS` role to another account", async () => {
      const { healthChecker } = await loadFixture(deployFixture);

      const randomUser = await wallet.getSignerWithBalance();

      await healthChecker.grantRole(HEALTH_CHECKER_ROLE, randomUser.address);

      let hasRole = await healthChecker.hasRole(
        HEALTH_CHECKER_ROLE,
        randomUser.address
      );

      expect(hasRole).to.be.true;

      await healthChecker.revokeRole(HEALTH_CHECKER_ROLE, randomUser.address);

      hasRole = await healthChecker.hasRole(
        HEALTH_CHECKER_ROLE,
        randomUser.address
      );

      expect(hasRole).to.be.false;
    });
  });
});
