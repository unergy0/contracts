import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { ethers } from "hardhat";
import { expect } from "chai";

import { deployFixture, createProjectFixture } from "./fixtures";

describe("UnergyData Contract", () => {
  describe("Transfer Ownership", function () {
    it("Should transfer the ownership of UnergyData contract", async function () {
      const { unergyData, notAdmin } = await loadFixture(deployFixture);

      await unergyData.transferOwnership(notAdmin.address);

      await unergyData.connect(notAdmin).acceptOwnership();

      expect(await unergyData.owner()).to.deep.equal(notAdmin.address);
    });
  });

  describe("Pause/UnPause", function () {
    it("Should revert if the contract is paused", async function () {
      const { unergyData, uWattToken, notAdmin, user1, meter1 } =
        await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      //pause
      await unergyData.pause();
      expect(await unergyData.paused()).to.equal(true);

      await expect(
        unergyData.setUWattAddr(uWattToken.address)
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        unergyData.setDepreciationBalance(project1.address)
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        unergyData.setAccEnergyByMeter(project1.address, meter1.address, 0)
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        unergyData.generatePurchaseTicket(
          project1.address,
          0,
          0,
          notAdmin.address
        )
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        unergyData.redeemTicket(project1.address, notAdmin.address)
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        unergyData.changePurchaseTicketUsed(project1.address, notAdmin.address)
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        unergyData.setExternalHolderAddress(
          user1.address,
          project1.address,
          false
        )
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        unergyData.transferOwnership(user1.address)
      ).to.be.revertedWith("Pausable: paused");

      await expect(unergyData.pause()).to.be.revertedWith("Pausable: paused");

      await unergyData.unpause();

      expect(await unergyData.paused()).to.equal(false);
    });
  });

  describe("Non Authorized Callers", function () {
    it("Should revert if the caller is not authorized", async function () {
      const {
        unergyData,
        uWattToken,
        permissionGranter,
        notAdmin,
        user1,
        meter1,
      } = await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      await expect(
        unergyData.connect(notAdmin).setUWattAddr(uWattToken.address)
      ).to.be.revertedWithCustomError(
        permissionGranter,
        "NotAuthorizedToCallFunction"
      );

      await expect(
        unergyData.connect(notAdmin).setDepreciationBalance(project1.address)
      ).to.be.revertedWithCustomError(
        permissionGranter,
        "NotAuthorizedToCallFunction"
      );

      await expect(
        unergyData
          .connect(notAdmin)
          .setAccEnergyByMeter(project1.address, meter1.address, 0)
      ).to.be.revertedWithCustomError(
        permissionGranter,
        "NotAuthorizedToCallFunction"
      );

      await expect(
        unergyData
          .connect(notAdmin)
          .generatePurchaseTicket(project1.address, 0, 0, notAdmin.address)
      ).to.be.revertedWithCustomError(
        permissionGranter,
        "NotAuthorizedToCallFunction"
      );

      await expect(
        unergyData
          .connect(notAdmin)
          .changePurchaseTicketUsed(project1.address, notAdmin.address)
      ).to.be.revertedWithCustomError(
        permissionGranter,
        "NotAuthorizedToCallFunction"
      );

      await expect(
        unergyData
          .connect(notAdmin)
          .setExternalHolderAddress(user1.address, project1.address, false)
      ).to.be.revertedWithCustomError(
        permissionGranter,
        "NotAuthorizedToCallFunction"
      );

      await expect(
        unergyData
          .connect(notAdmin)
          .setPermissionGranterAddr(permissionGranter.address)
      )
        .to.be.revertedWithCustomError(
          unergyData,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("setPermissionGranterAddr", notAdmin.address);

      await expect(
        unergyData.connect(notAdmin).transferOwnership(user1.address)
      ).to.be.revertedWith("Ownable: caller is not the owner");

      await expect(unergyData.connect(notAdmin).pause()).to.be.revertedWith(
        "Ownable: caller is not the owner"
      );
    });
  });

  describe("Contract Addresses setup", function () {
    it("Should revert if an admin try to set address 0 as a contract address", async () => {
      const { admin, unergyData, multiSignPermission } = await loadFixture(
        deployFixture
      );

      await multiSignPermission.setPermission(
        admin.address,
        unergyData.address,
        "setPermissionGranterAddr"
      );

      await expect(
        unergyData.setUWattAddr(ethers.constants.AddressZero)
      ).to.be.revertedWithCustomError(unergyData, "ZeroAddressNotAllowed");

      await expect(
        unergyData.setPermissionGranterAddr(ethers.constants.AddressZero)
      ).to.be.revertedWithCustomError(unergyData, "ZeroAddressNotAllowed");

      await expect(
        unergyData.transferOwnership(ethers.constants.AddressZero)
      ).to.be.revertedWithCustomError(unergyData, "ZeroAddressNotAllowed");
    });
  });
});
