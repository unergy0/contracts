import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { ethers } from "hardhat";
import { expect } from "chai";

import { deployFixture, createProjectFixture } from "./fixtures";
import { ProjectState, Signature } from "../types/contracts.types";
import { getAddressFromReceipt, parseProject } from "../utils";
import { parseMilestone, parseUnits } from "../utils/helpers";
import { PROTOCOL_CONTRACT_ROLE, factories } from "../utils/constants";
import {
  MilestoneStruct,
  ProjectInputStruct,
} from "../typechain-types/contracts/ProjectsManager";
import { ERC20Project } from "../typechain-types";
import { MultiSignPermission } from "../utils/multiSignPermissions";
import { assertEnabled } from "@thirdweb-dev/sdk";

describe("Projects Manager Contract", function () {
  it("Should deploy the contract", async function () {
    const { projectsManager } = await loadFixture(deployFixture);
    expect(projectsManager.address).to.not.equal(0);
  });

  it("Should set the rigth permission granter address", async function () {
    const { projectsManager, permissionGranter } = await loadFixture(
      deployFixture
    );

    const permissionGranterAddress =
      await projectsManager.getPermissionGranterAddress();

    expect(permissionGranterAddress).to.equal(permissionGranter.address);
  });

  it("Shoudn't allow not admin to set permission granter address", async function () {
    const { projectsManager, permissionGranter, notAdmin } = await loadFixture(
      deployFixture
    );

    await expect(
      projectsManager
        .connect(notAdmin)
        .setPermissionGranterAddr(permissionGranter.address)
    )
      .to.be.revertedWithCustomError(
        projectsManager,
        "NotAuthorizedToCallFunction"
      )
      .withArgs("setPermissionGranterAddr", notAdmin.address);
  });

  it("Should revert if an admin try to set address 0 as a contract address", async () => {
    const { admin, projectsManager, multiSignPermission } = await loadFixture(
      deployFixture
    );

    await multiSignPermission.setPermission(
      admin.address,
      projectsManager.address,
      "setPermissionGranterAddr"
    );

    await expect(
      projectsManager.setPermissionGranterAddr(ethers.constants.AddressZero)
    ).to.be.revertedWithCustomError(projectsManager, "ZeroAddressNotAllowed");

    await expect(
      projectsManager.transferOwnership(ethers.constants.AddressZero)
    ).to.be.revertedWithCustomError(projectsManager, "ZeroAddressNotAllowed");
  });

  it("Should revert if the caller is not the owner", async () => {
    const { projectsManager, notAdmin } = await loadFixture(deployFixture);

    const randomAddress = ethers.Wallet.createRandom().address;

    await expect(projectsManager.connect(notAdmin).pause()).to.be.revertedWith(
      "Ownable: caller is not the owner"
    );

    await expect(
      projectsManager.connect(notAdmin).setPermissionGranterAddr(randomAddress)
    )
      .to.be.revertedWithCustomError(
        projectsManager,
        "NotAuthorizedToCallFunction"
      )
      .withArgs("setPermissionGranterAddr", notAdmin.address);

    await expect(
      projectsManager.connect(notAdmin).transferOwnership(randomAddress)
    ).to.be.revertedWith("Ownable: caller is not the owner");
  });

  describe("Create Project", function () {
    it("Shouldn't allow not admin to create a project", async function () {
      const {
        projectsManager,
        permissionGranter,
        notAdmin,
        assetManager,
        assetManagerFeePercentage,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      await expect(
        projectsManager
          .connect(notAdmin)
          .createProject(defaultProjectInput, "Project 1", "PRJ1")
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("createProject", notAdmin.address);
    });

    it("Should allow admin to create a project", async function () {
      const {
        projectsManager,
        admin,
        assetManager,
        assetManagerFeePercentage,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      await projectsManager
        .connect(admin)
        .createProject(defaultProjectInput, "Project 1", "PRJ1");
    });

    it("Should create project with the rigth values", async function () {
      const { projectsManager, assetManager, assetManagerFeePercentage } =
        await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 7";
      const PROJECT_SYMBOL = "PRJ7";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const project = await projectsManager.getProject(
        createdProjectAddress[0]
      );
      const projectContract = await ethers.getContractAt(
        "ERC20Project",
        createdProjectAddress[0]
      );

      expect(await projectContract.name()).to.equal(PROJECT_NAME);
      expect(await projectContract.symbol()).to.equal(PROJECT_SYMBOL);

      expect(parseProject(project)).to.deep.equal({
        id: 6,
        maintenancePercentage: defaultProjectInput.maintenancePercentage,
        initialProjectValue: defaultProjectInput.initialProjectValue,
        currentProjectValue: defaultProjectInput.currentProjectValue,
        swapFactor: defaultProjectInput.swapFactor,
        state: ProjectState.FUNDING,
        addr: createdProjectAddress[0],
        adminAddr: defaultProjectInput.adminAddr,
        installerAddr: defaultProjectInput.installerAddr,
        usdDepreciated: 0,
        pWattsSupply: defaultProjectInput.totalPWatts,
        signatures: { isSignedByInstaller: false, isSignedByOriginator: false },
        originator: defaultProjectInput.originator,
        originatorFee: defaultProjectInput.originatorFee,
        stableAddr: defaultProjectInput.stableAddr,
      });
    });

    it("Should revert if a second attempt is made to configure the project", async function () {
      const {
        unergyLogicReserve,
        unergyEvent,
        admin,
        projectsManager,
        assetManager,
        assetManagerFeePercentage,
        user1,
        projectOriginator1,
        multiSignPermission,
        permissionGranter,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress: any = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const projectAddress = createdProjectAddress[0];

      await permissionGranter.grantRole(PROTOCOL_CONTRACT_ROLE, projectAddress);

      await multiSignPermission.setPermission(
        projectsManager.address,
        projectAddress,
        "mint"
      );

      await multiSignPermission.setPermission(
        projectsManager.address,
        projectAddress,
        "approveSwap"
      );

      await multiSignPermission.setPermission(
        unergyEvent.address,
        projectAddress,
        "approveSwap"
      );

      await multiSignPermission.setPermission(
        projectAddress,
        unergyEvent.address,
        "beforeTransferReceipt"
      );

      await multiSignPermission.setPermission(
        projectAddress,
        unergyEvent.address,
        "afterTransferReceipt"
      );

      await multiSignPermission.setPermission(
        admin.address,
        projectAddress,
        "approveSwap"
      );
      await multiSignPermission.setPermission(
        unergyLogicReserve.address,
        projectAddress,
        "approveSwap"
      );

      await projectsManager.configureProject(projectAddress);

      await expect(
        projectsManager.connect(admin).configureProject(projectAddress)
      )
        .to.be.revertedWithCustomError(
          projectsManager,
          "ProjectAlreadyConfigured"
        )
        .withArgs(projectAddress);
    });
  });

  describe("Project Holders", function () {
    it("Should not allow to add holder to a not admin", async function () {
      const {
        projectsManager,
        permissionGranter,
        notAdmin,
        user2,
        assetManager,
        assetManagerFeePercentage,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      await expect(
        projectsManager
          .connect(notAdmin)
          .addProjectHolder(createdProjectAddress[0], user2.address)
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("updateProjectRelatedProperties", notAdmin.address);
    });

    it("Should not allow to add holder to a project that doesn't exist", async function () {
      const { projectsManager, user1 } = await loadFixture(deployFixture);

      const UNEXISTING_PROJECT_ADDRESS = ethers.Wallet.createRandom().address;

      await expect(
        projectsManager.addProjectHolder(
          UNEXISTING_PROJECT_ADDRESS,
          user1.address
        )
      )
        .to.be.revertedWithCustomError(projectsManager, "ProjectDoesNotExist")
        .withArgs(UNEXISTING_PROJECT_ADDRESS);
    });

    it("Should add a project holder with the correct values", async function () {
      const {
        unergyEvent,
        admin,
        unergyLogicReserve,
        projectsManager,
        user1,
        assetManager,
        assetManagerFeePercentage,
        project1Admin,
        projectOriginator1,
        installerProject1,
        multiSignPermission,
        permissionGranter,
        STABLE_COIN_DECIMALS,
        stableCoin,
      } = await loadFixture(deployFixture);

      const PROJECTS_DECIMALS = 18;
      const defaultProjectInput: ProjectInputStruct = {
        maintenancePercentage: parseUnits(10, PROJECTS_DECIMALS),
        initialProjectValue: parseUnits(120000, STABLE_COIN_DECIMALS), // 10%
        currentProjectValue: parseUnits(120000, STABLE_COIN_DECIMALS), // 120k
        swapFactor: parseUnits(1, PROJECTS_DECIMALS), // 100% (2 decimals)
        totalPWatts: parseUnits(120000, PROJECTS_DECIMALS), // 120k pWatts (2 decimals)
        adminAddr: project1Admin.address,
        installerAddr: installerProject1.address,
        originator: projectOriginator1.address,
        originatorFee: parseUnits(10_000, PROJECTS_DECIMALS), // 10k pWatts (2 decimals)
        stableAddr: stableCoin.address,
        assetManagerAddr: assetManager.address,
        assetManagerFeePercentage: assetManagerFeePercentage,
      };

      const PROJECT_NAME = "Project X";
      const PROJECT_SYMBOL = "PRJX";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress: any = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const projectAddress = createdProjectAddress[0];

      await permissionGranter.grantRole(PROTOCOL_CONTRACT_ROLE, projectAddress);

      await multiSignPermission.setPermission(
        projectsManager.address,
        projectAddress,
        "mint"
      );

      await multiSignPermission.setPermission(
        projectsManager.address,
        projectAddress,
        "approveSwap"
      );

      await multiSignPermission.setPermission(
        unergyEvent.address,
        projectAddress,
        "approveSwap"
      );

      await multiSignPermission.setPermission(
        projectAddress,
        unergyEvent.address,
        "beforeTransferReceipt"
      );

      await multiSignPermission.setPermission(
        projectAddress,
        unergyEvent.address,
        "afterTransferReceipt"
      );

      await multiSignPermission.setPermission(
        admin.address,
        projectAddress,
        "approveSwap"
      );
      await multiSignPermission.setPermission(
        unergyLogicReserve.address,
        projectAddress,
        "approveSwap"
      );

      await projectsManager.configureProject(projectAddress);

      await projectsManager.addProjectHolder(projectAddress, user1.address);

      const project = await projectsManager.getProject(projectAddress);

      const projectHolders = await projectsManager.getProjectHolders(
        project.addr
      );

      expect(projectHolders).to.deep.equal([
        defaultProjectInput.adminAddr,
        projectOriginator1.address,
        user1.address,
      ]);
    });

    it("Should revert if trying to get project holders when does no exist", async function () {
      const { projectsManager } = await loadFixture(deployFixture);

      const randomAddress = ethers.Wallet.createRandom().address;

      await expect(projectsManager.getProjectHolders(randomAddress))
        .to.be.revertedWithCustomError(projectsManager, "ProjectDoesNotExist")
        .withArgs(randomAddress);
    });
  });

  describe("Update Project", function () {
    it("Should allow to update a project", async function () {
      const {
        projectsManager,
        assetManager,
        assetManagerFeePercentage,
        STABLE_COIN_DECIMALS,
        multiSignPermission,
        unergyEvent,
        permissionGranter,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);
      const { ERC20ProjectFactory } = await factories;

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      )[0];

      const project = parseProject(
        await projectsManager.getProject(createdProjectAddress)
      );

      // * -------- Permissions required to configure the project ----------
      await permissionGranter.grantRole(
        PROTOCOL_CONTRACT_ROLE,
        createdProjectAddress
      );

      await multiSignPermission.setPermission(
        projectsManager.address,
        createdProjectAddress,
        "mint"
      );
      await multiSignPermission.setPermission(
        projectsManager.address,
        createdProjectAddress,
        "approveSwap"
      );
      await multiSignPermission.setPermission(
        createdProjectAddress,
        unergyEvent.address,
        "beforeTransferReceipt"
      );
      await multiSignPermission.setPermission(
        createdProjectAddress,
        unergyEvent.address,
        "afterTransferReceipt"
      );
      await multiSignPermission.setPermission(
        unergyEvent.address,
        createdProjectAddress,
        "approveSwap"
      );

      await projectsManager.configureProject(createdProjectAddress);
      // * -----------------------------------------------------------------

      const project1 = ERC20ProjectFactory.attach(
        createdProjectAddress
      ) as ERC20Project;

      const project1Decimals = await project1.decimals();
      const projectSupply = await project1.totalSupply();

      const newMaintenancePercentage = parseUnits(15, 18);
      const newProjectValue = parseUnits(122000, STABLE_COIN_DECIMALS);
      const newSwapFactor = parseUnits(90, 18);
      const newState = ProjectState.CANCELLED;
      const newAdminAddr = ethers.Wallet.createRandom().address;
      const newInstallerAddr = ethers.Wallet.createRandom().address;
      const newUsdDepreciated = parseUnits(1000, STABLE_COIN_DECIMALS);
      const newPWattsSupply = parseUnits(1_000_000, project1Decimals);
      const newOriginator = ethers.Wallet.createRandom().address;
      const newOriginatorFee = projectSupply.div(2);
      const newStableAddr = ethers.Wallet.createRandom().address;

      await projectsManager.updateProject(project.addr, {
        ...project,
        maintenancePercentage: newMaintenancePercentage,
        currentProjectValue: newProjectValue,
        swapFactor: newSwapFactor,
        state: newState,
        adminAddr: newAdminAddr,
        installerAddr: newInstallerAddr,
        usdDepreciated: newUsdDepreciated,
        pWattsSupply: newPWattsSupply,
        originator: newOriginator,
        originatorFee: newOriginatorFee,
        stableAddr: newStableAddr,
      });

      const updatedProject = parseProject(
        await projectsManager.getProject(createdProjectAddress)
      );

      expect({
        maintenancePercentage: updatedProject.maintenancePercentage,
        currentProjectValue: updatedProject.currentProjectValue,
        swapFactor: updatedProject.swapFactor,
        state: updatedProject.state,
        adminAddr: updatedProject.adminAddr,
        installerAddr: updatedProject.installerAddr,
        usdDepreciated: updatedProject.usdDepreciated,
        pWattsSupply: updatedProject.pWattsSupply,
        originator: updatedProject.originator,
        originatorFee: updatedProject.originatorFee,
        stableAddr: updatedProject.stableAddr,
      }).to.deep.equal({
        maintenancePercentage: newMaintenancePercentage,
        currentProjectValue: newProjectValue,
        swapFactor: newSwapFactor,
        state: newState,
        adminAddr: newAdminAddr,
        installerAddr: newInstallerAddr,
        usdDepreciated: newUsdDepreciated,
        pWattsSupply: newPWattsSupply,
        originator: newOriginator,
        originatorFee: newOriginatorFee,
        stableAddr: newStableAddr,
      });
    });

    it("Shouldn't allow to update project if the caller is not authorized", async function () {
      const {
        projectsManager,
        notAdmin,
        permissionGranter,
        assetManager,
        assetManagerFeePercentage,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const project = parseProject(
        await projectsManager.getProject(createdProjectAddress[0])
      );

      await expect(
        projectsManager
          .connect(notAdmin)
          .updateProject(createdProjectAddress[0], {
            ...project,
            state: ProjectState.CANCELLED,
          })
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("updateProjectRelatedProperties", notAdmin.address);
    });

    it("Shouldn't allow to update project's state if the project does no exist", async function () {
      const { projectsManager, assetManager, assetManagerFeePercentage } =
        await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const project = parseProject(
        await projectsManager.getProject(createdProjectAddress[0])
      );

      const NOT_EXISTING_PROJECT_ADDRESS =
        "0x1111111111111111111111111111111111111111";

      await expect(
        projectsManager.updateProject(NOT_EXISTING_PROJECT_ADDRESS, {
          ...project,
          state: ProjectState.CANCELLED,
        })
      )
        .to.be.revertedWithCustomError(projectsManager, "ProjectDoesNotExist")
        .withArgs(NOT_EXISTING_PROJECT_ADDRESS);
    });

    it("Shouldn't allow to set project's state if the caller is not authorized", async function () {
      const {
        projectsManager,
        notAdmin,
        permissionGranter,
        assetManager,
        assetManagerFeePercentage,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      await expect(
        projectsManager
          .connect(notAdmin)
          .setProjectState(createdProjectAddress[0], ProjectState.CANCELLED)
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("updateProjectRelatedProperties", notAdmin.address);
    });

    it("Should allow to set project's state", async function () {
      const { projectsManager, assetManager, assetManagerFeePercentage } =
        await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      await projectsManager.setProjectState(
        createdProjectAddress[0],
        ProjectState.CANCELLED
      );

      const project = parseProject(
        await projectsManager.getProject(createdProjectAddress[0])
      );

      expect(project.state).to.deep.equal(ProjectState.CANCELLED);
    });
  });

  describe("Milestones", function () {
    it("Shouldn't allow to create a milestone if the caller is not authorized", async function () {
      const {
        projectsManager,
        notAdmin,
        permissionGranter,
        assetManager,
        assetManagerFeePercentage,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const MILESTONE_NAME = "Milestone 1";
      const MILESTONE_WEIGHT = 1;

      await expect(
        projectsManager
          .connect(notAdmin)
          .addProjectMilestone(
            createdProjectAddress[0],
            MILESTONE_NAME,
            MILESTONE_WEIGHT
          )
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("updateProjectRelatedProperties", notAdmin.address);
    });

    it("Should allow to create a milestone", async function () {
      const { projectsManager, assetManager, assetManagerFeePercentage } =
        await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const MILESTONE_NAME = "Milestone 1";
      const MILESTONE_WEIGHT = 1;

      await projectsManager.addProjectMilestone(
        createdProjectAddress[0],
        MILESTONE_NAME,
        MILESTONE_WEIGHT
      );

      const projectMilestones = await projectsManager.getProjectMilestones(
        createdProjectAddress[0]
      );

      expect(projectMilestones.length).to.equal(1);
      expect(parseMilestone(projectMilestones[0])).to.deep.equal({
        name: MILESTONE_NAME,
        weight: MILESTONE_WEIGHT,
        isReached: false,
        wasSignedByInstaller: false,
        wasSignedByOriginator: false,
      });
    });

    it("Should allow to update a milestone", async function () {
      const { projectsManager, assetManager, assetManagerFeePercentage } =
        await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const MILESTONE_1_NAME = "Milestone 1";
      const MILESTONE_1_WEIGHT = 1;

      await projectsManager.addProjectMilestone(
        createdProjectAddress[0],
        MILESTONE_1_NAME,
        MILESTONE_1_WEIGHT
      );

      const projectMilestonesBefore =
        await projectsManager.getProjectMilestones(createdProjectAddress[0]);

      await projectsManager.updateMilestoneAtIndex(
        createdProjectAddress[0],
        0,
        {
          ...projectMilestonesBefore[0],
          isReached: true,
        }
      );

      const projectMilestonesAfter = await projectsManager.getProjectMilestones(
        createdProjectAddress[0]
      );

      expect(parseMilestone(projectMilestonesBefore[0])).to.deep.equal({
        name: MILESTONE_1_NAME,
        weight: MILESTONE_1_WEIGHT,
        isReached: false,
        wasSignedByInstaller: false,
        wasSignedByOriginator: false,
      });
      expect(parseMilestone(projectMilestonesAfter[0])).to.deep.equal({
        name: MILESTONE_1_NAME,
        weight: MILESTONE_1_WEIGHT,
        isReached: true, // * <--- changed
        wasSignedByInstaller: false,
        wasSignedByOriginator: false,
      });
    });

    it("Should allow to update milestones", async () => {
      const { projectsManager } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const newMilestones: MilestoneStruct[] = [
        {
          name: "Desarrollo de ingeniería de detalle del proyecto",
          weight: 10,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
        {
          name: "Anticipo de paneles, inversores y estructuras",
          weight: 20,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
        {
          name: "Equipos en sitio",
          weight: 20,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
        {
          name: "Realizar instalación del proyecto",
          weight: 30,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
        {
          name: "Obtener certificaciones y pólizas",
          weight: 10,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
        {
          name: "Realizar proceso con Operador de Red",
          weight: 10,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
      ];

      await projectsManager.updateProjectMilestones(
        project1.address,
        newMilestones
      );
    });

    it("Should revert for milestones weights greater than 100", async () => {
      const { projectsManager } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const newMilestones: MilestoneStruct[] = [
        {
          name: "Desarrollo de ingeniería de detalle del proyecto",
          weight: 20,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
        {
          name: "Anticipo de paneles, inversores y estructuras",
          weight: 20,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
        {
          name: "Equipos en sitio",
          weight: 20,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
        {
          name: "Realizar instalación del proyecto",
          weight: 30,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
        {
          name: "Obtener certificaciones y pólizas",
          weight: 10,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
        {
          name: "Realizar proceso con Operador de Red",
          weight: 10,
          isReached: false,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        },
      ];

      await expect(
        projectsManager.updateProjectMilestones(project1.address, newMilestones)
      ).to.be.revertedWithCustomError(
        projectsManager,
        "MilestoneWeightsExceeded"
      );
    });
  });

  describe("Signatures", function () {
    it("Shouldn't allow to sign a project if the caller is not authorized", async function () {
      const {
        permissionGranter,
        projectsManager,
        notAdmin,
        assetManager,
        assetManagerFeePercentage,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      await expect(
        projectsManager
          .connect(notAdmin)
          .setSignature(createdProjectAddress[0], Signature.INSTALLER)
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("setSignature", notAdmin.address);
    });

    it("Shouldn't allow to sign a project with an invalid signature", async function () {
      const {
        projectsManager,
        notAdmin,
        assetManager,
        assetManagerFeePercentage,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const INVALID_SIGNATURE = 2;

      await expect(
        projectsManager
          .connect(notAdmin)
          .setSignature(createdProjectAddress[0], INVALID_SIGNATURE)
      ).to.be.revertedWithoutReason();
    });

    it("Should allow to sign a project", async function () {
      const { projectsManager, assetManager, assetManagerFeePercentage } =
        await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const setSignatureTransaction = await projectsManager.setSignature(
        createdProjectAddress[0],
        Signature.INSTALLER
      );

      await expect(setSignatureTransaction)
        .to.emit(projectsManager, "SignatureSet")
        .withArgs(createdProjectAddress[0], Signature.INSTALLER);
    });
  });

  describe("ProjectManager - General Public Queries", function () {
    it("Should revert if the project doesn't exist", async function () {
      const { projectsManager } = await loadFixture(deployFixture);

      const NOT_EXISTING_PROJECT_ADDRESS = ethers.Wallet.createRandom().address;

      await expect(projectsManager.getProject(NOT_EXISTING_PROJECT_ADDRESS))
        .to.be.revertedWithCustomError(projectsManager, "ProjectDoesNotExist")
        .withArgs(NOT_EXISTING_PROJECT_ADDRESS);
    });

    it("Should allow get the pWatts project's supply", async function () {
      const { projectsManager, assetManager, assetManagerFeePercentage } =
        await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const pWattsProjectSupply = await projectsManager.getTotalPWatts(
        createdProjectAddress[0]
      );

      expect(pWattsProjectSupply).to.equal(defaultProjectInput.totalPWatts);
    });

    it("Should allow get the project state", async function () {
      const { projectsManager, assetManager, assetManagerFeePercentage } =
        await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const pWattsProjectSupply = await projectsManager.getProjectState(
        createdProjectAddress[0]
      );

      expect(pWattsProjectSupply).to.equal(ProjectState.FUNDING);
    });

    it("Should allow to get projects holders", async function () {
      const {
        unergyLogicReserve,
        unergyEvent,
        admin,
        projectsManager,
        assetManager,
        assetManagerFeePercentage,
        user1,
        projectOriginator1,
        multiSignPermission,
        permissionGranter,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress: any = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      const projectAddress = createdProjectAddress[0];

      await permissionGranter.grantRole(PROTOCOL_CONTRACT_ROLE, projectAddress);

      await multiSignPermission.setPermission(
        projectsManager.address,
        projectAddress,
        "mint"
      );

      await multiSignPermission.setPermission(
        projectsManager.address,
        projectAddress,
        "approveSwap"
      );

      await multiSignPermission.setPermission(
        unergyEvent.address,
        projectAddress,
        "approveSwap"
      );

      await multiSignPermission.setPermission(
        projectAddress,
        unergyEvent.address,
        "beforeTransferReceipt"
      );

      await multiSignPermission.setPermission(
        projectAddress,
        unergyEvent.address,
        "afterTransferReceipt"
      );

      await multiSignPermission.setPermission(
        admin.address,
        projectAddress,
        "approveSwap"
      );
      await multiSignPermission.setPermission(
        unergyLogicReserve.address,
        projectAddress,
        "approveSwap"
      );

      await projectsManager.configureProject(projectAddress);

      await projectsManager.addProjectHolder(
        createdProjectAddress[0],
        user1.address
      );

      const projectHolders = await projectsManager.getProjectHolders(
        createdProjectAddress[0]
      );

      expect(projectHolders).to.deep.equal([
        defaultProjectInput.adminAddr,
        projectOriginator1.address,
        user1.address,
      ]);
    });

    it("Should allow get projects signature", async function () {
      const { projectsManager, assetManager, assetManagerFeePercentage } =
        await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      const createProjectTx = await projectsManager.createProject(
        defaultProjectInput,
        PROJECT_NAME,
        PROJECT_SYMBOL
      );

      const createdProjectAddress = getAddressFromReceipt(
        await createProjectTx.wait()
      );

      // * Set installer signature
      await projectsManager.setSignature(
        createdProjectAddress[0],
        Signature.INSTALLER
      );

      let projectSignatures = await projectsManager.getSignatures(
        createdProjectAddress[0]
      );

      expect(projectSignatures).to.deep.equal([true, false]);

      // * Set uenrgy signature
      await projectsManager.setSignature(
        createdProjectAddress[0],
        Signature.ORIGINATOR
      );

      projectSignatures = await projectsManager.getSignatures(
        createdProjectAddress[0]
      );

      expect(projectSignatures).to.deep.equal([true, true]);
    });
  });

  describe("Project Manager Paused", function () {
    it("Should pause and unpause the contract", async () => {
      const { projectsManager } = await loadFixture(deployFixture);

      await projectsManager.pause();
      expect(await projectsManager.paused()).to.equal(true);

      await projectsManager.unpause();
      expect(await projectsManager.paused()).to.equal(false);
    });

    it("Should revert if the contract is paused", async function () {
      const {
        projectsManager,
        user1,
        assetManager,
        assetManagerFeePercentage,
      } = await loadFixture(deployFixture);
      const { defaultProjectInput } = await loadFixture(createProjectFixture);

      const PROJECT_NAME = "Project 1";
      const PROJECT_SYMBOL = "PRJ1";

      await projectsManager.pause();

      await expect(
        projectsManager.createProject(
          defaultProjectInput,
          PROJECT_NAME,
          PROJECT_SYMBOL
        )
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        projectsManager.updateProject(user1.address, {
          ...defaultProjectInput,
          id: 1,
          state: ProjectState.FUNDING,
          addr: user1.address,
          usdDepreciated: 0,
          pWattsSupply: 0,
          signatures: {
            isSignedByInstaller: false,
            isSignedByOriginator: false,
          },
        })
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        projectsManager.setProjectState(user1.address, ProjectState.FUNDING)
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        projectsManager.addProjectHolder(
          ethers.utils.getAddress(user1.address),
          user1.address
        )
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        projectsManager.addProjectMilestone(user1.address, "Name", 1)
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        projectsManager.updateMilestoneAtIndex(user1.address, 1, {
          name: "Name",
          isReached: false,
          weight: 1,
          wasSignedByInstaller: false,
          wasSignedByOriginator: false,
        })
      ).to.be.revertedWith("Pausable: paused");

      await expect(
        projectsManager.setSignature(user1.address, Signature.INSTALLER)
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if trying to pause when already paused", async function () {
      const { projectsManager } = await loadFixture(deployFixture);

      await projectsManager.pause();

      await expect(projectsManager.pause()).to.be.revertedWith(
        "Pausable: paused"
      );
    });

    it("Should revert if trying to unpause when already unpaused", async function () {
      const { projectsManager } = await loadFixture(deployFixture);

      expect(projectsManager.unpause()).to.be.revertedWith(
        "Pausable: not paused"
      );
    });
  });

  describe("Transfer ownership", () => {
    it("Should revert if the contract is paused", async () => {
      const { projectsManager } = await loadFixture(deployFixture);

      await projectsManager.pause();

      const randomAddress = ethers.Wallet.createRandom().address;

      await expect(
        projectsManager.transferOwnership(randomAddress)
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is not authorized", async () => {
      const { projectsManager, notAdmin } = await loadFixture(deployFixture);

      const randomAddress = ethers.Wallet.createRandom().address;

      await expect(
        projectsManager.connect(notAdmin).transferOwnership(randomAddress)
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it("Should revert address to set is the zero address", async () => {
      const { projectsManager } = await loadFixture(deployFixture);

      const zeroAddr = ethers.constants.AddressZero;

      await expect(
        projectsManager.transferOwnership(zeroAddr)
      ).to.be.revertedWithCustomError(projectsManager, "ZeroAddressNotAllowed");
    });

    it("Should transfer ownership correctly", async () => {
      const { projectsManager, notAdmin } = await loadFixture(deployFixture);

      await projectsManager.transferOwnership(notAdmin.address);

      await projectsManager.connect(notAdmin).acceptOwnership();

      expect(await projectsManager.owner()).to.be.equal(notAdmin.address);
    });
  });
});
