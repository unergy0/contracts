import { loadFixture, time } from "@nomicfoundation/hardhat-network-helpers";
import { ethers } from "hardhat";
import { expect } from "chai";

import { createProjectFixture, deployFixture } from "./fixtures";
import {
  toBn,
  parseUnits,
  transferAndApprove,
  calcMaxPWattThatCanBuyTheUnergyBuyer,
  parseProject,
} from "../utils";
import { ProjectState } from "../types/contracts.types";
import { ZERO_ADDRESS, externalDependenciesEnabled } from "../utils/constants";
import { parseEther, wait } from "../utils/helpers";

describe("UnergyLogicReserve Contract", () => {
  it("Initialize the contract with the correct value", async () => {
    const { unergyLogicReserve, permissionGranter, mantainer, unergyData } =
      await loadFixture(deployFixture);

    expect(await unergyLogicReserve.permissionGranter()).to.equal(
      permissionGranter.address
    );

    expect(await unergyData.maintainerAddress()).to.equal(mantainer.address);

    expect(await unergyLogicReserve.unergyData()).to.equal(unergyData.address);
  });

  // @todo remove this test. it is duplicated
  it("Should revert if an address not allowed try to set the permission granter address", async () => {
    const { unergyLogicReserve, permissionGranter, notAdmin } =
      await loadFixture(deployFixture);

    await expect(
      unergyLogicReserve
        .connect(notAdmin)
        .setPermissionGranterAddr(permissionGranter.address)
    )
      .to.be.revertedWithCustomError(
        unergyLogicReserve,
        "NotAuthorizedToCallFunction"
      )
      .withArgs("setPermissionGranterAddr", notAdmin.address);
  });

  it("Should revert if an admin try to set address 0 as permission granter", async () => {
    const { admin, unergyLogicReserve, multiSignPermission } =
      await loadFixture(deployFixture);

    await multiSignPermission.setPermission(
      admin.address,
      unergyLogicReserve.address,
      "setPermissionGranterAddr"
    );

    await expect(
      unergyLogicReserve
        .connect(admin)
        .setPermissionGranterAddr(ethers.constants.AddressZero)
    ).to.be.revertedWithCustomError(
      unergyLogicReserve,
      "ZeroAddressNotAllowed"
    );
  });

  describe("Pause/Unpause", () => {
    it("Should pause and unpause the contract", async () => {
      const { unergyLogicReserve } = await loadFixture(deployFixture);

      await unergyLogicReserve.pause();
      expect(await unergyLogicReserve.paused()).to.equal(true);

      await unergyLogicReserve.unpause();
      expect(await unergyLogicReserve.paused()).to.equal(false);
    });

    it("Should revert if not called by the owner address", async () => {
      const { unergyLogicReserve, user1 } = await loadFixture(deployFixture);

      await expect(
        unergyLogicReserve.connect(user1).pause()
      ).to.be.revertedWith("Ownable: caller is not the owner");
      expect(await unergyLogicReserve.paused()).to.equal(false);

      await expect(
        unergyLogicReserve.connect(user1).unpause()
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it("Should revert if the contract is already paused", async () => {
      const { unergyLogicReserve } = await loadFixture(deployFixture);

      await unergyLogicReserve.pause();

      expect(await unergyLogicReserve.paused()).to.equal(true);

      await expect(unergyLogicReserve.pause()).to.be.revertedWith(
        "Pausable: paused"
      );

      await unergyLogicReserve.unpause();

      expect(await unergyLogicReserve.paused()).to.equal(false);

      await expect(unergyLogicReserve.unpause()).to.be.revertedWith(
        "Pausable: not paused"
      );
    });
  });

  describe("Energy Report", () => {
    it("Should revert if the contract is paused", async () => {
      const { unergyLogicReserve } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await unergyLogicReserve.pause();
      expect(await unergyLogicReserve.paused()).to.equal(true);

      await expect(
        unergyLogicReserve.energyReport(project1.address, toBn(2000))
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is not authorized", async () => {
      const {
        unergyLogicReserve,
        notAdmin,
        permissionGranter,
        projectHelper,
        user1,
        user2,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await projectHelper.swapToken(
        project1,
        {
          users: [user1.address, user2.address],
          amounts: [],
        },
        undefined,
        undefined,
        true
      );

      await expect(
        unergyLogicReserve
          .connect(notAdmin)
          .energyReport(project1.address, toBn(2000))
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("energyReport", notAdmin.address);
    });

    it("Should not allow a meter to report energy to a project other than the one to which it is assigned", async () => {
      const { user1, user2, meter2, projectHelper, unergyLogicReserve } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await projectHelper.swapToken(
        project1,
        {
          users: [user1.address, user2.address],
          amounts: [],
        },
        undefined,
        undefined,
        true
      );

      await expect(
        unergyLogicReserve
          .connect(meter2)
          .energyReport(project1.address, toBn(2000))
      )
        .to.be.revertedWithCustomError(
          unergyLogicReserve,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("energyReport", meter2.address);
    });

    it("Should revert if last reported energy is greatter than current reporting energy", async () => {
      const { unergyLogicReserve, meter1, projectHelper, user1, user2 } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await projectHelper.swapToken(
        project1,
        {
          users: [user1.address, user2.address],
          amounts: [],
        },
        undefined,
        undefined,
        true
      );

      const FIRST_REPORT_ENERGY = toBn(2000);
      await unergyLogicReserve
        .connect(meter1)
        .energyReport(project1.address, FIRST_REPORT_ENERGY);

      const SECOND_REPORT_ENERGY = toBn(1000);
      await expect(
        unergyLogicReserve
          .connect(meter1)
          .energyReport(project1.address, SECOND_REPORT_ENERGY)
      ).to.be.revertedWithCustomError(
        unergyLogicReserve,
        "UnderRegisteredEnergyError"
      );
    });

    it("Should revert if the project is not in production stage", async () => {
      const { unergyLogicReserve, meter1 } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const FIRST_REPORT_ENERGY = toBn(2000);

      await expect(
        unergyLogicReserve
          .connect(meter1)
          .energyReport(project1.address, FIRST_REPORT_ENERGY)
      ).to.be.revertedWithCustomError(
        unergyLogicReserve,
        "ProjectNotInProduction"
      );
    });

    it("Should made the energy report from the meter and update the correct values", async () => {
      const {
        unergyLogicReserve,
        unergyData,
        meter1,
        projectHelper,
        user1,
        user2,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const lastAccumulatedEnergy = await unergyData.getAccEnergyByMeter(
        project1.address,
        meter1.address
      );

      await projectHelper.swapToken(
        project1,
        {
          users: [user1.address, user2.address],
          amounts: [],
        },
        undefined,
        undefined,
        true
      );

      const FIRST_REPORT_ENERGY = toBn(2000);
      const reportTx = await unergyLogicReserve
        .connect(meter1)
        .energyReport(project1.address, FIRST_REPORT_ENERGY);

      const currentAccumulatedEnergy = await unergyData.getAccEnergyByMeter(
        project1.address,
        meter1.address
      );

      const ENERGY_DELTA = FIRST_REPORT_ENERGY.sub(lastAccumulatedEnergy);

      expect(currentAccumulatedEnergy).to.equal(FIRST_REPORT_ENERGY);
      expect(reportTx)
        .to.emit(unergyLogicReserve, "EnergyReported")
        .withArgs([project1.address, FIRST_REPORT_ENERGY, ENERGY_DELTA]);
    });

    it("Should made the energy report from the meter and mint the corresponding Clean Energy Assests when one report already done", async () => {
      const {
        unergyLogicReserve,
        unergyData,
        meter1,
        cleanEnergyAsset,
        projectHelper,
        user1,
        user2,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await projectHelper.swapToken(
        project1,
        {
          users: [user1.address, user2.address],
          amounts: [],
        },
        undefined,
        undefined,
        true
      );

      const FIRST_REPORT_ENERGY = toBn(2000);
      await unergyLogicReserve
        .connect(meter1)
        .energyReport(project1.address, FIRST_REPORT_ENERGY);

      const SECOND_REPORT_ENERGY = toBn(4000);
      const secondReportTx = await unergyLogicReserve
        .connect(meter1)
        .energyReport(project1.address, SECOND_REPORT_ENERGY);

      const currentAccumulatedEnergy = await unergyData.getAccEnergyByMeter(
        project1.address,
        meter1.address
      );

      const afterSecondReportCleanEnergyAssetsProjectBalance =
        await cleanEnergyAsset.balanceOf(
          cleanEnergyAsset.address,
          await cleanEnergyAsset.tokenIdByProjectAddress(project1.address)
        );

      const ENERGY_DELTA = SECOND_REPORT_ENERGY.sub(FIRST_REPORT_ENERGY);

      expect(secondReportTx)
        .to.emit(unergyLogicReserve, "EnergyReported")
        .withArgs([project1.address, SECOND_REPORT_ENERGY, ENERGY_DELTA]);
      expect(currentAccumulatedEnergy).to.equal(SECOND_REPORT_ENERGY);
      expect(afterSecondReportCleanEnergyAssetsProjectBalance).to.equal(
        ENERGY_DELTA
      );
    });
  });

  describe("Invoice Report", () => {
    it("Should revert if the contract is paused", async () => {
      const { unergyLogicReserve } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await unergyLogicReserve.pause();
      expect(await unergyLogicReserve.paused()).to.equal(true);

      await expect(
        unergyLogicReserve.invoiceReport(
          project1.address,
          toBn(2000),
          toBn(2000),
          toBn(2000)
        )
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is not authorized", async () => {
      const { unergyLogicReserve, permissionGranter, notAdmin } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await expect(
        unergyLogicReserve
          .connect(notAdmin)
          .invoiceReport(project1.address, toBn(2000), toBn(2000), toBn(2000))
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("invoiceReport", notAdmin.address);
    });

    it("Should revert if the project is not in production nor installed stage", async () => {
      const { unergyLogicReserve } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const ENERGY_DELTA = toBn(1000);
      const ENERGY_TARIFF = toBn(200);
      const USD_DEPRECIATED = toBn(2);

      await expect(
        unergyLogicReserve.invoiceReport(
          project1.address,
          ENERGY_DELTA,
          ENERGY_TARIFF,
          USD_DEPRECIATED
        )
      )
        .to.be.revertedWithCustomError(
          unergyLogicReserve,
          "InvoiceReportNotAvailable"
        )
        .withArgs(ProjectState.FUNDING);
    });

    it("Should make an invoice report and emit the corresponding event", async () => {
      const {
        user1,
        user2,
        meter1,
        projectHelper,
        unergyLogicReserve,
        STABLE_COIN_DECIMALS,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await projectHelper.setInstallerSign(project1, {
        users: [user1.address, user2.address],
        amounts: [],
      });

      await unergyLogicReserve.swapToken(project1.address, toBn(5));

      const FIRST_REPORT_ENERGY = BigInt(10 ** STABLE_COIN_DECIMALS);
      await unergyLogicReserve
        .connect(meter1)
        .energyReport(project1.address, FIRST_REPORT_ENERGY);

      const SECOND_REPORT_ENERGY = BigInt(2 * 10 ** STABLE_COIN_DECIMALS);
      await unergyLogicReserve
        .connect(meter1)
        .energyReport(project1.address, SECOND_REPORT_ENERGY);

      const AMOUNT = toBn(2000);
      const ENERGY_DELTA = toBn(1000);
      const ENERGY_TARIFF = toBn(200);
      const USD_DEPRECIATED = toBn(2);

      const invoiceReportTx = await unergyLogicReserve.invoiceReport(
        project1.address,
        ENERGY_DELTA,
        ENERGY_TARIFF,
        USD_DEPRECIATED
      );

      expect(invoiceReportTx)
        .to.emit(unergyLogicReserve, "InvoiceReport")
        .withArgs([project1.address, ENERGY_DELTA, ENERGY_TARIFF, AMOUNT, 0]);
    });

    it("Should not report depreciation when project state is INSTALLED", async () => {
      const {
        user1,
        user2,
        unergyData,
        unergyLogicReserve,
        unergyBuyer,
        projectHelper,
        projectsManager,
        meter1,
      } = await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];
      await projectHelper.transferPWattToUsers(project1, {
        users,
        amounts: [],
      });

      const projectMilestones = await projectsManager.getProjectMilestones(
        project1.address
      );

      for (let i = 0; i < projectMilestones.length; i++) {
        await unergyBuyer.setInstallerSign(project1.address);
      }

      for (let i = 0; i < projectMilestones.length; i++) {
        await unergyBuyer.setOriginatorSign(project1.address, i);
      }

      await unergyLogicReserve.swapToken(project1.address, toBn(10));

      const FIRST_REPORT_ENERGY = toBn(10000);
      await unergyLogicReserve
        .connect(meter1)
        .energyReport(project1.address, FIRST_REPORT_ENERGY);

      const SECOND_REPORT_ENERGY = toBn(20000000000);
      await unergyLogicReserve
        .connect(meter1)
        .energyReport(project1.address, SECOND_REPORT_ENERGY);

      const ENERGY_DELTA = toBn(25);
      const ENERGY_TARIFF = toBn(100);
      const NEW_PROJECT_VALUE = parseProject(
        await projectsManager.getProject(project1.address)
      ).currentProjectValue;

      await unergyLogicReserve.invoiceReport(
        project1.address,
        ENERGY_DELTA,
        ENERGY_TARIFF,
        NEW_PROJECT_VALUE
      );

      const depreciationBalance = await unergyData.depreciationBalance();

      expect(depreciationBalance.toString()).to.equal("0");
    });

    it("Should not report depreciation when currentProjectValue increase", async function () {
      if (!externalDependenciesEnabled) this.skip();

      const {
        user1,
        user2,
        meter1,
        unergyData,
        unergyLogicReserve,
        projectHelper,
        projectsManager,
        STABLE_COIN_DECIMALS,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const usersProject1 = [user1.address, user2.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1
      );

      const projectStruct = await projectsManager.getProject(project1.address);
      const currentProjectValue = projectStruct.currentProjectValue;

      const newProjectValue =
        currentProjectValue.toBigInt() + BigInt(2 * 10 ** STABLE_COIN_DECIMALS);

      const ENERGY_DELTA = toBn(1000);
      const ENERGY_TARIFF = toBn(200);

      const depreciationBalancerBeforeSecondIR =
        await unergyData.depreciationBalance();

      await unergyLogicReserve.invoiceReport(
        project1.address,
        ENERGY_DELTA,
        ENERGY_TARIFF,
        newProjectValue
      );

      const projectStructAfterReport = await projectsManager.getProject(
        project1.address
      );
      const projectValueAfterReport =
        projectStructAfterReport.currentProjectValue;

      const depreciationBalanceAfterSecondIR =
        await unergyData.depreciationBalance();

      expect(projectValueAfterReport.toString()).to.equal(
        newProjectValue.toString()
      );

      expect(depreciationBalanceAfterSecondIR.toString()).to.equal(
        depreciationBalancerBeforeSecondIR.toString()
      );
    });

    it("Should make invoice report when currentProjectValue is equal to project present value", async () => {
      const {
        user1,
        user2,
        meter1,
        unergyLogicReserve,
        projectHelper,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const usersProject1 = [user1.address, user2.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      const projectStruct = await projectsManager.getProject(project1.address);
      const currentProjectValue = projectStruct.currentProjectValue;

      const newProjectValue = currentProjectValue.toBigInt();

      const ENERGY_DELTA = toBn(1000);
      const ENERGY_TARIFF = toBn(200);

      await unergyLogicReserve.invoiceReport(
        project1.address,
        ENERGY_DELTA,
        ENERGY_TARIFF,
        newProjectValue
      );

      const projectStructAfterIReport = await projectsManager.getProject(
        project1.address
      );
      const projectValueAfterIReport =
        projectStructAfterIReport.currentProjectValue;

      expect(projectValueAfterIReport.toString()).to.equal(
        newProjectValue.toString()
      );
    });

    it("Should make invoice report when newProjectValue is greater than project initial value", async () => {
      const {
        user1,
        user2,
        meter1,
        unergyLogicReserve,
        projectHelper,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const usersProject1 = [user1.address, user2.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      const newProjectValue = 0;

      const ENERGY_DELTA = toBn(1000);
      const ENERGY_TARIFF = toBn(200);

      await unergyLogicReserve.invoiceReport(
        project1.address,
        ENERGY_DELTA,
        ENERGY_TARIFF,
        newProjectValue
      );

      const projectStructAfterIReport = await projectsManager.getProject(
        project1.address
      );
      const projectValueAfterIReport =
        projectStructAfterIReport.currentProjectValue;

      expect(projectValueAfterIReport.toString()).to.equal("0");
    });

    it("Should make invoiceReport when occurs an customSwap and the newProjectValue is greater than project initial value", async () => {
      const {
        user1,
        user2,
        meter1,
        unergyData,
        unergyLogicReserve,
        projectHelper,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      //user2 marked as externall holder
      await unergyData.setExternalHolderAddress(
        user2.address,
        project1.address,
        true
      );

      const usersProject1 = [user1.address, user2.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      const projectStructAfterFirstInvoiceReport =
        await projectsManager.getProject(project1.address);

      const projectStruct = await projectsManager.getProject(project1.address);
      const initialProjectValue = projectStruct.initialProjectValue;

      //project appraisal
      const newProjectValue = initialProjectValue.add(BigInt(2000000));

      const ENERGY_DELTA = toBn(1000);
      const ENERGY_TARIFF = toBn(200);

      await unergyLogicReserve.invoiceReport(
        project1.address,
        ENERGY_DELTA,
        ENERGY_TARIFF,
        newProjectValue
      );

      const project1Struct = await projectsManager.getProject(project1.address);
      const swapFactor = project1Struct.swapFactor;

      //custom Swap
      const newSwapFactor = swapFactor.div(2);

      await unergyLogicReserve.customSwap(
        project1.address,
        user2.address,
        newSwapFactor
      );

      //new invoiceReport with all project depreciated
      await unergyLogicReserve.invoiceReport(
        project1.address,
        ENERGY_DELTA,
        ENERGY_TARIFF,
        0
      );

      const projectStructAfterIReport = await projectsManager.getProject(
        project1.address
      );
      const projectValueAfterIReport =
        projectStructAfterIReport.currentProjectValue;

      expect(projectValueAfterIReport.toString()).to.equal("0");
    });
  });

  describe("pWatts Transfer", () => {
    it("Should revert if the contract is paused", async () => {
      const { unergyLogicReserve } = await loadFixture(deployFixture);

      await unergyLogicReserve.pause();

      await expect(
        unergyLogicReserve.pWattsTransfer(
          ethers.constants.AddressZero,
          ethers.constants.AddressZero,
          toBn(2000)
        )
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is not authorized", async () => {
      const { unergyLogicReserve, permissionGranter, notAdmin } =
        await loadFixture(deployFixture);

      expect(
        unergyLogicReserve
          .connect(notAdmin)
          .pWattsTransfer(
            ethers.constants.AddressZero,
            ethers.constants.AddressZero,
            toBn(2000)
          )
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("pWattsTransfer", notAdmin.address);
    });

    it("Should revert if the `butTicket` has been used", async () => {
      const { unergyLogicReserve, unergyData, STABLE_COIN_DECIMALS, user1 } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATT_PRICE = BigInt(10 ** STABLE_COIN_DECIMALS);
      const LIFE_TIME = 1000;

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      const PWATTS_TO_TRANSFER = toBn(2000);

      await unergyLogicReserve.pWattsTransfer(
        project1.address,
        user1.address,
        PWATTS_TO_TRANSFER
      );

      await expect(
        unergyLogicReserve.pWattsTransfer(
          project1.address,
          user1.address,
          PWATTS_TO_TRANSFER
        )
      ).to.be.revertedWithCustomError(unergyLogicReserve, "TicketUsed");
    });

    it("Should revert if the `butTicket` has expired", async () => {
      const { unergyLogicReserve, unergyData, STABLE_COIN_DECIMALS, user1 } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATT_PRICE = BigInt(10 ** STABLE_COIN_DECIMALS);
      const LIFE_TIME = 1000;

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      // Set the next block.timestamp to be after the ticket's lifetime
      await time.increase(LIFE_TIME + 2);

      const PWATTS_TO_TRANSFER = toBn(2000);
      await expect(
        unergyLogicReserve.pWattsTransfer(
          project1.address,
          user1.address,
          PWATTS_TO_TRANSFER
        )
      ).to.be.revertedWithCustomError(unergyLogicReserve, "TicketExpired");
    });

    it("Should revert if the buyer is the project originator", async () => {
      const {
        unergyLogicReserve,
        unergyData,
        STABLE_COIN_DECIMALS,
        projectOriginator1,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATT_PRICE = BigInt(10 ** STABLE_COIN_DECIMALS);
      const LIFE_TIME = 1000;

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        projectOriginator1.address
      );

      const PWATTS_TO_TRANSFER = toBn(2000);
      await expect(
        unergyLogicReserve.pWattsTransfer(
          project1.address,
          projectOriginator1.address,
          PWATTS_TO_TRANSFER
        )
      ).to.be.revertedWithCustomError(
        unergyLogicReserve,
        "OriginatorCannotMakeAPurchase"
      );
    });

    it("Should execute a pWattsTransfer and update the rigth values", async () => {
      const {
        unergyLogicReserve,
        unergyData,
        projectsManager,
        STABLE_COIN_DECIMALS,
        user1,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATT_PRICE = BigInt(10 ** STABLE_COIN_DECIMALS);
      const LIFE_TIME = 1000;

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      const PWATTS_TO_TRANSFER = toBn(2000);
      await unergyLogicReserve.pWattsTransfer(
        project1.address,
        user1.address,
        PWATTS_TO_TRANSFER
      );

      const project1Struct = await projectsManager.getProject(project1.address);

      const balanceOfPWatts_user1 = await project1.balanceOf(user1.address);
      const buyTicked_user1 = await unergyData.redeemTicket(
        project1.address,
        user1.address
      );
      const approvalOfUser1OverUnergyLogicOperation = await project1.allowance(
        user1.address,
        unergyLogicReserve.address
      );

      expect(balanceOfPWatts_user1).to.equal(PWATTS_TO_TRANSFER);
      expect(buyTicked_user1.used).to.equal(true);
      expect(approvalOfUser1OverUnergyLogicOperation).to.equal(
        ethers.constants.MaxUint256
      );
    });

    // @todo test when the receiver is the unergyBuyer
  });

  describe("Buy pWatts", () => {
    it("Should revert if the contract is paused", async () => {
      const { unergyLogicReserve, user1 } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await unergyLogicReserve.pause();

      const PWATTS_TO_BUY = toBn(2000);
      await expect(
        unergyLogicReserve
          .connect(user1)
          .buyPWatts(project1.address, PWATTS_TO_BUY)
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller has no `BuyTicket`", async () => {
      const { unergyLogicReserve, user1 } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = toBn(2000);

      await expect(
        unergyLogicReserve
          .connect(user1)
          .buyPWatts(project1.address, PWATTS_TO_BUY)
      ).to.be.revertedWithCustomError(unergyLogicReserve, "UserHasNoTicket");
    });

    it("Should revert if the ticket has already been used", async () => {
      const {
        unergyLogicReserve,
        unergyData,
        STABLE_COIN_DECIMALS,
        stableCoin,
        user1,
        admin,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = toBn(2000);

      const PWATT_PRICE = BigInt(10 ** STABLE_COIN_DECIMALS);
      const LIFE_TIME = 1000;

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      const STABLE_AMOUNT = parseUnits(2000, STABLE_COIN_DECIMALS);
      await transferAndApprove(
        stableCoin,
        admin,
        user1,
        unergyLogicReserve.address,
        STABLE_AMOUNT
      );

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      expect(
        unergyLogicReserve.buyPWattsFrom(
          ZERO_ADDRESS,
          project1.address,
          PWATTS_TO_BUY
        )
      ).to.be.revertedWithCustomError(unergyLogicReserve, "TicketUsed");
    });

    it("Should revert if the ticket has expired", async () => {
      const { unergyLogicReserve, unergyData, STABLE_COIN_DECIMALS, user1 } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = toBn(2000);

      const PWATT_PRICE = BigInt(10 ** STABLE_COIN_DECIMALS);
      const LIFE_TIME = 1000;

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      // Set the next block.timestamp to be after the ticket's lifetime
      await time.increase(LIFE_TIME + 1);

      await expect(
        unergyLogicReserve
          .connect(user1)
          .buyPWatts(project1.address, PWATTS_TO_BUY)
      ).to.be.revertedWithCustomError(unergyLogicReserve, "TicketExpired");
    });

    it("Should revert if the buyer is the project originator", async () => {
      const {
        unergyLogicReserve,
        unergyData,
        STABLE_COIN_DECIMALS,
        projectOriginator1,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = toBn(2000);

      const PWATT_PRICE = BigInt(10 ** STABLE_COIN_DECIMALS);
      const LIFE_TIME = 1000;

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        projectOriginator1.address
      );

      await expect(
        unergyLogicReserve
          .connect(projectOriginator1)
          .buyPWatts(project1.address, PWATTS_TO_BUY)
      ).to.be.revertedWithCustomError(
        unergyLogicReserve,
        "OriginatorCannotMakeAPurchase"
      );
    });

    it("Should buy pWatts and update the rigth values", async () => {
      const {
        user1,
        admin,
        stableCoin,
        unergyData,
        unergyLogicReserve,
        STABLE_COIN_DECIMALS,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = parseUnits(200, 2);

      const PWATT_PRICE = BigInt(10 ** STABLE_COIN_DECIMALS);
      const LIFE_TIME = 1000;

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      const STABLE_AMOUNT = BigInt(200 * 10 ** STABLE_COIN_DECIMALS);
      await transferAndApprove(
        stableCoin,
        admin,
        user1,
        unergyLogicReserve.address,
        STABLE_AMOUNT
      );

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      const balanceOfPWatts_user1 = await project1.balanceOf(user1.address);
      const buyTicked_user1 = await unergyData.redeemTicket(
        project1.address,
        user1.address
      );
      const approvalOfUser1OverUnergyLogicOperation = await project1.allowance(
        user1.address,
        unergyLogicReserve.address
      );

      expect(balanceOfPWatts_user1).to.equal(PWATTS_TO_BUY);
      expect(buyTicked_user1.used).to.equal(true);
      expect(approvalOfUser1OverUnergyLogicOperation).to.equal(
        ethers.constants.MaxUint256
      );
    });
  });

  describe("Swap Token", () => {
    it("Should revert if the contract is paused", async () => {
      const { unergyLogicReserve } = await loadFixture(deployFixture);

      await unergyLogicReserve.pause();

      await expect(
        unergyLogicReserve.swapToken(ethers.constants.AddressZero, 0)
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is not authorized", async () => {
      const { unergyLogicReserve, permissionGranter, notAdmin } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await expect(
        unergyLogicReserve.connect(notAdmin).swapToken(project1.address, 0)
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("swapToken", notAdmin.address);
    });

    it("Should revert if the project is not fully signed", async () => {
      const {
        unergyLogicReserve,
        projectsManager,
        user1,
        user2,
        projectHelper,
      } = await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];
      await projectHelper.transferPWattToUsers(project1, {
        users,
        amounts: [],
      });

      const project1Users = await projectsManager.getProjectHolders(
        project1.address
      );

      await expect(
        unergyLogicReserve.swapToken(project1.address, project1Users.length)
      ).to.be.revertedWithCustomError(
        unergyLogicReserve,
        "ProjectNotFullySigned"
      );
    });

    it("Should revert if the invoice reported amount is less than the amount that the unergyBuyer want to buy", async () => {
      const {
        user1,
        user2,
        meter1,
        unergyBuyer,
        unergyLogicReserve,
        projectHelper,
        projectsManager,
        stableCoin,
        unergyData,
      } = await loadFixture(deployFixture);
      const { project1, project2 } = await loadFixture(createProjectFixture);

      const usersProject1 = [user1.address, user2.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      const project2Struct = await projectsManager.getProject(project2.address);
      const project2Decimals = await project2.decimals();
      const balanceOfReserve = await stableCoin.balanceOf(
        unergyLogicReserve.address
      );

      await project2.approveSwap(
        project2Struct.adminAddr,
        unergyLogicReserve.address,
        project2Struct.pWattsSupply.mul(2)
      );

      const invalidMaxPWattThatCanBuyTheUnergyBuyer =
        calcMaxPWattThatCanBuyTheUnergyBuyer(
          project2Decimals,
          project2Struct,
          balanceOfReserve,
          false
        ).add("90000000000000000000000");

      await unergyData.generatePurchaseTicket(
        project2.address,
        parseUnits(1, await stableCoin.decimals()),
        1000000,
        unergyBuyer.address
      );

      await expect(
        unergyLogicReserve.pWattsTransfer(
          project2.address,
          unergyBuyer.address,
          invalidMaxPWattThatCanBuyTheUnergyBuyer
        )
      ).to.be.revertedWithCustomError(
        unergyLogicReserve,
        "NotEnoughPWattsToPurchase"
      );
    });

    it("Should revert token if the project is already in PRODUCTION", async function () {
      if (!externalDependenciesEnabled) this.skip();

      const {
        user1,
        user2,
        meter1,
        unergyLogicReserve,
        projectHelper,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const usersProject1 = [user1.address, user2.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1
      );

      const project1Users = await projectsManager.getProjectHolders(
        project1.address
      );

      //bring project 1 back to the swap
      await expect(
        unergyLogicReserve.swapToken(project1.address, project1Users.length)
      )
        .to.be.revertedWithCustomError(
          unergyLogicReserve,
          "ProjectAlreadyInProduction"
        )
        .withArgs(project1.address);
    });

    it("Should not swap tokens for an external holders addresses", async function () {
      if (!externalDependenciesEnabled) this.skip();

      const {
        user1,
        user2,
        meter1,
        projectHelper,
        projectsManager,
        uWattToken,
        unergyData,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      // * Set External holder address user 2
      await unergyData.setExternalHolderAddress(
        user2.address,
        project1.address,
        true
      );

      const users = [user1.address, user2.address];

      const projectStruct = await projectsManager.getProject(project1.address);
      const expectedBalance = (
        await projectsManager.getTotalPWatts(project1.address)
      )
        .sub(projectStruct.originatorFee)
        .div(BigInt(users.length));

      await projectHelper.bringToProduction(
        project1,
        {
          users,
          amounts: [],
        },
        meter1
      );

      const uWattBalanceOfUser1 = await uWattToken.balanceOf(user1.address);
      const uWattBalanceOfUser2 = await uWattToken.balanceOf(user2.address);

      const pWattBalanceOfUser1 = await project1.balanceOf(user1.address);
      const pWattBalanceOfUser2 = await project1.balanceOf(user2.address);

      expect({
        uWattBalanceOfUser1,
        uWattBalanceOfUser2,
      }).to.deep.equal({
        uWattBalanceOfUser1: expectedBalance.mul(9135).div(10000), // swapFactor
        uWattBalanceOfUser2: 0,
      });

      expect({
        pWattBalanceOfUser1,
        pWattBalanceOfUser2,
      }).to.deep.equal({
        pWattBalanceOfUser1: 0,
        pWattBalanceOfUser2: expectedBalance,
      });
    });

    it("Should swap token and update the rigth values", async () => {
      const {
        unergyLogicReserve,
        unergyData,
        user1,
        user2,
        projectHelper,
        projectsManager,
        uWattToken,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];
      await projectHelper.setInstallerSign(project1, {
        users,
        amounts: [],
      });

      const project1Struct = await projectsManager.getProject(project1.address);

      const project1Users = await projectsManager.getProjectHolders(
        project1.address
      );

      await unergyLogicReserve.swapToken(
        project1.address,
        project1Users.length
      );

      const uWattBalanceOfUser1 = await uWattToken.balanceOf(user1.address);
      const uWattBalanceOfUser2 = await uWattToken.balanceOf(user2.address);

      const project = await projectsManager.getProject(project1.address);

      const rawExpectedBalance = project.pWattsSupply
        .sub(project.originatorFee)
        .div(BigInt(users.length))
        .mul(project1Struct.swapFactor)
        .div(BigInt(10 ** 18));

      const swapFeePercentage = await unergyData.swapFeePercentage();
      const divFactor = BigInt(100 * 10 ** 18 * 10 ** 80);
      const swapFee1 = BigInt(
        BigInt(rawExpectedBalance.toString()) * BigInt(10 ** 80)
      );
      const swapFee2 =
        BigInt(swapFee1.toString()) * BigInt(swapFeePercentage.toString());
      const swapFee = swapFee2 / divFactor;
      const expectedBalance = rawExpectedBalance.sub(swapFee);

      expect(uWattBalanceOfUser1).to.be.closeTo(expectedBalance, 500000);
      expect(uWattBalanceOfUser2).to.be.closeTo(expectedBalance, 500000);
    });

    it("Should swap token when the UnergyBuyer is a pWatt holder", async function () {
      if (!externalDependenciesEnabled) this.skip();

      const {
        user1,
        user2,
        user3,
        meter1,
        meter2,
        unergyBuyer,
        unergyLogicReserve,
        projectHelper,
        projectsManager,
        stableCoin,
        unergyData,
        uWattToken,
      } = await loadFixture(deployFixture);
      const { project1, project2 } = await loadFixture(createProjectFixture);

      const stableCoinDecimals = await stableCoin.decimals();

      const usersProject1 = [user1.address, user2.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1
      );

      const stableAmountDepreciated = await unergyData.depreciationBalance();

      const project2Struct = await projectsManager.getProject(project2.address);
      const project2Decimals = await project2.decimals();
      let balanceOfReserve = await stableCoin.balanceOf(
        unergyLogicReserve.address
      );

      const pWattPrice = parseUnits(1, stableCoinDecimals);

      const maxPWattThatCanBuyTheUnergyBuyer =
        calcMaxPWattThatCanBuyTheUnergyBuyer(
          project2Decimals,
          project2Struct,
          balanceOfReserve,
          true
        ).div(2);

      const pWattsToBuyForUser3 = project2Struct.pWattsSupply
        .sub(maxPWattThatCanBuyTheUnergyBuyer)
        .sub(project2Struct.originatorFee);

      const usersProject2 = [unergyBuyer.address, user3.address];
      const amountsProject2 = [
        maxPWattThatCanBuyTheUnergyBuyer.toBigInt(),
        pWattsToBuyForUser3.toBigInt(),
      ];
      await projectHelper.bringToProduction(
        project2,
        {
          users: usersProject2,
          amounts: amountsProject2,
        },
        meter2,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        pWattPrice
      );

      balanceOfReserve = await uWattToken.balanceOf(unergyBuyer.address);

      const pWattPayedAsDepreciation = stableAmountDepreciated
        .mul(parseUnits(1, project2Decimals))
        .div(parseUnits(1, stableCoinDecimals));
      const expectedBalanceOfUnergyBuyer = maxPWattThatCanBuyTheUnergyBuyer
        .sub(pWattPayedAsDepreciation)
        .mul(project2Struct.swapFactor)
        // As the values multiplied above have 2 decimals,
        // we need to divide by 10000 to get the right value
        .div(10000);

      expect(balanceOfReserve).to.deep.equal(expectedBalanceOfUnergyBuyer);
    });

    it("Should swap token and update the right values although the project admin holds pWatts", async () => {
      const {
        unergyLogicReserve,
        unergyData,
        user1,
        user2,
        user3,
        projectHelper,
        projectsManager,
        uWattToken,
      } = await loadFixture(deployFixture);
      const { project2 } = await loadFixture(createProjectFixture);

      const signers = await ethers.getSigners();

      const users: string[] = (() => {
        const users = [];

        for (let i = 0; i < 11; i++) {
          users.push(signers[i].address);
        }

        return users;
      })();

      await projectHelper.setInstallerSign(project2, {
        users,
        amounts: [],
      });

      const project = await projectsManager.getProject(project2.address);
      const adminpWattBalance = await project2.balanceOf(project.adminAddr);

      expect(adminpWattBalance).to.greaterThan(0);

      const project2Users = await projectsManager.getProjectHolders(
        project2.address
      );

      await unergyLogicReserve.swapToken(
        project2.address,
        project2Users.length
      );

      const uWattBalanceOfUser1 = await uWattToken.balanceOf(user1.address);
      const uWattBalanceOfUser2 = await uWattToken.balanceOf(user2.address);
      const uWattBalanceOfUser3 = await uWattToken.balanceOf(user3.address);
      const uWattBalanceOfProjectAdmin = await uWattToken.balanceOf(
        project.adminAddr
      );

      const rawExpectedBalance = project.pWattsSupply
        .sub(project.originatorFee)
        .mul(10102)
        .div(10000)
        .div(users.length)
        .sub(1); // this is necessary due to the rounding where 1 == 0.01

      const swapFeePercentage = await unergyData.swapFeePercentage();
      const divFactor = BigInt(100 * 10 ** 18 * 10 ** 80);

      const swapFee1 = BigInt(
        BigInt(rawExpectedBalance.toString()) * BigInt(10 ** 80)
      );
      const swapFee2 =
        BigInt(swapFee1.toString()) * BigInt(swapFeePercentage.toString());
      const swapFee = swapFee2 / divFactor;
      const expectedBalance = rawExpectedBalance.sub(swapFee);

      const swapFee1Admin = BigInt(
        BigInt(adminpWattBalance.toString()) * BigInt(10 ** 80)
      );
      const swapFee2Admin =
        BigInt(swapFee1Admin.toString()) * BigInt(swapFeePercentage.toString());
      const swapFeeAdmin = swapFee2Admin / divFactor;
      const expectedBalanceAdmin = adminpWattBalance.sub(swapFeeAdmin);

      expect(uWattBalanceOfUser1).to.be.closeTo(expectedBalance, 10000);
      expect(uWattBalanceOfUser2).to.be.closeTo(expectedBalance, 10000);
      expect(uWattBalanceOfUser3).to.be.closeTo(expectedBalance, 10000);
      expect(uWattBalanceOfProjectAdmin).to.be.closeTo(
        expectedBalanceAdmin,
        10000
      );
    });

    it("Should swap tokens using requestCustomSwap function", async function () {
      if (!externalDependenciesEnabled) this.skip();

      const {
        user1,
        user2,
        meter1,
        unergyData,
        unergyLogicReserve,
        projectHelper,
        projectsManager,
        uWattToken,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      //set user 2 as external holder address
      await unergyData.setExternalHolderAddress(
        user2.address,
        project1.address,
        true
      );

      const usersProject1 = [user1.address, user2.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      const project1Struct = await projectsManager.getProject(project1.address);

      //user 2 project 1 balance of
      const user2P1PWatts = await project1.balanceOf(user2.address);

      //customSwap execution
      console.log({
        projectAddr: project1.address,
        userAddr: user2.address,
        swapFactor: project1Struct.swapFactor.toString(),
      });

      await unergyLogicReserve.requestCustomSwap(
        project1.address,
        user2.address,
        project1Struct.swapFactor
      );
      await wait(10);

      const user2UWattBalance = await uWattToken.balanceOf(user2.address);
      const swapFactor = project1Struct.swapFactor;
      const supposedUser2UWattBalance = user2P1PWatts
        .mul(swapFactor)
        .div(BigInt(10 ** 18));

      expect(user2UWattBalance).to.equal(supposedUser2UWattBalance);
    });

    it("Should swap tokens using customSwap function", async () => {
      const {
        user1,
        user2: externalHolder,
        meter1,
        unergyData,
        unergyLogicReserve,
        projectHelper,
        projectsManager,
        uWattToken,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      //set user 2 as External holder address
      await unergyData.setExternalHolderAddress(
        externalHolder.address,
        project1.address,
        true
      );

      const usersProject1 = [user1.address, externalHolder.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      const project1Struct = await projectsManager.getProject(project1.address);

      //user 2 project 1 balance of
      const externalHolderP1PWatts = await project1.balanceOf(
        externalHolder.address
      );

      //customSwap execution
      await unergyLogicReserve.customSwap(
        project1.address,
        externalHolder.address,
        project1Struct.swapFactor
      );

      const externalHolderUWattBalance = await uWattToken.balanceOf(
        externalHolder.address
      );
      const swapFactor = project1Struct.swapFactor;

      const swapFeePercentage = await unergyData.swapFeePercentage();
      const swapFee = externalHolderP1PWatts
        .mul(swapFactor)
        .div(parseEther(1))
        .mul(swapFeePercentage)
        .div(parseEther(100));

      const supposedExternalHolderUWattBalance = externalHolderP1PWatts
        .mul(swapFactor)
        .div(BigInt(10 ** 18))
        .sub(swapFee);

      expect(externalHolderUWattBalance).to.equal(
        supposedExternalHolderUWattBalance
      );
    });

    it("Should calculate well the percentage of pWatts that is inside the reserve", async function () {
      if (!externalDependenciesEnabled) this.skip();

      const {
        user1,
        user2,
        meter1,
        unergyData,
        projectHelper,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      //set user 2 as external holder address
      await unergyData.setExternalHolderAddress(
        user2.address,
        project1.address,
        true
      );

      const usersProject1 = [user1.address, user2.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1
      );

      const project1Struct = await projectsManager.getProject(project1.address);

      const pWattsInside = project1Struct.pWattsSupply;

      const pWattTotalSupply = await project1.totalSupply();

      const pWattsPercentageIntoReserve = pWattsInside
        .mul(BigInt(1e20))
        .div(pWattTotalSupply);

      const usdDepreciatedOffChain = pWattsPercentageIntoReserve
        .mul(BigInt(1e6))
        .div(BigInt(1e20));

      const usdDepreciatedOnChain = await unergyData.depreciationBalance();

      expect(usdDepreciatedOnChain).to.equal(usdDepreciatedOffChain);
    });

    it("Should swap token and all reserve uWatts purchased goes for depreciation", async () => {
      const {
        user1,
        user2,
        user3,
        meter1,
        meter2,
        unergyBuyer,
        projectHelper,
        projectsManager,
        uWattToken,
      } = await loadFixture(deployFixture);
      const { project1, project2 } = await loadFixture(createProjectFixture);

      const usersProject1 = [user1.address, user2.address];
      await projectHelper.bringToProduction(
        project1,
        {
          users: usersProject1,
          amounts: [],
        },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      const project2Struct = await projectsManager.getProject(project2.address);

      const maxPWattThatCanBuyTheUnergyBuyer = BigInt(50);

      const pWattsThatWillBeBoughtByUser3 = project2Struct.pWattsSupply
        .sub(maxPWattThatCanBuyTheUnergyBuyer)
        .sub(project2Struct.originatorFee);

      const usersProject2 = [unergyBuyer.address, user3.address];
      await projectHelper.bringToProduction(
        project2,
        {
          users: usersProject2,
          amounts: [
            maxPWattThatCanBuyTheUnergyBuyer,
            pWattsThatWillBeBoughtByUser3.toBigInt(),
          ],
        },
        meter2,
        undefined,
        undefined,
        undefined,
        2,
        undefined,
        undefined,
        undefined,
        true
      );

      expect(await uWattToken.balanceOf(unergyBuyer.address)).to.equal(0);
    });

    it("Should swap tokens of all token holders and make them external holder addresses", async function () {
      const {
        unergyLogicReserve,
        unergyData,
        user1,
        user2,
        projectHelper,
        projectsManager,
        uWattToken,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];
      await projectHelper.setInstallerSign(project1, {
        users,
        amounts: [],
      });

      const project1Struct = await projectsManager.getProject(project1.address);

      const project1Users = await projectsManager.getProjectHolders(
        project1.address
      );

      await unergyLogicReserve.swapToken(
        project1.address,
        project1Users.length
      );

      const projectHolders = await projectsManager.getProjectHolders(project1.address);

      for (let i = 0; i < projectHolders.length; i++) {
        const isExternalHolder = await unergyData.isExternalHolderAddress(projectHolders[i], project1.address);
        const projectAdmin = project1Struct.adminAddr;
        if(projectHolders[i] != projectAdmin && projectHolders[i] != unergyLogicReserve.address) {
          expect(isExternalHolder).to.equal(true);
        }
      }

    });

  });

  describe("stableCoin Withdraw", function () {
    it("Should withdraw stableCoin after a energy report", async function () {
      const {
        user1,
        user2,
        meter1,
        notAdmin,
        projectHelper,
        unergyLogicReserve,
        stableCoin,
        ENERGY_ASSET_DECIMALS,
        STABLE_COIN_DECIMALS,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await projectHelper.setInstallerSign(project1, {
        users: [user1.address, user2.address],
        amounts: [],
      });

      await unergyLogicReserve.swapToken(project1.address, toBn(5));

      const FIRST_REPORT_ENERGY = BigInt(10 ** ENERGY_ASSET_DECIMALS);
      await unergyLogicReserve
        .connect(meter1)
        .energyReport(project1.address, FIRST_REPORT_ENERGY);

      const SECOND_REPORT_ENERGY = BigInt(2 * 10 ** ENERGY_ASSET_DECIMALS);
      await unergyLogicReserve
        .connect(meter1)
        .energyReport(project1.address, SECOND_REPORT_ENERGY);

      const ENERGY_DELTA = BigInt(10 ** ENERGY_ASSET_DECIMALS);
      const ENERGY_TARIFF = BigInt(1000000);
      const USD_DEPRECIATED = BigInt(2000000);

      await unergyLogicReserve.invoiceReport(
        project1.address,
        ENERGY_DELTA,
        ENERGY_TARIFF,
        USD_DEPRECIATED
      );

      const unergyLogicReserveBalance = await stableCoin.balanceOf(
        unergyLogicReserve.address
      );

      const amountPayed = (ENERGY_DELTA * ENERGY_TARIFF) / BigInt(10 ** 18);

      const stableAmountForMaintenance =
        (amountPayed * BigInt(10 ** STABLE_COIN_DECIMALS) * BigInt(10 ** 18)) /
        (BigInt(10) * BigInt(10 ** STABLE_COIN_DECIMALS) * BigInt(10 ** 18));

      const stableAmountForAssetsManagement =
        (amountPayed * BigInt(2 * 10 ** 18)) / (BigInt(10 ** 18) * BigInt(100));

      const reserveStableEstimatedValue =
        amountPayed -
        stableAmountForMaintenance -
        stableAmountForAssetsManagement;

      //stable withdraw
      await unergyLogicReserve.withdrawStableCoin(
        project1.address,
        notAdmin.address,
        unergyLogicReserveBalance
      );

      const notAdminStableBalance = await stableCoin.balanceOf(
        notAdmin.address
      );

      expect(notAdminStableBalance).to.equal(reserveStableEstimatedValue);
    });
  });

  describe("Addresses setters", () => {
    it("Should revert if the caller is not the owner", async () => {
      const { unergyLogicReserve, notAdmin } = await loadFixture(deployFixture);

      const randomAddress = ethers.Wallet.createRandom().address;

      await expect(
        unergyLogicReserve
          .connect(notAdmin)
          .setPermissionGranterAddr(randomAddress)
      )
        .to.be.revertedWithCustomError(
          unergyLogicReserve,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("setPermissionGranterAddr", notAdmin.address);
    });

    it("Should revert if the address to set is the zero address", async () => {
      const { admin, unergyLogicReserve, multiSignPermission } =
        await loadFixture(deployFixture);

      const zeroAddr = ethers.constants.AddressZero;

      await multiSignPermission.setPermission(
        admin.address,
        unergyLogicReserve.address,
        "setPermissionGranterAddr"
      );

      await expect(
        unergyLogicReserve.setPermissionGranterAddr(zeroAddr)
      ).to.be.revertedWithCustomError(
        unergyLogicReserve,
        "ZeroAddressNotAllowed"
      );
    });

    it("Should set the address of the required contracts correctly", async () => {
      const { unergyLogicReserve, multiSignPermission, admin } =
        await loadFixture(deployFixture);

      const randomAddress = ethers.Wallet.createRandom().address;

      await multiSignPermission.setPermission(
        admin.address,
        unergyLogicReserve.address,
        "setPermissionGranterAddr"
      );

      await unergyLogicReserve.setPermissionGranterAddr(randomAddress);

      expect(await unergyLogicReserve.permissionGranter()).to.be.equal(
        randomAddress
      );
    });
  });

  describe("Transfer ownership", () => {
    it("Should revert if the contract is paused", async () => {
      const { unergyLogicReserve } = await loadFixture(deployFixture);

      await unergyLogicReserve.pause();

      const randomAddress = ethers.Wallet.createRandom().address;

      await expect(
        unergyLogicReserve.transferOwnership(randomAddress)
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is not authorized", async () => {
      const { unergyLogicReserve, notAdmin } = await loadFixture(deployFixture);

      const randomAddress = ethers.Wallet.createRandom().address;

      await expect(
        unergyLogicReserve.connect(notAdmin).transferOwnership(randomAddress)
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it("Should revert address to set is the zero address", async () => {
      const { unergyLogicReserve } = await loadFixture(deployFixture);

      const zeroAddr = ethers.constants.AddressZero;

      await expect(
        unergyLogicReserve.transferOwnership(zeroAddr)
      ).to.be.revertedWithCustomError(
        unergyLogicReserve,
        "ZeroAddressNotAllowed"
      );
    });

    it("Should transfer ownership correctly", async () => {
      const { unergyLogicReserve, notAdmin } = await loadFixture(deployFixture);

      await unergyLogicReserve.transferOwnership(notAdmin.address);

      await unergyLogicReserve.connect(notAdmin).acceptOwnership();

      expect(await unergyLogicReserve.owner()).to.be.equal(notAdmin.address);
    });
  });

  describe("Request Standard Swap", () => {
    it("Should emit the `StandardSwapRequested` event", async function () {
      const { projectHelper, unergyLogicReserve, user1, user2 } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await projectHelper.setInstallerSign(project1, {
        users: [user1.address, user2.address],
        amounts: [],
      });

      const usersToProcess = toBn(10);

      await expect(
        unergyLogicReserve.requestStandardSwap(project1.address, usersToProcess)
      ).to.emit(unergyLogicReserve, "StandardSwapRequested");
    });

    it("Should revert if a StandardSwapRequested is made when the project already in production", async function () {
      const { projectHelper, unergyLogicReserve, user1, user2 } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await projectHelper.swapToken(
        project1,
        {
          users: [user1.address, user2.address],
          amounts: [],
        },
        undefined,
        undefined,
        true
      );

      const usersToProcess = toBn(10);

      await expect(
        unergyLogicReserve.requestStandardSwap(project1.address, usersToProcess)
      ).to.be.revertedWithCustomError(
        unergyLogicReserve,
        "ProjectAlreadyInProduction"
      );
    });
  });

  describe("Request Custom Swap", () => {
    it("Should emit the `CustomSwapRequested` event", async function () {
      const { unergyLogicReserve, unergyData, user1 } = await loadFixture(
        deployFixture
      );
      const { project1 } = await loadFixture(createProjectFixture);

      await unergyData.setExternalHolderAddress(
        user1.address,
        project1.address,
        true
      );

      await expect(
        unergyLogicReserve.requestCustomSwap(
          project1.address,
          user1.address,
          parseUnits(1, 18)
        )
      ).to.emit(unergyLogicReserve, "CustomSwapRequested");
    });
  });
});
