import { loadFixture, time } from "@nomicfoundation/hardhat-network-helpers";
import { ethers } from "hardhat";
import { expect } from "chai";

import { deployFixture } from "./fixtures/deployFixture";
import { DEFAULT_ADMIN_ROLE } from "../utils/constants";
import { PermissionType } from "../types/contracts.types";
import { createProjectFixture } from "./fixtures";
import { wallet } from "../utils/wallet";

describe("PermissionGranter", function () {
  it("Should set the rigth role to the admin", async function () {
    const { permissionGranter, admin } = await loadFixture(deployFixture);

    expect(await permissionGranter.hasRole(DEFAULT_ADMIN_ROLE, admin.address))
      .to.be.true;
  });

  it("Should set permissions to the admin's address", async function () {
    const { permissionGranter, admin, projectsManager } = await loadFixture(
      deployFixture
    );

    await permissionGranter.setPermission(
      admin.address,
      projectsManager.address,
      "createProject",
      PermissionType.PERMANENT,
      0
    );

    const hasCreateProjectPermission = await permissionGranter.getPermission(
      admin.address,
      projectsManager.address,
      "createProject"
    );

    expect(hasCreateProjectPermission).to.be.true;
    expect(hasCreateProjectPermission).to.be.true;
  });

  it("Should not allow not admin to set permissions", async function () {
    const { permissionGranter, notAdmin, admin, projectsManager } =
      await loadFixture(deployFixture);

    await expect(
      permissionGranter
        .connect(notAdmin)
        .setPermission(
          admin.address,
          projectsManager.address,
          "createProject",
          PermissionType.PERMANENT,
          0
        )
    ).to.be.revertedWith(
      `AccessControl: account ${ethers.utils.hexlify(
        notAdmin.address
      )} is missing role ${DEFAULT_ADMIN_ROLE}`
    );
  });

  it("Should not allow not admin to revoke permissions", async function () {
    const { permissionGranter, notAdmin, admin, projectsManager } =
      await loadFixture(deployFixture);

    await expect(
      permissionGranter
        .connect(notAdmin)
        .revokePermission(
          admin.address,
          projectsManager.address,
          "createProject",
          0
        )
    ).to.be.revertedWith(
      `AccessControl: account ${ethers.utils.hexlify(
        notAdmin.address
      )} is missing role ${DEFAULT_ADMIN_ROLE}`
    );
  });

  it("Should return false to a non existing permission for admin's address", async function () {
    const { permissionGranter, admin, projectsManager } = await loadFixture(
      deployFixture
    );

    const hasPermission = await permissionGranter.getPermission(
      admin.address,
      projectsManager.address,
      "nonExistingPermission"
    );

    expect(hasPermission).to.be.false;
  });

  it("Should return false to a non allowed address", async function () {
    const { permissionGranter, notAdmin, projectsManager } = await loadFixture(
      deployFixture
    );

    const hasCreateProjectPermission = await permissionGranter.getPermission(
      notAdmin.address,
      projectsManager.address,
      "createProject"
    );

    const hasUpdateProjectPermissionPermission =
      await permissionGranter.getPermission(
        notAdmin.address,
        projectsManager.address,
        "updateProjectRelatedProperties"
      );

    expect([
      hasCreateProjectPermission,
      hasUpdateProjectPermissionPermission,
    ]).to.deep.equal([false, false]);
  });

  it("Should revoke permission to the admin's address", async function () {
    const { permissionGranter, admin, projectsManager, multiSignPermission } =
      await loadFixture(deployFixture);

    const permissionName = "permissionName";
    const permissiontType = PermissionType.PERMANENT;
    const permissionValue = 0n;

    await permissionGranter.setPermission(
      admin.address,
      projectsManager.address,
      permissionName,
      permissiontType,
      permissionValue
    );

    await permissionGranter.revokePermission(
      admin.address,
      projectsManager.address,
      permissionName,
      permissionValue
    );

    const permission = await permissionGranter.getPermission(
      admin.address,
      projectsManager.address,
      permissionName
    );

    expect(permission).to.be.false;
  });

  it("Should set a ONETIME permission and allows exactly one execution", async () => {
    const { multiSignPermission, user8 } = await loadFixture(deployFixture);
    const { project1 } = await loadFixture(createProjectFixture);

    const functionName = "mint";
    const permissionType = PermissionType.ONETIME;
    const permissionValue = BigInt(0);

    await multiSignPermission.setPermission(
      user8.address,
      project1.address,
      functionName,
      permissionType,
      permissionValue
    );

    const amountToMint = 1_000_000;
    await project1.connect(user8).mint(user8.address, amountToMint);

    await expect(
      project1.connect(user8).mint(user8.address, amountToMint)
    ).to.be.revertedWithCustomError(project1, "NotAuthorizedToCallFunction");
  });

  it("Should set a EXECUTION permission and allows exactly the given executions", async () => {
    const { multiSignPermission, user8 } = await loadFixture(deployFixture);
    const { project1 } = await loadFixture(createProjectFixture);

    const functionName = "mint";
    const permissionType = PermissionType.EXECUTIONS;
    const permissionValue = BigInt(3);

    await multiSignPermission.setPermission(
      user8.address,
      project1.address,
      functionName,
      permissionType,
      permissionValue
    );

    const amountToMint = 1_000_000;

    // * Execution #1
    await project1.connect(user8).mint(user8.address, amountToMint);

    // * Execution #2
    await project1.connect(user8).mint(user8.address, amountToMint);

    // * Execution #3
    await project1.connect(user8).mint(user8.address, amountToMint);

    await expect(
      // * Execution #4
      project1.connect(user8).mint(user8.address, amountToMint)
    ).to.be.revertedWithCustomError(project1, "NotAuthorizedToCallFunction");
  });

  it("Should set a TIMELOCK permission and allow execution of the function until the time has been reached", async () => {
    const { multiSignPermission, user8 } = await loadFixture(deployFixture);
    const { project1 } = await loadFixture(createProjectFixture);

    const functionName = "mint";
    const permissionType = PermissionType.TIMELOCK;
    const permissionValue = BigInt((await time.latest()) + 10_000_000);

    await multiSignPermission.setPermission(
      user8.address,
      project1.address,
      functionName,
      permissionType,
      permissionValue
    );

    const amountToMint = 1_000_000;

    // * Execution #1
    await project1.connect(user8).mint(user8.address, amountToMint);

    // * Execution #2
    await project1.connect(user8).mint(user8.address, amountToMint);

    // * Execution #3
    await project1.connect(user8).mint(user8.address, amountToMint);

    await time.increase(permissionValue + 1n);

    await expect(
      // * Execution #4
      project1.connect(user8).mint(user8.address, amountToMint)
    ).to.be.revertedWithCustomError(project1, "NotAuthorizedToCallFunction");
  });

  it("Should set permissions in batch", async () => {
    const { permissionGranter } = await loadFixture(deployFixture);

    const addresses = wallet.getRandom({ amount: 3 }) as string[];
    const contract = wallet.getRandom({ amount: 3 }) as string[];
    const fNames = ["mint", "burn", "transfer"];
    const pTypes = [
      PermissionType.EXECUTIONS,
      PermissionType.ONETIME,
      PermissionType.TIMELOCK,
    ];
    const pValues = [BigInt(1), BigInt(0), BigInt((await time.latest()) + 100)];

    await permissionGranter.setPermissionsBatch(
      addresses,
      contract,
      fNames,
      pTypes,
      pValues
    );

    const hasMintPermission = await permissionGranter.getPermission(
      addresses[0],
      contract[0],
      fNames[0]
    );
    const hasBurnPermission = await permissionGranter.getPermission(
      addresses[1],
      contract[1],
      fNames[1]
    );
    const hasTransferPermission = await permissionGranter.getPermission(
      addresses[2],
      contract[2],
      fNames[2]
    );

    expect(hasMintPermission).to.be.true;
    expect(hasBurnPermission).to.be.true;
    expect(hasTransferPermission).to.be.true;
  });

  it("Should set meter permissions in batch", async () => {
    const { permissionGranter } = await loadFixture(deployFixture);

    const meterAddresses = wallet.getRandom({ amount: 3 }) as string[];
    const logicContract = wallet.getRandom() as string;
    const projectsContracts = wallet.getRandom({ amount: 3 }) as string[];

    await permissionGranter.setMeterPermissionsBatch(
      meterAddresses,
      logicContract,
      projectsContracts
    );

    const hasMeterPermissions = await Promise.all(
      meterAddresses.map((meterAddress, i) =>
        permissionGranter.getMeterPermission(
          meterAddress,
          logicContract,
          projectsContracts[i]
        )
      )
    );

    expect(hasMeterPermissions).to.deep.equal([true, true, true]);
  });
});
