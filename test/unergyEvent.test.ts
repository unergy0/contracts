import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { ethers } from "hardhat";
import { expect } from "chai";

import { deployFixture } from "./fixtures";

describe("UnergyEvent Contract", () => {
  describe("Non Authorized Callers", function () {
    it("Should revert if the caller is not authorized", async function () {
      const {
        unergyEvent,
        uWattToken,
        permissionGranter,
        notAdmin,
        user1,
        user2,
      } = await loadFixture(deployFixture);

      await expect(
        unergyEvent
          .connect(notAdmin)
          .beforeTransferReceipt(
            uWattToken.address,
            user1.address,
            user2.address,
            1
          )
      ).to.be.revertedWithCustomError(
        permissionGranter,
        "NotAuthorizedToCallFunction"
      );

      await expect(
        unergyEvent
          .connect(notAdmin)
          .afterTransferReceipt(
            uWattToken.address,
            user1.address,
            user2.address,
            1
          )
      ).to.be.revertedWithCustomError(
        permissionGranter,
        "NotAuthorizedToCallFunction"
      );

      await expect(
        unergyEvent
          .connect(notAdmin)
          .setPermissionGranterAddr(permissionGranter.address)
      )
        .to.be.revertedWithCustomError(
          unergyEvent,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("setPermissionGranterAddr", notAdmin.address);
    });
  });

  describe("Contract Addresses setup", function () {
    it("Should revert if an admin try to set address 0 as a contract address", async () => {
      const { admin, unergyEvent, multiSignPermission } = await loadFixture(
        deployFixture
      );

      await multiSignPermission.setPermission(
        admin.address,
        unergyEvent.address,
        "setPermissionGranterAddr"
      );

      await expect(
        unergyEvent.setPermissionGranterAddr(ethers.constants.AddressZero)
      ).to.be.revertedWithCustomError(unergyEvent, "ZeroAddressNotAllowed");
    });
  });
});
