import {
  calcMaxPWattThatCanBuyTheUnergyBuyer,
  getImpersonatedSigner,
  parseUnits,
  wait,
} from "./../utils/helpers";
import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { ethers } from "hardhat";
import { expect } from "chai";

import {
  GENERAL_ASSET_ID,
  externalDependenciesEnabled,
} from "../utils/constants";
import { randomUsers } from "../utils";
import { ProjectState } from "../types/contracts.types";
import { createProjectFixture, deployFixture } from "./fixtures";
import { claimCalculator } from "../utils/calculatePercentageToClaim";

/**
 * * ----------------------------- Integration Test -----------------------------
 *
 * * [Case 1]: Project1 with 2 users, Project2 with 2 users, 1 of them is the Unergy Buyer, User1 and User2 claims, Project3 with 2 users,
 * *           Project4 with 2 users, Project5 with 2 users, 1 of them is the Unergy Buyer, all users claims.
 *
 * * [Case 2]: Project 1 goes into production with 2 users (user1, user2), then Project 2 goes into production with 2 users, 1 of them is
 * *           the Unergy Buyer, Project 3 goes into production with 2 users, 1 of them is the UnergyBuyer, User1 and User2 claims.
 */

describe("Integration Test", () => {
  it("Should burn Project Energy Asset and General Energy Asset and store profit when project is INSTALLED", async () => {
    const {
      user1,
      user2,
      meter1,
      mantainer,
      assetManager,
      stableCoin,
      cleanEnergyAsset,
      unergyLogicReserve,
      projectHelper,
    } = await loadFixture(deployFixture);
    const { project1 } = await loadFixture(createProjectFixture);

    const {
      invoiceReportTx,
      energyDelta,
      toPayEnergyDelta,
      energyTariff,
      incomeAmount,
    } = await projectHelper.bringToProduction(
      project1,
      {
        users: [user1.address, user2.address],
        amounts: [],
      },
      meter1,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      true
    );

    const assetId = await cleanEnergyAsset.tokenIdByProjectAddress(
      project1.address
    );

    const projectEnergyAssetBalance = await cleanEnergyAsset.balanceOf(
      cleanEnergyAsset.address,
      assetId
    );

    const generalEnergyAssetBalance = await cleanEnergyAsset.balanceOf(
      cleanEnergyAsset.address,
      GENERAL_ASSET_ID
    );

    const unergyLogicOperationStableCoinBalance = await stableCoin.balanceOf(
      unergyLogicReserve.address
    );

    const mantainerStableCoinBalance = await stableCoin.balanceOf(
      mantainer.address
    );

    const assetManagerStableCoinBalance = await stableCoin.balanceOf(
      assetManager.address
    );

    expect(invoiceReportTx)
      .to.emit(unergyLogicReserve, "InvoiceReport")
      .withArgs(
        project1.address,
        energyDelta,
        energyTariff,
        incomeAmount,
        0 // USD Depreciated per project
      );
    expect({
      projectEnergyAssetBalance,
      generalEnergyAssetBalance,
      unergyLogicOperationStableCoinBalance,
    }).to.deep.equal({
      projectEnergyAssetBalance: toPayEnergyDelta,
      generalEnergyAssetBalance: toPayEnergyDelta,
      unergyLogicOperationStableCoinBalance:
        incomeAmount -
        mantainerStableCoinBalance.toBigInt() -
        assetManagerStableCoinBalance.toBigInt(),
    });
  });

  it("Should burn Project Energy Asset and General Energy Asset, store profit and depreciation when project is PRODUCTION and tokens already swapped", async function () {
    if (!externalDependenciesEnabled) this.skip();

    const {
      user1,
      user2,
      user3,
      user4,
      meter1,
      meter2,
      projectsManager,
      unergyLogicReserve,
      projectHelper,
    } = await loadFixture(deployFixture);
    const { project1, project2 } = await loadFixture(createProjectFixture);

    // * ----------------------------- Project 1 -----------------------------

    await projectHelper.bringToProduction(
      project1,
      {
        users: [user1.address, user2.address],
        amounts: [],
      },
      meter1
    );

    // * ----------------------------- Project 2 -----------------------------

    await projectHelper.bringToProduction(
      project2,
      {
        users: [user3.address, user4.address],
        amounts: [],
      },
      meter2
    );

    const logicOperationProject1Balance = await project2.balanceOf(
      unergyLogicReserve.address
    );

    const project1TotalPWatts = await projectsManager.getTotalPWatts(
      project2.address
    );

    expect(logicOperationProject1Balance).to.equal(project1TotalPWatts);
  });

  it("Should bring to production all projects", async function () {
    if (!externalDependenciesEnabled) this.skip();

    const {
      meter1,
      meter2,
      meter3,
      meter4,
      meter5,
      meter6,
      projectsManager,
      projectHelper,
    } = await loadFixture(deployFixture);
    const { project1, project2, project3, project4, project5, project6 } =
      await loadFixture(createProjectFixture);

    await projectHelper.bringToProduction(
      project1,
      {
        users: await randomUsers(),
        amounts: [],
      },
      meter1
    );

    const project1Struct = await projectsManager.getProject(project1.address);
    const adminBalanceProject1 = await project1.balanceOf(
      project1Struct.adminAddr
    );
    expect(adminBalanceProject1).to.lessThan(100); // = 1 pWatt
    expect(project1Struct.state).to.equal(ProjectState.PRODUCTION);

    await projectHelper.bringToProduction(
      project2,
      {
        users: await randomUsers(),
        amounts: [],
      },
      meter2
    );

    const project2Struct = await projectsManager.getProject(project2.address);
    const adminBalanceProject2 = await project2.balanceOf(
      project2Struct.adminAddr
    );
    expect(adminBalanceProject2).to.lessThan(100); // = 1 pWatt
    expect(project2Struct.state).to.equal(ProjectState.PRODUCTION);

    await projectHelper.bringToProduction(
      project3,
      {
        users: await randomUsers(),
        amounts: [],
      },
      meter3
    );

    const project3Struct = await projectsManager.getProject(project3.address);
    const adminBalanceProject3 = await project3.balanceOf(
      project3Struct.adminAddr
    );
    expect(adminBalanceProject3).to.lessThan(100); // = 1 pWatt
    expect(project3Struct.state).to.equal(ProjectState.PRODUCTION);

    await projectHelper.bringToProduction(
      project4,
      {
        users: await randomUsers(),
        amounts: [],
      },
      meter4
    );

    const project4Struct = await projectsManager.getProject(project4.address);
    const adminBalanceProject4 = await project4.balanceOf(
      project4Struct.adminAddr
    );
    expect(adminBalanceProject4).to.lessThan(100); // = 1 pWatt
    expect(project4Struct.state).to.equal(ProjectState.PRODUCTION);

    await projectHelper.bringToProduction(
      project5,
      {
        users: await randomUsers(),
        amounts: [],
      },
      meter5
    );

    const project5Struct = await projectsManager.getProject(project5.address);
    const adminBalanceProject5 = await project5.balanceOf(
      project5Struct.adminAddr
    );
    expect(adminBalanceProject5).to.lessThan(100);
    expect(project5Struct.state).to.equal(ProjectState.PRODUCTION);

    await projectHelper.bringToProduction(
      project6,
      {
        users: await randomUsers(),
        amounts: [],
      },
      meter6
    );

    const project6Struct = await projectsManager.getProject(project6.address);
    const adminBalanceProject6 = await project6.balanceOf(
      project6Struct.adminAddr
    );
    expect(adminBalanceProject6).to.lessThan(100);
    expect(project6Struct.state).to.equal(ProjectState.PRODUCTION);
  });

  it("[Demo] Should claim user's profit with the rigth values", async function () {
    if (!externalDependenciesEnabled) this.skip();

    const {
      user1,
      user2,
      user3,
      user4,
      user5,
      user6,
      user7,
      meter1,
      meter2,
      meter3,
      meter4,
      randomUser1,
      randomUser2,
      randomUser3,
      randomUser4,
      unergyLogicReserve,
      projectsManager,
      projectHelper,
      unergyBuyer,
      stableCoin,
      uWattToken,
      projectOriginator1,
    } = await loadFixture(deployFixture);
    const { project1, project2, project3, project4 } = await loadFixture(
      createProjectFixture
    );

    console.log({
      user1: user1.address,
      user2: user2.address,
      user3: user3.address,
      user4: user4.address,
      user5: user5.address,
      user6: user6.address,
      user7: user7.address,
      randomUser1: randomUser1.address,
      randomUser2: randomUser2.address,
      randomUser3: randomUser3.address,
      randomUser4: randomUser4.address,
    });

    await projectHelper.bringToProduction(
      project1,
      {
        users: [user1.address, user2.address],
        amounts: [],
      },
      meter1
    );

    // Everything is working fine until here
    console.log("Project 1 brought to production");
    await wait(3);

    await projectHelper.bringToProduction(
      project2,
      {
        users: [user3.address, user4.address],
        amounts: [],
      },
      meter2
    );

    // Everything is working fine until here
    console.log("Project 2 brought to production");
    await wait(3);

    await projectHelper.bringToProduction(
      project3,
      {
        users: [user5.address, user6.address],
        amounts: [],
      },
      meter3
    );

    console.log("Project 3 brought to production");
    await wait(3);

    await uWattToken.connect(user2).transfer(randomUser1.address, 100);
    await uWattToken
      .connect(user2)
      .transfer(randomUser2.address, parseUnits(200, 18));
    await uWattToken.connect(user2).transfer(randomUser3.address, 300);
    await uWattToken.connect(user2).transfer(randomUser4.address, 100);

    const project4Struct = await projectsManager.getProject(project4.address);
    const pWattDecimals = await project4.decimals();
    const unergyLogicReserveStableBalance = await stableCoin.balanceOf(
      unergyLogicReserve.address
    );

    const maxPWattThatCanBuyTheUnergyBuyer =
      calcMaxPWattThatCanBuyTheUnergyBuyer(
        pWattDecimals,
        project4Struct,
        unergyLogicReserveStableBalance,
        true
      );

    const pWattsUser7 = (
      await project4.balanceOf(project4Struct.adminAddr)
    ).sub(maxPWattThatCanBuyTheUnergyBuyer);

    const totalSupply = (await uWattToken.totalSupply()).toBigInt();

    const user1Balance = (await uWattToken.balanceOf(user1.address)).toBigInt();
    const user2Balance = (await uWattToken.balanceOf(user2.address)).toBigInt();
    const user3Balance = (await uWattToken.balanceOf(user3.address)).toBigInt();
    const user4Balance = (await uWattToken.balanceOf(user4.address)).toBigInt();
    const user5Balance = (await uWattToken.balanceOf(user5.address)).toBigInt();
    const user6Balance = (await uWattToken.balanceOf(user6.address)).toBigInt();
    const rnd1Balance = (
      await uWattToken.balanceOf(randomUser1.address)
    ).toBigInt();
    const rnd2Balance = (
      await uWattToken.balanceOf(randomUser2.address)
    ).toBigInt();
    const rnd3Balance = (
      await uWattToken.balanceOf(randomUser3.address)
    ).toBigInt();
    const rnd4Balance = (
      await uWattToken.balanceOf(randomUser4.address)
    ).toBigInt();
    let optrBalance = (
      await uWattToken.balanceOf(projectOriginator1.address)
    ).toBigInt();

    await projectHelper.bringToProduction(
      project4,
      {
        users: [user7.address, unergyBuyer.address],
        amounts: [
          pWattsUser7.toBigInt(),
          maxPWattThatCanBuyTheUnergyBuyer.toBigInt(),
        ],
      },
      meter4
    );

    console.log("Project 4 brought to production");
    await wait(5);

    const optrBalanceAfterLastSwap = (
      await uWattToken.balanceOf(projectOriginator1.address)
    ).toBigInt();

    const unergyBuyerBalance = (
      await uWattToken.balanceOf(unergyBuyer.address)
    ).toBigInt();

    const user1Claim = claimCalculator(
      user1Balance,
      totalSupply,
      unergyBuyerBalance
    );
    const user2Claim = claimCalculator(
      user2Balance,
      totalSupply,
      unergyBuyerBalance
    );
    const user3Claim = claimCalculator(
      user3Balance,
      totalSupply,
      unergyBuyerBalance
    );
    const user4Claim = claimCalculator(
      user4Balance,
      totalSupply,
      unergyBuyerBalance
    );
    const user5Claim = claimCalculator(
      user5Balance,
      totalSupply,
      unergyBuyerBalance
    );
    const user6Claim = claimCalculator(
      user6Balance,
      totalSupply,
      unergyBuyerBalance
    );
    const rnd1Claim = claimCalculator(
      rnd1Balance,
      totalSupply,
      unergyBuyerBalance
    );
    const rnd2Claim = claimCalculator(
      rnd2Balance,
      totalSupply,
      unergyBuyerBalance
    );
    const rnd3Claim = claimCalculator(
      rnd3Balance,
      totalSupply,
      unergyBuyerBalance
    );
    const rnd4Claim = claimCalculator(
      rnd4Balance,
      totalSupply,
      unergyBuyerBalance
    );
    const optrClaim = claimCalculator(
      optrBalance,
      totalSupply,
      unergyBuyerBalance
    );

    await unergyLogicReserve.connect(user1).requestClaim();
    await unergyLogicReserve.connect(user2).requestClaim();
    await unergyLogicReserve.connect(user3).requestClaim();
    await unergyLogicReserve.connect(user4).requestClaim();
    await unergyLogicReserve.connect(user5).requestClaim();
    await unergyLogicReserve.connect(user6).requestClaim();
    await unergyLogicReserve.connect(randomUser1).requestClaim();
    await unergyLogicReserve.connect(randomUser2).requestClaim();
    await unergyLogicReserve.connect(randomUser3).requestClaim();
    await unergyLogicReserve.connect(randomUser4).requestClaim();
    await unergyLogicReserve.connect(projectOriginator1).requestClaim();

    await wait(110);

    const user1BalanceAfterClaim = await uWattToken.balanceOf(user1.address);
    const user2BalanceAfterClaim = await uWattToken.balanceOf(user2.address);
    const user3BalanceAfterClaim = await uWattToken.balanceOf(user3.address);
    const user4BalanceAfterClaim = await uWattToken.balanceOf(user4.address);
    const user5BalanceAfterClaim = await uWattToken.balanceOf(user5.address);
    const user6BalanceAfterClaim = await uWattToken.balanceOf(user6.address);
    const randomUser1BalanceAfterClaim = await uWattToken.balanceOf(
      randomUser1.address
    );
    const randomUser2BalanceAfterClaim = await uWattToken.balanceOf(
      randomUser2.address
    );
    const randomUser3BalanceAfterClaim = await uWattToken.balanceOf(
      randomUser3.address
    );
    const randomUser4BalanceAfterClaim = await uWattToken.balanceOf(
      randomUser4.address
    );
    const operatorBalanceAfterClaim = await uWattToken.balanceOf(
      projectOriginator1.address
    );

    expect(operatorBalanceAfterClaim).to.equal(
      optrBalanceAfterLastSwap + optrClaim
    );
    expect(user1BalanceAfterClaim).to.equal(user1Balance + user1Claim);
    expect(user2BalanceAfterClaim).to.equal(user2Balance + user2Claim);
    expect(user3BalanceAfterClaim).to.equal(user3Balance + user3Claim);
    expect(user4BalanceAfterClaim).to.equal(user4Balance + user4Claim);
    expect(user5BalanceAfterClaim).to.equal(user5Balance + user5Claim);
    expect(user6BalanceAfterClaim).to.equal(user6Balance + user6Claim);
    expect(randomUser1BalanceAfterClaim).to.equal(rnd1Balance + rnd1Claim);
    expect(randomUser2BalanceAfterClaim).to.equal(rnd2Balance + rnd2Claim);
    expect(randomUser3BalanceAfterClaim).to.equal(rnd3Balance + rnd3Claim);
    expect(randomUser4BalanceAfterClaim).to.equal(rnd4Balance + rnd4Claim);
  });

  it("[Case 1] Should claim user's profit with the rigth values", async function () {
    if (!externalDependenciesEnabled) this.skip();

    const {
      user1,
      user2,
      user3,
      user4,
      user5,
      user6,
      user7,
      user8,
      meter1,
      meter2,
      meter3,
      meter4,
      meter5,
      stableCoin,
      uWattToken,
      unergyBuyer,
      randomUser1,
      randomUser2,
      randomUser3,
      randomUser4,
      projectHelper,
      projectsManager,
      projectOriginator1,
      unergyLogicReserve,
    } = await loadFixture(deployFixture);
    const { project1, project2, project3, project4, project5 } =
      await loadFixture(createProjectFixture);

    console.log({
      user1: user1.address,
      user2: user2.address,
      user3: user3.address,
      user4: user4.address,
      user5: user5.address,
      user6: user6.address,
      user7: user7.address,
      user8: user8.address,
      unergyBuyer: unergyBuyer.address,
      randomUser1: randomUser1.address,
      randomUser2: randomUser2.address,
      randomUser3: randomUser3.address,
      randomUser4: randomUser4.address,
      originator: projectOriginator1.address,
    });

    /**
     * * Project 1 - [ User 1, User 2 ]
     */
    await projectHelper.bringToProduction(
      project1,
      {
        users: [user1.address, user2.address],
        amounts: [],
      },
      meter1
    );

    console.log("Project 1 brought to production");
    await wait(3);

    // * --------------------------------------

    const pWattDecimals = await project2.decimals();
    let unergyLogicReserveStableBalance = await stableCoin.balanceOf(
      unergyLogicReserve.address
    );

    const pWattsUnergyBuyerProject2 = ethers.utils
      .parseUnits("36450", pWattDecimals)
      .toBigInt();

    const pWattsUser7 = ethers.utils
      .parseUnits("36450", pWattDecimals)
      .toBigInt();

    /**
     * * Project 2 - [ User 7, Unergy Buyer ]
     */
    await projectHelper.bringToProduction(
      project2,
      {
        users: [user7.address, unergyBuyer.address],
        amounts: [pWattsUnergyBuyerProject2, pWattsUser7],
      },
      meter2
    );

    console.log("Project 2 brought to production");
    await wait(5);

    // * --------------------------------------------------

    // * Claiming for project 1
    // *
    await unergyLogicReserve.connect(user1).requestClaim();
    await unergyLogicReserve.connect(user2).requestClaim();

    await wait(30);
    // *
    // * --------------------------------------------------

    /**
     * * Project 3 - [ User 3, User 4 ]
     */
    await projectHelper.bringToProduction(
      project3,
      {
        users: [user3.address, user4.address],
        amounts: [],
      },
      meter3
    );

    // Everything is working fine until here
    console.log("Project 3 brought to production");
    await wait(3);

    // * --------------------------------------------------

    /**
     * * Project 4 - [ User 5, User 6 ]
     */
    await projectHelper.bringToProduction(
      project4,
      {
        users: [user5.address, user6.address],
        amounts: [],
      },
      meter4
    );

    console.log("Project 4 brought to production");
    await wait(3);

    // * --------------------------------------------------

    // * --------- Transfers ------------------------------

    await uWattToken.connect(user2).transfer(randomUser1.address, 100);
    await uWattToken.connect(user2).transfer(randomUser2.address, 200);
    await uWattToken.connect(user2).transfer(randomUser3.address, 300);
    await uWattToken.connect(user2).transfer(randomUser4.address, 100);

    // * --------------------------------------------------

    const project5Struct = await projectsManager.getProject(project5.address);
    unergyLogicReserveStableBalance = await stableCoin.balanceOf(
      unergyLogicReserve.address
    );

    const maxPWattThatCanBuyTheUnergyBuyer =
      calcMaxPWattThatCanBuyTheUnergyBuyer(
        pWattDecimals,
        project5Struct,
        unergyLogicReserveStableBalance,
        true
      );

    /**
     * * Project 5 - [ User 8, Unergy Buyer ]
     */
    const pWattsUser8 = (
      await project5.balanceOf(project5Struct.adminAddr)
    ).sub(maxPWattThatCanBuyTheUnergyBuyer);

    await projectHelper.bringToProduction(
      project5,
      {
        users: [user8.address, unergyBuyer.address],
        amounts: [
          pWattsUser8.toBigInt(),
          maxPWattThatCanBuyTheUnergyBuyer.toBigInt(),
        ],
      },
      meter5
    );

    console.log("Project 5 brought to production");
    await wait(3);

    // * --------------------------------------------------

    await unergyLogicReserve.connect(user1).requestClaim();
    await unergyLogicReserve.connect(user2).requestClaim();
    await unergyLogicReserve.connect(user3).requestClaim();
    await unergyLogicReserve.connect(user4).requestClaim();
    await unergyLogicReserve.connect(user5).requestClaim();
    await unergyLogicReserve.connect(user6).requestClaim();
    await unergyLogicReserve.connect(user7).requestClaim();
    await unergyLogicReserve.connect(randomUser1).requestClaim();
    await unergyLogicReserve.connect(randomUser2).requestClaim();
    await unergyLogicReserve.connect(randomUser3).requestClaim();
    await unergyLogicReserve.connect(randomUser4).requestClaim();
    await unergyLogicReserve.connect(projectOriginator1).requestClaim();

    await wait(200);

    const user1BalanceAfterClaim = await uWattToken.balanceOf(user1.address);
    const user2BalanceAfterClaim = await uWattToken.balanceOf(user2.address);
    const user3BalanceAfterClaim = await uWattToken.balanceOf(user3.address);
    const user4BalanceAfterClaim = await uWattToken.balanceOf(user4.address);
    const user5BalanceAfterClaim = await uWattToken.balanceOf(user5.address);
    const user6BalanceAfterClaim = await uWattToken.balanceOf(user6.address);
    const user7BalanceAfterClaim = await uWattToken.balanceOf(user7.address);
    const randomUser1BalanceAfterClaim = await uWattToken.balanceOf(
      randomUser1.address
    );
    const randomUser2BalanceAfterClaim = await uWattToken.balanceOf(
      randomUser2.address
    );
    const randomUser3BalanceAfterClaim = await uWattToken.balanceOf(
      randomUser3.address
    );
    const randomUser4BalanceAfterClaim = await uWattToken.balanceOf(
      randomUser4.address
    );
    const operatorBalanceAfterClaim = await uWattToken.balanceOf(
      projectOriginator1.address
    );

    console.log({
      user1BalanceAfterClaim,
      user2BalanceAfterClaim,
      user3BalanceAfterClaim,
      user4BalanceAfterClaim,
      user5BalanceAfterClaim,
      user6BalanceAfterClaim,
      user7BalanceAfterClaim,
      operatorBalanceAfterClaim,
      randomUser1BalanceAfterClaim,
      randomUser2BalanceAfterClaim,
      randomUser3BalanceAfterClaim,
      randomUser4BalanceAfterClaim,
    });

    // @todo - Check that the values are correct within the test itself. This is currently done manually.
  });

  it("[Case 2] Should claim user's profit with the rigth values", async function () {
    // @todo - Finish this test
    if (!externalDependenciesEnabled) this.skip();

    const {
      user1,
      user2,
      user3,
      user4,
      meter1,
      meter2,
      meter3,
      unergyData,
      uWattToken,
      unergyBuyer,
      projectHelper,
      unergyLogicReserve,
      projectOriginator1: optr,
    } = await loadFixture(deployFixture);
    const { project1, project2, project3 } = await loadFixture(
      createProjectFixture
    );

    // * ------------------| Project 1 |-------------------
    await projectHelper.bringToProduction(
      project1,
      {
        users: [user1.address, user2.address],
        amounts: [],
      },
      meter1
    );

    console.log("Project 1 brought to production");
    // * --------------------------------------------------

    const user1Balance = (await uWattToken.balanceOf(user1.address)).toBigInt();
    const user2Balance = (await uWattToken.balanceOf(user2.address)).toBigInt();
    const optrBalance = (await uWattToken.balanceOf(optr.address)).toBigInt();

    const totalSupply = (await uWattToken.totalSupply()).toBigInt();

    // * ------------------| Project 2 |-------------------
    await projectHelper.bringToProduction(
      project2,
      {
        users: [user3.address, unergyBuyer.address],
        amounts: [],
      },
      meter2
    );
    // * --------------------------------------------------

    let unergyBuyerBalance = (
      await uWattToken.balanceOf(unergyBuyer.address)
    ).toBigInt();

    let user1Claim = claimCalculator(
      user1Balance,
      totalSupply,
      unergyBuyerBalance
    );
    let user2Claim = claimCalculator(
      user2Balance,
      totalSupply,
      unergyBuyerBalance
    );
    let optrClaim = claimCalculator(
      optrBalance,
      totalSupply,
      unergyBuyerBalance
    );

    // * -------------------| Project 3 |-------------------
    await projectHelper.bringToProduction(
      project3,
      {
        users: [user4.address, unergyBuyer.address],
        amounts: [],
      },
      meter3
    );
    // * ---------------------------------------------------

    unergyBuyerBalance = (
      await uWattToken.balanceOf(unergyBuyer.address)
    ).toBigInt();

    user1Claim = claimCalculator(
      user1Balance + user1Claim,
      totalSupply,
      unergyBuyerBalance
    );
    user2Claim = claimCalculator(
      user2Balance + user2Claim,
      totalSupply,
      unergyBuyerBalance
    );
    optrClaim = claimCalculator(
      optrBalance + optrClaim,
      totalSupply,
      unergyBuyerBalance
    );

    const oprtBalanceAfterLastSwap = (
      await uWattToken.balanceOf(optr.address)
    ).toBigInt();
    const user1BalanceAfterLastSwap = (
      await uWattToken.balanceOf(user1.address)
    ).toBigInt();
    const user2BalanceAfterLastSwap = (
      await uWattToken.balanceOf(user2.address)
    ).toBigInt();

    // * -----------------| Swapping Project 2 |-------------
    await unergyLogicReserve.requestStandardSwap(project2.address, 3);
    await wait(10);
    // * ----------------------------------------------------

    // * --------------------| Claiming |--------------------
    await unergyLogicReserve.connect(optr).requestClaim();
    await unergyLogicReserve.connect(user1).requestClaim();
    await unergyLogicReserve.connect(user2).requestClaim();
    // * ----------------------------------------------------

    expect(oprtBalanceAfterLastSwap).to.equal(optrBalance + optrClaim);
    expect(user1BalanceAfterLastSwap).to.equal(user1Balance + user1Claim);
    expect(user2BalanceAfterLastSwap).to.equal(user2Balance + user2Claim);
  });

  it("Reserve initialization", async () => {
    const {
      user1,
      user2,
      user3,
      user4,
      user5,
      user6,
      user7,
      user8,
      uWattToken,
      unergyData,
      unergyBuyer,
      projectHelper,
      projectsManager,
      unergyLogicReserve,
    } = await loadFixture(deployFixture);
    const { project1, project2, project3, project4 } = await loadFixture(
      createProjectFixture
    );

    //Initialization parameters for pWatts buying
    const expirationTime = 1000000000;
    const pWattPrice = 0;

    //Project1 Initialization

    //save real project 1 value
    const project1Struct = await projectsManager.getProject(project1.address);
    const realProjectValue = project1Struct.currentProjectValue;

    //change Project1 Value
    await unergyBuyer.setInitialProjectValue(project1.address, 0);

    const usersProject1 = [user1.address, user2.address];

    await projectHelper.setInstallerSign(
      project1,
      {
        users: usersProject1,
        amounts: [],
      },
      expirationTime,
      pWattPrice
    );

    const project1Users = await projectsManager.getProjectHolders(
      project1.address
    );

    //reestablish the project 1 value
    await unergyBuyer.setInitialProjectValue(
      project1.address,
      realProjectValue
    );

    await unergyLogicReserve.swapToken(project1.address, project1Users.length);

    const project1State = await projectsManager.getProjectState(
      project1.address
    );

    const rawExpectedBalanceP1 = project1Struct.pWattsSupply
      .sub(project1Struct.originatorFee)
      .mul(project1Struct.swapFactor)
      .div(BigInt(10 ** 18))
      .div(usersProject1.length);

    const swapFeePercentageP1 = await unergyData.swapFeePercentage();
    const divFactorP1 = BigInt(100 * 10 ** 18 * 10 ** 80);
    const swapFee1P1 = BigInt(
      BigInt(rawExpectedBalanceP1.toString()) * BigInt(10 ** 80)
    );
    const swapFee2P1 =
      BigInt(swapFee1P1.toString()) * BigInt(swapFeePercentageP1.toString());
    const swapFeeP1 = swapFee2P1 / divFactorP1;
    const expectedBalanceP1 = rawExpectedBalanceP1.sub(swapFeeP1);

    const user1UWattBalance = await uWattToken.balanceOf(user1.address);

    const user2UWattBalance = await uWattToken.balanceOf(user2.address);

    expect(project1State).to.equal(2);
    expect(user1UWattBalance).to.be.closeTo(expectedBalanceP1, 200000);
    expect(user2UWattBalance).to.be.closeTo(expectedBalanceP1, 200000);

    //Project2 Initialization

    //save real project 2 value
    const project2Struct = await projectsManager.getProject(project2.address);
    const realProject2Value = project2Struct.currentProjectValue;

    //change Project2Value
    await unergyBuyer.setInitialProjectValue(project2.address, 0);

    const usersProject2 = [user3.address, user4.address];

    await projectHelper.setInstallerSign(
      project2,
      {
        users: usersProject2,
        amounts: [],
      },
      expirationTime,
      pWattPrice
    );

    const project2Users = await projectsManager.getProjectHolders(
      project2.address
    );

    //reestablish the project 2 value
    await unergyBuyer.setInitialProjectValue(
      project2.address,
      realProject2Value
    );

    await unergyLogicReserve.swapToken(project2.address, project2Users.length);

    const project2State = await projectsManager.getProjectState(
      project2.address
    );

    const rawExpectedBalanceP2 = project2Struct.pWattsSupply
      .sub(project2Struct.originatorFee)
      .mul(project2Struct.swapFactor)
      .div(BigInt(10 ** 18))
      .div(usersProject2.length);

    const swapFeePercentageP2 = await unergyData.swapFeePercentage();
    const divFactorP2 = BigInt(100 * 10 ** 18 * 10 ** 80);
    const swapFee1P2 = BigInt(
      BigInt(rawExpectedBalanceP2.toString()) * BigInt(10 ** 80)
    );
    const swapFee2P2 =
      BigInt(swapFee1P2.toString()) * BigInt(swapFeePercentageP2.toString());
    const swapFeeP2 = swapFee2P2 / divFactorP2;
    const expectedBalanceP2 = rawExpectedBalanceP2.sub(swapFeeP2);

    const user3UWattBalance = await uWattToken.balanceOf(user3.address);

    const user4UWattBalance = await uWattToken.balanceOf(user4.address);

    expect(project2State).to.equal(2);
    expect(user3UWattBalance).to.be.closeTo(expectedBalanceP2, 200000);
    expect(user4UWattBalance).to.be.closeTo(expectedBalanceP2, 200000);

    //Project3 Initialization

    //save real project 3 value
    const project3Struct = await projectsManager.getProject(project3.address);
    const realProject3Value = project3Struct.currentProjectValue;

    //change Project1 Value
    await unergyBuyer.setInitialProjectValue(project3.address, 0);

    const usersProject3 = [user5.address, user6.address];

    await projectHelper.setInstallerSign(
      project3,
      {
        users: usersProject3,
        amounts: [],
      },
      expirationTime,
      pWattPrice
    );

    const project3Users = await projectsManager.getProjectHolders(
      project3.address
    );

    //reestablish the project 3 value
    await unergyBuyer.setInitialProjectValue(
      project3.address,
      realProject3Value
    );

    await unergyLogicReserve.swapToken(project3.address, project3Users.length);

    const project3State = await projectsManager.getProjectState(
      project3.address
    );

    const rawExpectedBalanceP3 = project3Struct.pWattsSupply
      .sub(project3Struct.originatorFee)
      .mul(project3Struct.swapFactor)
      .div(BigInt(10 ** 18))
      .div(usersProject3.length);

    const swapFeePercentageP3 = await unergyData.swapFeePercentage();
    const divFactorP3 = BigInt(100 * 10 ** 18 * 10 ** 80);
    const swapFee1P3 = BigInt(
      BigInt(rawExpectedBalanceP3.toString()) * BigInt(10 ** 80)
    );
    const swapFee2P3 =
      BigInt(swapFee1P3.toString()) * BigInt(swapFeePercentageP3.toString());
    const swapFeeP3 = swapFee2P3 / divFactorP3;
    const expectedBalanceP3 = rawExpectedBalanceP3.sub(swapFeeP3);

    const user5UWattBalance = await uWattToken.balanceOf(user5.address);

    const user6UWattBalance = await uWattToken.balanceOf(user6.address);

    expect(project3State).to.equal(2);
    expect(user5UWattBalance).to.be.closeTo(expectedBalanceP3, 200000);
    expect(user6UWattBalance).to.be.closeTo(expectedBalanceP3, 200000);

    //Project4 Initialization

    //save real project 4 value
    const project4Struct = await projectsManager.getProject(project4.address);
    const realProject4Value = project4Struct.currentProjectValue;

    //change Project4 Value
    await unergyBuyer.setInitialProjectValue(project4.address, 0);

    const usersProject4 = [user7.address, user8.address];

    await projectHelper.setInstallerSign(
      project4,
      {
        users: usersProject4,
        amounts: [],
      },
      expirationTime,
      pWattPrice
    );

    const project4Users = await projectsManager.getProjectHolders(
      project4.address
    );

    //reestablish the project 4 value
    await unergyBuyer.setInitialProjectValue(
      project4.address,
      realProject4Value
    );

    await unergyLogicReserve.swapToken(project4.address, project4Users.length);

    const project4State = await projectsManager.getProjectState(
      project4.address
    );

    const rawExpectedBalanceP4 = project4Struct.pWattsSupply
      .sub(project4Struct.originatorFee)
      .mul(project4Struct.swapFactor)
      .div(BigInt(10 ** 18))
      .div(usersProject4.length);

    const swapFeePercentageP4 = await unergyData.swapFeePercentage();
    const divFactorP4 = BigInt(100 * 10 ** 18 * 10 ** 80);
    const swapFee1P4 = BigInt(
      BigInt(rawExpectedBalanceP4.toString()) * BigInt(10 ** 80)
    );
    const swapFee2P4 =
      BigInt(swapFee1P4.toString()) * BigInt(swapFeePercentageP4.toString());
    const swapFeeP4 = swapFee2P4 / divFactorP4;
    const expectedBalanceP4 = rawExpectedBalanceP4.sub(swapFeeP4);

    const user7UWattBalance = await uWattToken.balanceOf(user7.address);

    const user8UWattBalance = await uWattToken.balanceOf(user8.address);

    expect(project4State).to.equal(2);
    expect(user7UWattBalance).to.be.closeTo(expectedBalanceP4, 2000000);
    expect(user8UWattBalance).to.be.closeTo(expectedBalanceP4, 2000000);
  });

  it("Should trigger a uWatts reward via customSwap", async function () {
    if (!externalDependenciesEnabled) this.skip();

    const {
      // Users
      admin,
      user1,
      user2,
      user3,
      user4: externalHolder,
      // Meters
      meter1,
      meter2,
      // Contracts
      uWattToken,
      unergyData,
      unergyBuyer,
      projectsManager,
      unergyLogicReserve,
      // Helpers
      projectHelper,
    } = await loadFixture(deployFixture);
    const { project1, project2 } = await loadFixture(createProjectFixture);

    const prtclStkAddr = await unergyData.stakingProtocolAddress();

    const project1Obj = await projectsManager.getProject(project1.address);
    const project2Obj = await projectsManager.getProject(project2.address);
    const protocolStaking = await getImpersonatedSigner(prtclStkAddr);
    const operator = await getImpersonatedSigner(project1Obj.originator);

    // console.log({
    //   user1: user1.address,
    //   user2: user2.address,
    //   user3: user3.address,
    //   externalHolder: externalHolder.address,
    //   project1Admin: project1Obj.adminAddr,
    //   project2Admin: project2Obj.adminAddr,
    //   originatorP1: project1Obj.originator,
    //   originatorP2: project2Obj.originator,
    //   protocolStaking: protocolStaking.address,
    // });

    // 0. The "Reserve" must be an externalHolder
    await unergyData.setExternalHolderAddress(
      unergyBuyer.address,
      project2.address,
      true
    );

    // 1. A user is marked as external holder, so it holds pWatts even if a Swap is triggered
    await unergyData.setExternalHolderAddress(
      externalHolder.address,
      project2.address,
      true
    );

    // 2. Bring project 1 to production
    await projectHelper.bringToProduction(
      project1,
      {
        users: [user1.address, user2.address],
        amounts: [],
      },
      meter1
    );
    await wait(3, { log: false });

    // 3. Bring project 2 to production, so external holder keeps pWatts
    await projectHelper.bringToProduction(
      project2,
      {
        users: [user3.address, externalHolder.address],
        amounts: [],
      },
      meter2
    );
    await wait(3, { log: false });

    // 4. The "Reserve" buy all the user pWatts
    const externalHoldersPWatts = await project2.balanceOf(
      externalHolder.address
    );

    // 4.1 As the pWatt must be a compliance token, some steps are needed to transfer it
    await project2.approveSwap(
      externalHolder.address,
      admin.address,
      externalHoldersPWatts
    );
    await project2.transferFrom(
      externalHolder.address,
      unergyBuyer.address,
      externalHoldersPWatts
    );

    /**
     * * Temporary logs
     */
    const balancesBefore = [
      {
        ReservepWattBalance: await project2.balanceOf(unergyBuyer.address),
      },
      {
        ReserveruWattBalance: await uWattToken.balanceOf(unergyBuyer.address),
      },
      {
        operatorUWattBalance: await uWattToken.balanceOf(operator.address),
      },
      {
        user1uWattBalance: await uWattToken.balanceOf(user1.address),
      },
      {
        user2uWattBalance: await uWattToken.balanceOf(user2.address),
      },
      {
        uWattSupply: await uWattToken.totalSupply(),
      },
      {
        protocolStakingBalance: await uWattToken.balanceOf(
          protocolStaking.address
        ),
      },
      {
        user3uWattBalance: await uWattToken.balanceOf(user3.address),
      },
    ];

    // 5. The "Reserve" swap the pWatts just bought so a uWatts reward is triggered
    await projectHelper.customSwap(
      project2,
      unergyBuyer.address,
      project2Obj.swapFactor.toBigInt()
    );
    await wait(10, { log: false });

    const ReserveruWattBalance = await uWattToken.balanceOf(
      unergyBuyer.address
    );

    const user1Claim = claimCalculator(
      balancesBefore[3].user1uWattBalance!.toBigInt(),
      balancesBefore[5].uWattSupply!.toBigInt(),
      ReserveruWattBalance.toBigInt()
    );

    const user2Claim = claimCalculator(
      balancesBefore[4].user2uWattBalance!.toBigInt(),
      balancesBefore[5].uWattSupply!.toBigInt(),
      ReserveruWattBalance.toBigInt()
    );

    const user3Claim = claimCalculator(
      balancesBefore[7].user3uWattBalance!.toBigInt(),
      balancesBefore[5].uWattSupply!.toBigInt(),
      ReserveruWattBalance.toBigInt()
    );

    const optrClaim = claimCalculator(
      balancesBefore[2].operatorUWattBalance!.toBigInt(),
      balancesBefore[5].uWattSupply!.toBigInt(),
      ReserveruWattBalance.toBigInt()
    );

    const protStkBalance = claimCalculator(
      balancesBefore[6].protocolStakingBalance!.toBigInt(),
      balancesBefore[5].uWattSupply!.toBigInt(),
      ReserveruWattBalance.toBigInt()
    );

    const usr1BalanceBefore = await uWattToken.balanceOf(user1.address);
    const usr2BalanceBefore = await uWattToken.balanceOf(user2.address);
    const usr3BalanceBefore = await uWattToken.balanceOf(user3.address);
    const optrBalanceBefore = await uWattToken.balanceOf(operator.address);
    const protStkBalanceBefore = await uWattToken.balanceOf(
      protocolStaking.address
    );

    await unergyLogicReserve.connect(user1).requestClaim();
    await unergyLogicReserve.connect(user2).requestClaim();
    await unergyLogicReserve.connect(user3).requestClaim();
    await unergyLogicReserve.connect(operator).requestClaim();
    await unergyLogicReserve.connect(protocolStaking).requestClaim();

    await wait(60, { log: false });

    const usr1BalanceAfter = await uWattToken.balanceOf(user1.address);
    const usr2BalanceAfter = await uWattToken.balanceOf(user2.address);
    const usr3BalanceAfter = await uWattToken.balanceOf(user3.address);
    const optrBalanceAfter = await uWattToken.balanceOf(operator.address);
    const protStkBalanceAfter = await uWattToken.balanceOf(
      protocolStaking.address
    );

    expect(usr1BalanceAfter).to.equal(
      usr1BalanceBefore.toBigInt() + user1Claim
    );
    expect(usr2BalanceAfter).to.equal(
      usr2BalanceBefore.toBigInt() + user2Claim
    );
    expect(usr3BalanceAfter).to.equal(
      usr3BalanceBefore.toBigInt() + user3Claim
    );
    expect(optrBalanceAfter).to.equal(optrBalanceBefore.toBigInt() + optrClaim);
    expect(protStkBalanceAfter).to.equal(
      protStkBalanceBefore.toBigInt() + protStkBalance
    );
  });
});
