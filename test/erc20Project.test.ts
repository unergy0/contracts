import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { ethers, network } from "hardhat";
import { expect } from "chai";
import { deployFixture, createProjectFixture } from "./fixtures";
import { externalDependenciesEnabled } from "../utils/constants";
import { getTimestampInSeconds } from "../utils/helpers";

describe("ERC20 Project Contract", () => {
  it("Should set the admin as owner of ERC20 uWatt contract", async function () {
    const { admin } = await loadFixture(deployFixture);
    const { project1 } = await loadFixture(createProjectFixture);

    expect(await project1.owner()).to.deep.equal(admin.address);
  });

  it("Should set the rigth decimals", async function () {
    const { project1 } = await loadFixture(createProjectFixture);

    const decimals = 18; //All protocol ERC20's have 2 decimals

    expect(await project1.decimals()).to.deep.equal(decimals);
  });

  describe("Approve/Allowance", () => {
    // @todo add test for allowance
  });

  describe("Transfer", () => {
    it("Should send pWatts to user3 using transfer function", async function () {
      const { user1, user2, user3, projectHelper } = await loadFixture(
        deployFixture
      );

      const { project1 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      await projectHelper.transferPWattToUsers(project1, {
        users: users,
        amounts: [],
      });

      const amountToTransfer = 100;
      await project1.connect(user1).transfer(user3.address, amountToTransfer);

      const user3Balance = await project1.balanceOf(user3.address);

      expect(user3Balance.toString()).to.deep.equal(
        amountToTransfer.toString()
      );
    });

    it("Should revert if the project state is not funding", async function () {
      if (!externalDependenciesEnabled) this.skip();

      const { user1, user2, meter1, unergyData, projectHelper } =
        await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      //mark user 1 as institutional
      await unergyData.setExternalHolderAddress(
        project1.address,
        user1.address,
        true
      );

      await projectHelper.bringToProduction(
        project1,
        {
          users: users,
          amounts: [],
        },
        meter1
      );

      await expect(
        project1.connect(user1).transfer(user2.address, 100)
      ).to.be.revertedWithCustomError(
        project1,
        "ProjectIsNotInFundingOrInstalledState"
      );
    });
  });

  describe("Transfer Ownership", function () {
    it("Should transfer the ownership of UnergyBuyer contract", async function () {
      const { notAdmin } = await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      await project1.transferOwnership(notAdmin.address);

      expect(await project1.owner()).to.deep.equal(notAdmin.address);
    });
  });

  describe("Non Authorized Callers", function () {
    it("Should revert if the caller is not authorized", async function () {
      const { permissionGranter, notAdmin, user1 } = await loadFixture(
        deployFixture
      );

      const { project1 } = await loadFixture(createProjectFixture);

      await expect(project1.connect(notAdmin).mint(user1.address, 100))
        .to.be.revertedWithCustomError(project1, "NotAuthorizedToCallFunction")
        .withArgs("mint", notAdmin.address);

      await expect(
        project1
          .connect(notAdmin)
          .setPermissionGranterAddr(permissionGranter.address)
      )
        .to.be.revertedWithCustomError(project1, "NotAuthorizedToCallFunction")
        .withArgs("setPermissionGranterAddr", notAdmin.address);

      await expect(
        project1.connect(notAdmin).transferOwnership(user1.address)
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it("Should revert if the approveSwap is called by a non authorized user", async function () {
      const { notAdmin, user1 } = await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      await expect(
        project1
          .connect(notAdmin)
          .approveSwap(user1.address, notAdmin.address, 100)
      ).to.be.revertedWithCustomError(project1, "NotAuthorizedToCallFunction");
    });
  });

  describe("Contract Addresses setup", function () {
    it("Should change all contracts addresses on ERC20Project contract", async function () {
      const { admin, multiSignPermission } = await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      const randomAddress = ethers.Wallet.createRandom().address;

      await multiSignPermission.setPermission(
        admin.address,
        project1.address,
        "setPermissionGranterAddr"
      );

      //change all contracts addresses
      await project1.setPermissionGranterAddr(randomAddress);

      const permissionGranterInstance = await project1.permissionGranter();

      expect([permissionGranterInstance]).to.deep.equal([randomAddress]);
    });

    it("Should revert if an admin try to set address 0 as a contract address", async () => {
      const { admin, multiSignPermission } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await multiSignPermission.setPermission(
        admin.address,
        project1.address,
        "setPermissionGranterAddr"
      );

      await expect(
        project1.setPermissionGranterAddr(ethers.constants.AddressZero)
      ).to.be.revertedWithCustomError(project1, "ZeroAddressNotAllowed");

      await expect(
        project1.transferOwnership(ethers.constants.AddressZero)
      ).to.be.revertedWithCustomError(project1, "ZeroAddressNotAllowed");
    });
  });

  describe("Permit", () => {
    it("Should permit to another account", async () => {
      const { admin, user1, multiSignPermission, projectHelper } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await multiSignPermission.setPermission(
        admin.address,
        project1.address,
        "mint"
      );

      const AMOUNT_TO_MINT = ethers.utils.parseEther("100");

      await project1.mint(admin.address, AMOUNT_TO_MINT);

      const nonce = await project1.nonces(admin.address);

      const domain = {
        name: await project1.name(),
        version: "1",
        chainId: network.config.chainId,
        verifyingContract: project1.address,
      };

      const types = {
        Permit: [
          {
            name: "owner",
            type: "address",
          },
          {
            name: "spender",
            type: "address",
          },
          {
            name: "value",
            type: "uint256",
          },
          {
            name: "nonce",
            type: "uint256",
          },
          {
            name: "deadline",
            type: "uint256",
          },
        ],
      };

      const DEADLINE = getTimestampInSeconds() + 4200;

      const values = {
        owner: admin.address,
        spender: user1.address,
        value: AMOUNT_TO_MINT,
        nonce,
        deadline: DEADLINE,
      };

      const signature = await admin._signTypedData(domain, types, values);

      const sig = ethers.utils.splitSignature(signature);

      // const recovered = ethers.utils.verifyTypedData(
      //   domain,
      //   types,
      //   values,
      //   sig
      // );
      // console.log({ recovered });

      await project1.permit(
        admin.address,
        user1.address,
        AMOUNT_TO_MINT,
        DEADLINE,
        sig.v,
        sig.r,
        sig.s
      );

      const user1Allowance = await project1.allowance(
        admin.address,
        user1.address
      );

      expect(user1Allowance).to.equal(AMOUNT_TO_MINT);
    });
  });
});
