#!/bin/bash

curr_pwd=$(pwd)

cache_dir=$curr_pwd/cache
artifacts_dir=$curr_pwd/artifacts
node_modules_dir=$curr_pwd/node_modules
typechain_dir=$curr_pwd/typechain

start_time=$(date +%s)

rm -rf $cache_dir
rm -rf $artifacts_dir
rm -rf $node_modules_dir
rm -rf $typechain_dir

npm i
npx hardhat compile

end_time=$(date +%s)

echo "clean.sh took $(($end_time - $start_time)) seconds to complete."
