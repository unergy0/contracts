import { BigNumberish } from "@ethersproject/bignumber/src.ts/bignumber";

export enum ProjectState {
  FUNDING = 0,
  INSTALLED = 1,
  PRODUCTION = 2,
  CLOSED = 3,
  CANCELLED = 4,
  INREFUND = 5,
}

export enum Signature {
  INSTALLER = 0,
  ORIGINATOR = 1,
}

export interface MilestoneInput {
  name: string;
  weight: BigNumberish;
}

export enum PermissionType {
  UNAUTHORIZED = 0,
  PERMANENT = 1,
  EXECUTIONS = 2,
  TIMELOCK = 3,
  ONETIME = 4,
}

export enum UnergyEventVersion {
  V1 = 0,
  V2 = 1,
}
