import hre from "hardhat";

export interface GasStationResponseInRange {
  maxPriorityFee: number;
  maxFee: number;
}

export interface GasStationResponseFull {
  safeLow: GasStationResponseInRange;
  standard: GasStationResponseInRange;
  fast: GasStationResponseInRange;
}

export async function getGasPrice<T>(
  range: "safeLow" | "standard" | "fast" | "full"
): Promise<T> {
  const gasData = await getGasData();

  if (range === "safeLow") return gasData.safeLow as T;
  if (range === "standard") return gasData.standard as T;
  if (range === "fast") return gasData.fast as T;
  return gasData as T;
}

async function getGasData(): Promise<GasStationResponseFull> {
  const gasStationUrl = getGasStationUrl();
  if (gasStationUrl)
    return fetch(gasStationUrl)
      .then((response) => response.json())
      .then((data) => data)
      .catch((error) => Promise.reject(error));
  else return Promise.reject("Gas station url not found");
}

function getGasStationUrl() {
  if (hre.network.name === "polygon") {
    return process.env.POLYGON_MAINNET_GAS_STATION_URL;
  }

  if (hre.network.name === "polygon_mumbai") {
    return process.env.POLYGON_MUMBAI_GAS_STATION_URL;
  }
}
