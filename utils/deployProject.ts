import { Wallet, Signer } from 'ethers';
import {
  ERC20Project,
  ERC20StableCoin,
  UnergyData,
  UnergyLogicFunding,
  UnergyLogicOperation,
} from '../typechain-types';
import { DataStructure } from '../typechain-types/contracts/IUnergyData';

interface UsersAndAmounts {
  users: `0x${string}`[];
  amounts: number[];
}

export enum Stage {
  NewlyCreated = 'NewlyCreated',
  Production = 'Production',
  Funding = 'Funding',
  PWattTrasnfer = 'PWattTrasnfer',
  Milestones = 'Milestones',
  Swap = 'Swap',
  EnergyReporting = 'EnergyReporting',
  Installed = 'Installed',
  Invoice = 'Invoice',
}

/**
 * @notice This function takes a project from the pWatt purchase/distribution phase, through the swap phase, to the energy reporting phase.
 * @param {ProjectInstance} project
 * @param {UsersAndAmounts} usersAndAmounts - An object containing an array of users and the amounts to be transferred to each user. Example: `{ users: [user1, user2], amounts: [10000, 20000] }`. If users array is empty it throws. If amounts array is empty, each user will receive the same amount by dividing the project total pWatts
 * @param {DataInstance} dataContract
 * @param {LogicOperationInstance} logicContract
 * @param {LogicFundingInstance} fundingContract
 * @param {StableCoinInstance} stableCoinContract
 * @param {BigInt} [pWattPrice=10e18] Default value is 10e18
 * @param {Meter} meter
 * @param {number} stableDecimals
 * @param {number} meterMultiplier When one meter is used more than once, it requires a greater measure in order to work, so this multiplier allows to increase the energy reported at ease. Default = 1.
 * @returns {Promise<void>}
 */
export async function deployProject(
  stage: Stage = Stage.Production,
  project: ERC20Project,
  usersAndAmounts: UsersAndAmounts,
  dataContract: UnergyData,
  logicContract: UnergyLogicOperation,
  fundingContract: UnergyLogicFunding,
  stableCoinContract: ERC20StableCoin,
  meter: Wallet | Signer,
  stableDecimals: number,
  lifeTime: number = 1000,
  pWattPrice: BigInt = BigInt(10 ** 6),
  meterMultiplier: number = 1
): Promise<void> {
  const errMsg = (c: string) => `A ${c} instance must be provided`;

  if (!meter) throw new Error(errMsg('Meter'));
  if (!project) throw new Error(errMsg('Project'));
  if (!dataContract) throw new Error(errMsg('Data'));
  if (!logicContract) throw new Error(errMsg('Logic'));
  if (!fundingContract) throw new Error(errMsg('Funding'));
  if (!stableCoinContract) throw new Error(errMsg('ERC20StableCoin'));
  if (usersAndAmounts.users.length === 0)
    throw new Error('You must provide at least one user');
  if (
    usersAndAmounts.amounts.length !== 0 &&
    usersAndAmounts.users.length !== usersAndAmounts.amounts.length
  )
    throw new Error(
      'If you pass users and amounts array, they must have the same number of elements'
    );

  if (stage === Stage.NewlyCreated) return;

  const projectPWatts = (await dataContract.getProject(project.address)).tokens
    .pWatts;
  const amountToDistribute = projectPWatts.div(usersAndAmounts.users.length);

  for (let i = 0; i < usersAndAmounts.users.length; i++) {
    //Generate user buyTicket
    await dataContract.generateBuyTicket(
      project.address,
      pWattPrice.toString(),
      stableCoinContract.address,
      lifeTime,
      usersAndAmounts.users[i],
      { gasLimit: 5000000 }
    );

    await logicContract.pWattsTransfer(
      project.address,
      usersAndAmounts.users[i],
      usersAndAmounts.amounts[i] ?? amountToDistribute,
      { gasLimit: 5000000 }
    );
  }

  const reservePWatts = await project.balanceOf(logicContract.address);
  console.log('Reserve PWatts');
  console.log(reservePWatts.toString());

  if (stage === Stage.PWattTrasnfer) return;

  const milestones: DataStructure.MilestoneStruct[] = [
    {
      name: 'Desarrollo de ingeniería de detalle del proyecto',
      state: false,
      weight: 1000,
      installerSignature: false,
      unergySignature: false,
    },
    {
      name: 'Anticipo de paneles, inversores y estructuras',
      state: false,
      weight: 2000,
      installerSignature: false,
      unergySignature: false,
    },
    {
      name: 'Equipos en sitio',
      state: false,
      weight: 2000,
      installerSignature: false,
      unergySignature: false,
    },
    {
      name: 'Realizar instalación del proyecto',
      state: false,
      weight: 3000,
      installerSignature: false,
      unergySignature: false,
    },
    {
      name: 'Obtener certificaciones y pólizas',
      state: false,
      weight: 1000,
      installerSignature: false,
      unergySignature: false,
    },
    {
      name: 'Realizar proceso con Operador de Red',
      state: false,
      weight: 1000,
      installerSignature: false,
      unergySignature: false,
    },
  ];

  await fundingContract.createMilestonesBatch(project.address, milestones, {
    gasLimit: 5000000,
  });

  if (stage === Stage.Milestones) return;

  for (let i = 0; i <= milestones.length; i++) {
    await fundingContract.setInstallerSign(project.address, {
      gasLimit: 5000000,
    });
  }

  if (stage === Stage.Installed) return;

  //Unergy signature and change project 1 state to: PRODUCTION
  for (let i = 0; i <= milestones.length; i++) {
    await fundingContract.setOriginatorSign(
      project.address,
      stableCoinContract.address,
      i,
      { gasLimit: 5000000 }
    );
  }

  if (stage === Stage.Production) return;

  await logicContract.swapToken(project.address, { gasLimit: 5000000 });

  if (stage === Stage.Swap) return;

  // firstMeter1AccEnergy have 6 decimals for mint ERC-1155
  const firstMeter1AccEnergy = 100000n * BigInt(meterMultiplier);
  await logicContract
    .connect(meter)
    .energyReport(project.address, firstMeter1AccEnergy, { gasLimit: 5000000 });

  //secondMeter1AccEnergy have 6 decimals for mint ERC-1155
  const secondMeter1AccEnergy = 106765000000000000000n * BigInt(meterMultiplier);
  const deltaEnergy = secondMeter1AccEnergy - firstMeter1AccEnergy; //Scalated by 1 gwei
  await logicContract
    .connect(meter)
    .energyReport(project.address, secondMeter1AccEnergy, {
      gasLimit: 5000000,
    });

  if (stage === Stage.EnergyReporting) return;

  //Invoice report - payed energy delta = 100;
  const reportEnergyDelta = deltaEnergy / 2n;
  const energyTariff = 100n; //1USD
  const IncomeAmount = reportEnergyDelta * energyTariff * 10000000000n;
  const usdDepreciated = BigInt(10 ** stableDecimals); //project 1 reported depreciation: 1%
  await logicContract.invoiceReport(
    project.address,
    stableCoinContract.address,
    IncomeAmount,
    reportEnergyDelta,
    energyTariff,
    usdDepreciated,
    { gasLimit: 5000000 }
  );
}
