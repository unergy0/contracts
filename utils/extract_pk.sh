#!/bin/bash

data=$(cat .env)

# Find all lines that contains "_PUBLIC_KEY = "
publicKeys=$(echo "$data" | grep '_PUBLIC_KEY = ".*"$')

# Extract the public keys and join them into a string
publicKeyString=$(echo "$publicKeys" | awk -F'"' '{print $2}' | tr '\n' '\n')

echo "$publicKeyString" > publicKeys.txt
echo "Public keys saved to publicKeys.txt"

