import { createHTTPServer } from "@trpc/server/adapters/standalone";
import { z } from "zod";
import { keysDb } from "./db";
import { publicProcedure, router } from "./trpc";
import cors from "cors";

const appRouter = router({
  sendKey: publicProcedure
    .input(z.object({ encryptedKey: z.string() }))
    .mutation(async (opts) => {
      const { input } = opts;
      keysDb.key.sendKey(input);

      return "ok";
    }),
});

export type AppRouter = typeof appRouter;

export const server = createHTTPServer({
  middleware: cors(),
  router: appRouter,
});
