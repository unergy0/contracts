import { AdminClient } from "defender-admin-client";
import dotenv from "dotenv";

dotenv.config();

const { OZ_DEFENDER_API_KEY, OZ_DEFENDER_SECRET } = process.env;

export const client = new AdminClient({
  apiKey: OZ_DEFENDER_API_KEY || "",
  apiSecret: OZ_DEFENDER_SECRET || "",
});
