import { Wallet } from "ethers";
import { ethers, network } from "hardhat";
import { BigNumberish } from "@ethersproject/bignumber/src.ts";
import { maxUintHex } from "./constants";

/**
 * @notice If no `type` is provided the default is `address`
 * @returns A random address or wallet or an array of random addresses or wallets
 */
function getRandom<T>(opts?: {
  amount?: number;
  type?: "wallet" | "address";
}): string | Wallet | (string | Wallet)[] {
  if (opts?.amount) {
    return Array.from({ length: opts.amount }, () =>
      opts.type === "wallet"
        ? ethers.Wallet.createRandom()
        : ethers.Wallet.createRandom().address
    );
  }

  return opts?.type === "wallet"
    ? ethers.Wallet.createRandom()
    : ethers.Wallet.createRandom().address;
}

const getSignerWithBalance = async (amount?: BigNumberish) => {
  const rdnUser = await ethers.getImpersonatedSigner(
    ethers.Wallet.createRandom().address
  );

  if (network.name === "hardhat" || network.name === "localhost") {
    await network.provider.send("hardhat_setBalance", [
      rdnUser.address,
      amount || maxUintHex,
    ]);
  }

  if (network.name === "polygon_fork") {
    await network.provider.send("tenderly_setBalance", [
      [rdnUser.address],
      amount || maxUintHex,
    ]);
  }

  return rdnUser;
};

export const wallet = {
  getRandom,
  getSignerWithBalance,
};
