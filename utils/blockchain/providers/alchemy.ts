import { Alchemy, Network, Utils } from "alchemy-sdk";

// Optional Config object, but defaults to demo api-key and eth-mainnet.
const settings = {
  apiKey: process.env.ALCHEMY_API_KEY,
  network: Network.MATIC_MUMBAI,
};

export const alchemy = new Alchemy(settings);

export class Interface extends Utils.Interface {
  constructor(abi: string) {
    super(abi);
  }
}
