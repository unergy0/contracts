import { BigNumberish } from "@ethersproject/bignumber/src.ts/bignumber";

interface Erc20Mintable {
  mint(to: string, amount: BigNumberish): Promise<any>;
  approve(spender: string, amount: BigNumberish): Promise<any>;
  //
  populateTransaction: {
    approve(spender: string, amount: BigNumberish): Promise<any>;
    mint(to: string, amount: BigNumberish): Promise<any>;
  };
}

export function erc20Communicator(erc20: Erc20Mintable) {
  async function mint(
    to: string,
    amount: BigNumberish,
    opts?: { send: boolean }
  ) {
    if (opts?.send) {
      const tx = await erc20.mint(to, amount);
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await erc20.populateTransaction.mint(to, amount);

    return {
      fName: "mint",
      data: tx.data,
    };
  }

  async function approve(
    spender: string,
    amount: BigNumberish,
    opts?: { send: boolean }
  ) {
    if (opts?.send) {
      const tx = await erc20.approve(spender, amount);
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await erc20.populateTransaction.approve(spender, amount);

    return {
      fName: "approve",
      data: tx.data,
    };
  }

  return {
    mint,
    approve,
  };
}
