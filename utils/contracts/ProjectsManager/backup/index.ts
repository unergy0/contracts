import { ethers } from "hardhat";
import { ProjectsManager } from "../../../../typechain-types";
import {
  projectsManagerCommunicator,
  projectsManagerInstance,
} from "../projectsManager";

let projectsManagerStorage = new Map<string, any>();

async function main() {
  const pm = await projectsManagerInstance();
  const pmCom = projectsManagerCommunicator(pm);

  // @todo
  // const unergyData = await pm.unergyData();
  // projectsManagerStorage.set("unergyData", unergyData);

  const potencialOwner = await pm.potentialOwner();
  projectsManagerStorage.set("potentialOwner", potencialOwner);

  const projectsAddresses = await pmCom.getCreatedProjectsAddress();
  projectsManagerStorage.set("projectsAddresses", projectsAddresses);

  const projects = await getProjects(pm, projectsAddresses);
  projectsManagerStorage.set("projects", projects);

  const projectsExists = await getProjectsExists(pm, projectsAddresses);
}

async function getProjects(pm: ProjectsManager, projectsAddresses: string[]) {
  const projects = await Promise.all(
    projectsAddresses.map(async (address) => {
      const project = await pm.getProject(address);
      return project;
    })
  );

  return projects;
}

async function getProjectsExists(
  pm: ProjectsManager,
  projectsAddresses: string[]
) {
  
}

function readPrivateMapping(mappingSlot: number) {


    
}

main();
