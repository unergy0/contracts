import { UnergyData } from "../../typechain-types";
import { PromiseOrValue } from "../../typechain-types/common";
import { UnergyEventVersion } from "../../types/contracts.types";

export function unergyDataCommunicator(unergyData: UnergyData) {
  async function setExternalHolderAddress(
    projectAdminAddress: PromiseOrValue<string>,
    projectAddress: string,
    isExternalHolder: boolean,
    opts?: {
      send: boolean;
    }
  ) {
    if (opts?.send) {
      const tx = await unergyData.setExternalHolderAddress(
        projectAdminAddress,
        projectAddress,
        isExternalHolder
      );
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await unergyData.populateTransaction.setExternalHolderAddress(
      projectAdminAddress,
      projectAddress,
      isExternalHolder
    );

    return {
      fName: "setExternalHolderAddress",
      data: tx.data,
    };
  }

  async function setProjectsManagerAddr(
    address: string,
    opts?: { send: boolean }
  ) {
    if (opts?.send) {
      const tx = await unergyData.setProjectsManagerAddr(address);
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await unergyData.populateTransaction.setProjectsManagerAddr(
      address
    );

    return {
      fName: "setProjectsManagerAddr",
      data: tx.data,
    };
  }

  async function setUnergyBuyerAddr(address: string, opts?: { send: boolean }) {
    if (opts?.send) {
      const tx = await unergyData.setUnergyBuyerAddr(address);
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await unergyData.populateTransaction.setUnergyBuyerAddr(address);

    return {
      fName: "setUnergyBuyerAddr",
      data: tx.data,
    };
  }

  async function setUnergyLogicReserveAddr(
    address: string,
    opts?: { send: boolean }
  ) {
    if (opts?.send) {
      const tx = await unergyData.setUnergyLogicReserveAddr(address);
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await unergyData.populateTransaction.setUnergyLogicReserveAddr(
      address
    );

    return {
      fName: "setUnergyLogicReserveAddr",
      data: tx.data,
    };
  }

  async function setUWattAddr(address: string, opts?: { send: boolean }) {
    if (opts?.send) {
      const tx = await unergyData.setUWattAddr(address);
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await unergyData.populateTransaction.setUWattAddr(address);

    return {
      fName: "setUWattAddr",
      data: tx.data,
    };
  }

  async function setCleanEnergyAssetsAddr(
    address: string,
    opts?: { send: boolean }
  ) {
    if (opts?.send) {
      const tx = await unergyData.setCleanEnergyAssetsAddr(address);
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await unergyData.populateTransaction.setCleanEnergyAssetsAddr(
      address
    );

    return {
      fName: "setCleanEnergyAssetsAddr",
      data: tx.data,
    };
  }

  async function setUnergyEventAddr(
    address: string,
    version: UnergyEventVersion,
    opts?: { send: boolean }
  ) {
    if (opts?.send) {
      const tx = await unergyData.setUnergyEventAddr(address, version);
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await unergyData.populateTransaction.setUnergyEventAddr(
      address,
      version
    );

    return {
      fName: "setUnergyEventAddr",
      data: tx.data,
    };
  }

  return {
    setExternalHolderAddress,
    setProjectsManagerAddr,
    setUnergyBuyerAddr,
    setUnergyLogicReserveAddr,
    setUWattAddr,
    setCleanEnergyAssetsAddr,
    setUnergyEventAddr,
  };
}
