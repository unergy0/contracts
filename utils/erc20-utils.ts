import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { getTimestampInSeconds } from "./helpers";

async function permitAndTransfer(
  erc20Permit: any,
  owner: SignerWithAddress,
  spender: string,
  value: bigint,
  deadline: number = getTimestampInSeconds() + 4200
) {
  await permit(erc20Permit, owner, spender, value, deadline);
  await erc20Permit.transferFrom(owner.address, spender, value);
}

/**
 * @param erc20Permit Must be an instance of an ERC20Permit compatible contract
 * @param owner The owner of the tokens
 * @param spender The address that will spend the tokens
 * @param value The value to approve
 * @param deadline The deadline to the sign to be valid
 */
async function permit(
  erc20Permit: any,
  owner: SignerWithAddress,
  spender: string,
  value: bigint,
  deadline: number = getTimestampInSeconds() + 4200
) {
  const nonce = await erc20Permit.nonces(owner.address);
  const domain = {
    name: await erc20Permit.name(),
    version: "1",
    chainId: 31337,
    verifyingContract: erc20Permit.address,
  };
  const types = {
    Permit: [
      { name: "owner", type: "address" },
      { name: "spender", type: "address" },
      { name: "value", type: "uint256" },
      { name: "nonce", type: "uint256" },
      { name: "deadline", type: "uint256" },
    ],
  };
  const values = {
    owner: owner.address,
    spender,
    value,
    nonce,
    deadline,
  };
  const signedTypedData = await owner._signTypedData(domain, types, values);
  const { v, r, s } = ethers.utils.splitSignature(signedTypedData);

  await erc20Permit.permit(owner.address, spender, value, deadline, v, r, s);
}

export default { permit, permitAndTransfer };
