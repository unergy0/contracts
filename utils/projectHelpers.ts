import { Wallet, Signer, BigNumber } from "ethers";
import { BigNumberish } from "@ethersproject/bignumber/src.ts";
import { ContractTransaction } from "@ethersproject/contracts/src.ts";
import {
  ERC20Project,
  ERC20StableCoin,
  ProjectsManager,
  UnergyData,
  UnergyBuyer,
  UnergyLogicReserve,
  ERC20UWatt,
} from "../typechain-types";
import { parseUnits } from "../utils";
import { ProjectState } from "../types/contracts.types";
import { wait } from "./helpers";
import { WAD } from "./constants";

interface UsersAndAmounts {
  users: string[];
  amounts: bigint[];
}

export class ProjectHelper {
  constructor(
    private readonly dataContract: UnergyData,
    private readonly projectsManager: ProjectsManager,
    private readonly logicContract: UnergyLogicReserve,
    private readonly fundingContract: UnergyBuyer,
    private readonly stableCoinContract: ERC20StableCoin,
    private readonly uWattToken: ERC20UWatt
  ) {}

  public async bringToProduction(
    projectContract: ERC20Project,
    usersAndAmounts: UsersAndAmounts,
    meter: Wallet | Signer,
    _energyTariff?: bigint,
    _projectPresentValue?: bigint,
    _stableDecimals?: number,
    _meterMultiplier?: number,
    _reportsAmounts?: bigint[],
    _expiration?: number,
    _pWattPrice?: BigNumberish,
    _callSwapDirectly?: boolean
  ) {
    return await this.generateInvoiceReport(
      projectContract,
      usersAndAmounts,
      meter,
      _energyTariff,
      _projectPresentValue,
      _stableDecimals,
      _meterMultiplier,
      _reportsAmounts,
      _expiration,
      _pWattPrice,
      _callSwapDirectly
    );
  }

  public async generateInvoiceReport(
    projectContract: ERC20Project,
    usersAndAmounts: UsersAndAmounts,
    meter: Wallet | Signer,
    _energyTariff?: bigint,
    _projectPresentValue?: bigint,
    _stableDecimals?: number,
    _meterMultiplier?: number,
    _reportsAmounts?: bigint[],
    _expiration?: number,
    _pWattPrice?: BigNumberish,
    _callSwapDirectly?: boolean
  ): Promise<{
    invoiceReportTx: ContractTransaction;
    energyDelta: bigint;
    toPayEnergyDelta: bigint;
    energyTariff: bigint;
    projectPresentValue: bigint | BigNumber;
    incomeAmount: bigint;
  }> {
    const { energyDelta } = await this.reportEnergy(
      projectContract,
      usersAndAmounts,
      meter,
      _stableDecimals,
      _meterMultiplier,
      _reportsAmounts,
      _expiration,
      _pWattPrice,
      _callSwapDirectly
    );

    const stableDecimals =
      _stableDecimals || (await this.stableCoinContract.decimals());

    const project = await this.projectsManager.getProject(
      projectContract.address
    );

    const projectDecimals = await projectContract.decimals();
    const toPayEnergyDelta = energyDelta / BigInt(2);

    const projectPresentValue =
      _projectPresentValue ||
      project.currentProjectValue.sub(BigInt(10 ** stableDecimals));
    const energyTariff = _energyTariff || BigInt(2 * 10 ** stableDecimals);
    const incomeAmount =
      (toPayEnergyDelta * energyTariff) / BigInt(10 ** projectDecimals);

    const invoiceReportTx = await this.logicContract.invoiceReport(
      projectContract.address,
      toPayEnergyDelta,
      energyTariff,
      projectPresentValue
    );

    return {
      invoiceReportTx,
      energyDelta,
      toPayEnergyDelta,
      energyTariff,
      projectPresentValue,
      incomeAmount,
    };
  }

  public async reportEnergy(
    projectContract: ERC20Project,
    usersAndAmounts: UsersAndAmounts,
    meter: Wallet | Signer,
    _stableDecimals?: number,
    _meterMultiplier?: number,
    _reportsAmounts?: bigint[],
    _expiration?: number,
    _pWattPrice?: BigNumberish,
    _callSwapDirectly?: boolean
  ) {
    const meterMultiplier = _meterMultiplier || 1;
    const projectDecimals = await projectContract.decimals();

    const reportsAmounts = _reportsAmounts || [
      BigInt(100_000 * 10 ** projectDecimals),
      BigInt(200_000 * 10 ** projectDecimals),
    ];

    if (reportsAmounts.length > 2) {
      throw new Error(`The function supports only 2 reports.`);
    }

    await this.swapToken(
      projectContract,
      usersAndAmounts,
      _expiration,
      _pWattPrice,
      _callSwapDirectly
    );

    this.validateBigIntArray(reportsAmounts.map((a) => a));
    for (let index = 0; index < reportsAmounts.length; index++) {
      const energyToReport = reportsAmounts[index] * BigInt(meterMultiplier);

      await this.logicContract
        .connect(meter)
        .energyReport(projectContract.address, energyToReport);
    }

    return {
      energyDelta: reportsAmounts[1] - reportsAmounts[0],
    };
  }

  public async swapToken(
    projectContract: ERC20Project,
    usersAndAmounts: UsersAndAmounts,
    _expiration?: number,
    _pWattPrice?: BigNumberish,
    _callSwapDirectly?: boolean
  ) {
    await this.setInstallerSign(
      projectContract,
      usersAndAmounts,
      _expiration,
      _pWattPrice
    );

    const projectUsers = await this.projectsManager.getProjectHolders(
      projectContract.address
    );

    if (_callSwapDirectly) {
      await this.logicContract.swapToken(
        projectContract.address,
        projectUsers.length
      );
    } else {
      await this.logicContract.requestStandardSwap(
        projectContract.address,
        projectUsers.length
      );
    }

    let projectState = await this.projectsManager.getProjectState(
      projectContract.address
    );

    let secondsWaited = 0;

    while (projectState !== ProjectState.PRODUCTION) {
      if (secondsWaited > 60)
        throw new Error(
          "Timeout reached while waiting to project state to be in production"
        );

      await wait(1, { log: false });

      projectState = await this.projectsManager.getProjectState(
        projectContract.address
      );

      secondsWaited++;
    }
  }

  public async customSwap(
    projectContract: ERC20Project,
    userAddress: string,
    swapFactor: bigint,
    callDirectly: boolean = false
  ) {
    if (callDirectly) {
      await this.logicContract.customSwap(
        projectContract.address,
        userAddress,
        swapFactor
      );
    } else {
      await this.logicContract.requestCustomSwap(
        projectContract.address,
        userAddress,
        swapFactor
      );

      let userBalance = await projectContract.balanceOf(userAddress);

      let secondsWaited = 0;
      while (userBalance.toBigInt() > 0n) {
        if (secondsWaited > 60) {
          throw new Error(
            "Timeout reached while waiting to user balance to be 0"
          );
        } else {
          const waitTime = 10;
          await wait(waitTime, { log: false });
          userBalance = await projectContract.balanceOf(userAddress);
          secondsWaited += waitTime;
        }
      }
    }
  }

  public async setInstallerSign(
    projectContract: ERC20Project,
    usersAndAmounts: UsersAndAmounts,
    _expiration?: number,
    _pWattPrice?: BigNumberish
  ) {
    await this.transferPWattToUsers(
      projectContract,
      usersAndAmounts,
      _expiration,
      _pWattPrice
    );

    const projectMilestones = await this.projectsManager.getProjectMilestones(
      projectContract.address
    );

    for (let i = 0; i < projectMilestones.length; i++) {
      await this.fundingContract.setInstallerSign(projectContract.address);
    }

    for (let i = 0; i < projectMilestones.length; i++) {
      await this.fundingContract.setOriginatorSign(projectContract.address, i);
    }
  }

  public async transferPWattToUsers(
    projectContract: ERC20Project,
    usersAndAmounts: UsersAndAmounts,
    expiration: number = 100_000,
    _pWattPrice?: BigNumberish
  ) {
    if (usersAndAmounts.users.length === 0) {
      throw new Error("You must provide at least one user");
    }

    if (
      usersAndAmounts.amounts.length !== 0 &&
      usersAndAmounts.users.length !== usersAndAmounts.amounts.length
    ) {
      throw new Error(
        "If you pass users and amounts array, they must have the same number of elements"
      );
    }

    const stableCoinDecimals = await this.stableCoinContract.decimals();
    let pWattPrice;
    if (_pWattPrice !== null && _pWattPrice !== undefined) {
      pWattPrice = _pWattPrice;
    } else {
      pWattPrice = parseUnits(1.3, stableCoinDecimals);
    }

    const projectStruct = await this.projectsManager.getProject(
      projectContract.address
    );
    const pWattsSupplyAvailable = await projectContract.balanceOf(
      projectStruct.adminAddr
    );

    const usersAndAmountsBig = BigInt(usersAndAmounts.users.length);

    const userAmount = pWattsSupplyAvailable.div(usersAndAmountsBig);

    for (let i = 0; i < usersAndAmounts.users.length; i++) {
      await this.dataContract.generatePurchaseTicket(
        projectContract.address,
        pWattPrice,
        expiration,
        usersAndAmounts.users[i]
      );

      if (!usersAndAmounts.amounts || usersAndAmounts.amounts.length === 0) {
        await this.logicContract.pWattsTransfer(
          projectContract.address,
          usersAndAmounts.users[i],
          userAmount
        );
      } else {
        await this.logicContract.pWattsTransfer(
          projectContract.address,
          usersAndAmounts.users[i],
          usersAndAmounts.amounts[i]
        );
      }
    }

    const pWattsSupplyAvailableAfterSwap = (
      await projectContract.balanceOf(projectStruct.adminAddr)
    ).toBigInt();

    if (pWattsSupplyAvailableAfterSwap > 100) {
      await this.dataContract.generatePurchaseTicket(
        projectContract.address,
        pWattPrice,
        expiration,
        usersAndAmounts.users[0]
      );

      await this.logicContract.pWattsTransfer(
        projectContract.address,
        usersAndAmounts.users[0],
        pWattsSupplyAvailableAfterSwap
      );
    }
  }

  public async buyPWattsFrom(
    userFrom: string,
    projectAddress: string,
    amount: BigNumberish,
    ticketOpts: {
      pWattPrice?: BigNumberish;
      expiration?: number;
    } = {
      pWattPrice: BigInt(1e6),
      expiration: 100_000,
    }
  ) {
    await this.generateTicket(
      userFrom,
      projectAddress,
      ticketOpts.pWattPrice,
      ticketOpts.expiration
    );
    await this.logicContract.buyPWattsFrom(userFrom, projectAddress, amount);
  }

  public async generateTicket(
    user: string,
    projectAddress: string,
    pWattPrice: BigNumberish = BigInt(1e6),
    expiration: number = 100_000
  ) {
    await this.dataContract.generatePurchaseTicket(
      projectAddress,
      pWattPrice,
      expiration,
      user
    );
  }

  public async getFeeAmount(amount: BigNumber) {
    const feePerc = await this.dataContract.swapFeePercentage();
    const feeAmount = amount.mul(feePerc).div(WAD).div(100);
    return feeAmount;
  }

  private validateBigIntArray(array: BigInt[]) {
    for (let i = 1; i < array.length; i++) {
      if (array[i] <= array[i - 1]) {
        throw new Error(
          `Element at index ${i} is not greater than the previous element. The transaction will fail`
        );
      }
    }
  }
}
