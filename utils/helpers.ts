import hre, { ethers, network } from "hardhat";
import { BigNumber, Wallet } from "ethers";
import { BigNumberish } from "@ethersproject/bignumber/src.ts/bignumber";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";
import { ContractReceipt } from "@ethersproject/contracts/src.ts/index";
import { time } from "@nomicfoundation/hardhat-network-helpers";

import {
  MilestoneStruct,
  ProjectStruct,
  ProjectStructOutput,
  ProjectsManager,
} from "../typechain-types/contracts/ProjectsManager";
import { ERC20 } from "../typechain-types";
import { logger } from "./logger";
import { input } from "@inquirer/prompts";

export const toBn = ethers.BigNumber.from;

export function parseProject(project: ProjectStructOutput): ProjectStruct {
  return {
    id: project.id,
    maintenancePercentage: project.maintenancePercentage,
    initialProjectValue: project.initialProjectValue,
    currentProjectValue: project.currentProjectValue,
    swapFactor: project.swapFactor,
    state: project.state,
    addr: project.addr,
    adminAddr: project.adminAddr,
    installerAddr: project.installerAddr,
    usdDepreciated: project.usdDepreciated,
    pWattsSupply: project.pWattsSupply,
    signatures: {
      isSignedByInstaller: project.signatures.isSignedByInstaller,
      isSignedByOriginator: project.signatures.isSignedByOriginator,
    },
    originator: project.originator,
    originatorFee: project.originatorFee,
    stableAddr: project.stableAddr,
  };
}

/**
 * 1. We filter all the events with the event name ProjectCreated, and store
 *    the result in projectCreatedEvents.
 * 2. We map all the projectCreatedEvents to return the createdProjectAddress.
 *    We use the decode function to decode the data and topics of the event.
 *    We pass the data and topics of the event as arguments to the decode
 *    function to parse the data and topics to the correct types. The decode
 *    function returns an array of arguments of the event. Since we just want
 *    the address of the created project, we return the first element of the
 *    array.
 * @notice This function assumes that the receipt has a ProjectCreated event.
 * @param receipt The receipt of the transaction that created the project.
 * @returns The address/es of the created project.
 */
export function getAddressFromReceipt(receipt: ContractReceipt): string[] {
  let projectCreatedEvents = receipt.events?.filter(
    (e) => e.event === "ProjectCreated"
  );

  if (projectCreatedEvents?.length === 0) {
    projectCreatedEvents = receipt.events?.filter(
      (e) => e.event === "PositionCreated"
    );
  }

  if (projectCreatedEvents?.length === 0) {
    throw new Error("No event found");
  }

  const createdProjectAddress = projectCreatedEvents?.map((e) => {
    return e.decode?.(e.data, e.topics)![0];
  });

  return createdProjectAddress!;
}

export function parseMilestone(milestone: any): MilestoneStruct {
  return {
    name: milestone.name,
    weight: milestone.weight,
    isReached: milestone.isReached,
    wasSignedByInstaller: milestone.wasSignedByInstaller,
    wasSignedByOriginator: milestone.wasSignedByOriginator,
  };
}

export const projectAt = (address: string) => {
  return ethers.getContractAt("ERC20Project", address);
};

/**
 * @param amount Amount of ERC20 compatible tokens to parse to wei.
 * @returns Returns the amount of tokens with 18 decimals.
 */
export const parseEther = (amount: number) => {
  return ethers.utils.parseEther(amount.toString());
};

/**
 * @returns Returns the amount of tokens with the specified decimals.
 */
export const parseUnits = (amount: number | string, decimals: number) => {
  if (typeof amount === "string") {
    return ethers.utils.parseUnits(amount, decimals);
  } else {
    return ethers.utils.parseUnits(amount.toString(), decimals);
  }
};

/**
 * Transfer an ERC20 compatilble token to an address and then, that address made an approve over another address.
 * This function assumes that the contractFrom has enough balance to transfer.
 * @param contractFrom The ERC20 compatible token contract to transfer from.
 * @param accountFrom The account to transfer from. This account must have enough balance.
 * @param accountTo The account to transfer to.
 * @param approveTo The account to approve the transfer to.
 * @param amount The amount of tokens to transfer.
 */
export const transferAndApprove = async (
  contractFrom: ERC20,
  accountFrom: SignerWithAddress,
  accountTo: SignerWithAddress,
  approveTo: string,
  amount: BigNumberish
): Promise<ContractReceipt> => {
  return contractFrom
    .connect(accountFrom)
    .transfer(accountTo.address, amount)
    .then(() => {
      return contractFrom.connect(accountTo).approve(approveTo, amount);
    })
    .then((tx: any) => {
      return tx.wait();
    });
};

export const calcMaxPWattThatCanBuyTheUnergyBuyer = (
  pWattDecimals: number,
  project: ProjectStructOutput,
  unergyLogicReserveStableBalance: BigNumber,
  checkForSupply: boolean = true
): BigNumber => {
  /**
   * as the amount of stable pWatts (y) the reserve will have to pay for is:
   *
   * `y = (Pa + Pp) / Pd`,
   *
   * where
   * - Pa: is the amount of pWatts it is buying;
   * - Pp: is the price of the pWatt; and
   * - Pd: are the decimals of the pWatt,
   * then the maximum pWatts (m) it can buy with a balance `x` is:
   *
   * `m = (x * Pd) / Pp`.
   *
   */
  const m = unergyLogicReserveStableBalance
    .mul(parseUnits(1, pWattDecimals))
    .div(ethers.utils.parseUnits("1.3", 6));

  if (checkForSupply) {
    if (project.pWattsSupply.sub(project.originatorFee).div(4) < m) {
      return project.pWattsSupply.div(4);
    } else {
      return m;
    }
  } else {
    return m;
  }
};

export const randomUsers = async (min: number = 11, max: number = 19) => {
  const [...signers] = await ethers.getSigners();
  const maxRnd = Math.floor(Math.random() * (max - min + 1) + min);

  const users: string[] = [];
  // This is to avoid the first 30 users
  for (let index = 30; index < maxRnd + 30; index++) {
    users.push(signers[index].address);
  }

  return users;
};

/**
 *
 * @param seconds Seconds to wait.
 */
export function wait(
  seconds: number,
  options: {
    log: boolean;
  } = {
    log: true,
  }
): Promise<void> {
  if (seconds <= 0) return Promise.resolve();
  if (options?.log) logger.log(`Waiting ${seconds} seconds...`);

  return new Promise((resolve) => setTimeout(resolve, 1000 * seconds));
}

export async function pressEnterToContinue(message?: string) {
  await input({
    message: `Press enter to continue ${message ? `(${message})` : ""}`,
  });
}

export function getTimestampInSeconds() {
  return Math.floor(Date.now() / 1000);
}

export async function createMilestone(
  prjAddr: string,
  name: string,
  weight: number,
  options: {
    signer?: SignerWithAddress | Wallet;
    projectsManager: ProjectsManager;
  }
) {
  let tx;

  if (options?.signer) {
    tx = await options.projectsManager
      .connect(options.signer)
      .addProjectMilestone(prjAddr, name, weight);
  } else {
    tx = await options.projectsManager.addProjectMilestone(
      prjAddr,
      name,
      weight
    );
  }

  const receipt = await tx.wait();

  if (receipt.confirmations > 0) {
    logger.log(`Milestone "${name}" created`);
  } else {
    logger.error(`Milestone "${name}" not created. Retrying`);
    await createMilestone(prjAddr, name, weight, {
      projectsManager: options.projectsManager,
    });
  }
}

type ERC20Mintable = ERC20 & {
  mint: (address: string, amount: BigNumberish) => Promise<void>;
};

export async function dealAndApprove(
  token: ERC20 | ERC20Mintable,
  owner: SignerWithAddress,
  spender: string,
  amount: BigNumberish = parseUnits(1000, 6),
  opts: {
    parseUnits?: boolean;
    mint?: boolean;
  } = {
    parseUnits: true,
    mint: false,
  }
) {
  await deal(token, owner.address, amount, opts);
  await token.connect(owner).approve(spender, amount);
}

/**
 * @notice Set the balance of an account of the specified ERC20 compatible token.
 * @param token The ERC20 compatible token contract.
 * @param address The address of the account to set the balance.
 * @param amount The balance to set. The passed amount is parsed with the token decimals by default.
 *               use { parseUnits: false } to avoid parsing the amount.
 */
export async function deal(
  token: ERC20 | ERC20Mintable,
  address: string,
  amount: BigNumberish = parseUnits(1000, 6),
  opts: {
    parseUnits?: boolean;
    mint?: boolean;
  } = {
    parseUnits: true,
    mint: false,
  }
) {
  if (opts) {
    const { parseUnits } = opts;

    if (opts.mint) {
      await (token as ERC20Mintable).mint(
        address,
        parseUnits
          ? ethers.utils.parseUnits(amount.toString(), await token.decimals())
          : amount
      );
    } else {
      await token.transfer(
        address,
        parseUnits
          ? ethers.utils.parseUnits(amount.toString(), await token.decimals())
          : amount
      );
    }
  }
}

export function formatAccessControlError(caller: string, role: string) {
  return `AccessControl: account ${caller.toLowerCase()} is missing role ${role}`;
}

export async function getImpersonatedSigner(
  address: string
): Promise<SignerWithAddress> {
  if (!ethers.utils.isAddress(address)) throw new Error("Invalid address");
  await network.provider.request({
    method: "hardhat_impersonateAccount",
    params: [address],
  });
  return ethers.getSigner(address);
}

/**
 * @notice Use this function to verify a deployed contract
 * @param address The address of the contract to verify
 * @param contructorArgs The constructor arguments of the contract. Default: []
 * @param waitTime The time in seconds to wait before run the verification -this is usefull to ensure that the contract is deployed, progagated and indexed by the blockexplorer-. Default: 60
 */
export async function verify(
  address: string,
  contructorArgs: any[] = [],
  waitTime = 60
) {
  if (hre.network.name === "polygon" || hre.network.name === "polygon_mumbai") {
    //
    if (waitTime > 0) await wait(waitTime);
    //
    try {
      //
      await hre.run("verify:verify", {
        address,
        constructorArguments: contructorArgs,
      });
      //
    } catch (error) {
      //
      logger.error(
        `Unable to verify ${address} on ${hre.network.name}: ${error}`
      );
      //
    }
  }
}

type TimeUnit =
  | "second"
  | "minute"
  | "hour"
  | "day"
  | "week"
  | "month"
  | "year";

/**
 * Mines a new block whose timestamp is `amountInSecond` after the latest block's timestamp
 * @param amountInSeconds number of seconds to increase the next block's timestamp by
 * @returns the `timestamp` of the mined block
 */
export async function increaseTime(amountInSeconds: number) {
  return time.increase(amountInSeconds);
}

/**
 * Mines a new block whose timestamp is `timestamp`.
 * @param timestamp Must be bigger than the latest block's timestamp
 */
export async function increateTimeTo(timestamp: number) {
  await time.increaseTo(timestamp);
}

/**
 * @param number The number of units to calculate the time from.
 * @param unit The unit of the ´number´.
 * @returns The seconds equivalent from now to the future time.
 */
export function getTime(number: number, unit: TimeUnit): number {
  const now = new Date();

  // Calculate time difference based on the unit
  let timeDifference = 0;
  switch (unit) {
    case "second":
      timeDifference = number * 1000;
      break;
    case "minute":
      timeDifference = number * 60 * 1000;
      break;
    case "hour":
      timeDifference = number * 60 * 60 * 1000;
      break;
    case "day":
      timeDifference = number * 24 * 60 * 60 * 1000;
      break;
    case "week":
      timeDifference = number * 7 * 24 * 60 * 60 * 1000;
      break;
    case "month":
      let newMonth = now.getMonth() + number;
      const newYear = now.getFullYear() + Math.floor(newMonth / 12);
      newMonth %= 12;
      const newDate = new Date(newYear, newMonth, now.getDate());
      timeDifference = newDate.getTime() - now.getTime();
      break;
    case "year":
      timeDifference = number * 365 * 24 * 60 * 60 * 1000; // Approximate for simplicity
      break;
    default:
      throw new Error(`Invalid unit: ${unit}`);
  }

  // Calculate the future timestamp and return it
  const futureTime = now.getTime() + timeDifference;
  return futureTime;
}
