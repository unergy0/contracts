export function validateUsersAndAmounts(
  _: any,
  __: string,
  descriptor: PropertyDescriptor
) {
  const originalMethod = descriptor.value;

  descriptor.value = function (...args: any[]) {
    const [, usersAndAmounts] = args;

    if (usersAndAmounts.users.length === 0) {
      throw new Error('You must provide at least one user');
    }

    if (
      usersAndAmounts.amounts.length !== 0 &&
      usersAndAmounts.users.length !== usersAndAmounts.amounts.length
    ) {
      throw new Error(
        'If you pass users and amounts array, they must have the same number of elements'
      );
    }

    return originalMethod.apply(this, args);
  };

  return descriptor;
}
