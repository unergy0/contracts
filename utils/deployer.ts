import {
  AdminClient,
  Contract as DefenderContract,
} from "defender-admin-client";
import { ContractFactory, Wallet, Contract } from "ethers";
import { Network as DefenderNetworks } from "defender-base-client/lib/utils/network";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";

import { Reporter, reporter } from "./reporter";
import { client } from "./ozDefender";
import { Logger, logger } from "./logger";
import hre, { network, upgrades } from "hardhat";
import { getImplementationAddress } from "@openzeppelin/upgrades-core";
import { HardhatUpgrades } from "@openzeppelin/hardhat-upgrades";
import { wait } from "./helpers";

interface OZDefenderOptions {
  network?: DefenderNetworks;
  enable?: boolean;
}

interface DeployerStartOptions {
  disableReporting?: boolean;
  ozDefender?: OZDefenderOptions;
}

export class Deployer {
  private static instance: Deployer;
  private ozDefenderOptions: OZDefenderOptions = {
    network: "mumbai",
    enable: false,
  };

  private constructor(
    private readonly reporter: Reporter,
    private readonly defenderClient: AdminClient,
    private readonly logger: Logger,
    private readonly upgradesProvider: HardhatUpgrades,
    private readonly provider: any = hre.ethers.provider
  ) {}

  public static getInstance(): Deployer {
    if (!Deployer.instance) {
      Deployer.instance = new Deployer(reporter, client, logger, upgrades);
    }

    return Deployer.instance;
  }

  get reporterInstance(): Reporter {
    return this.reporter;
  }

  public start(options?: DeployerStartOptions): void {
    this.reporter.clear();
    this.logger.log("Starting deployment...");
    this.reporter.enable();

    if (options?.disableReporting) this.reporter.disable();
    if (options?.ozDefender) {
      this.ozDefenderOptions = options.ozDefender;
    }
  }

  public finish(): void {
    this.logger.log("Deployment finished ✅");
  }

  public async deployContract(
    name: string,
    factory: ContractFactory,
    args: any[] = [],
    signer: Wallet | SignerWithAddress | undefined = undefined,
    overrides: any = {}
  ): Promise<Contract | undefined> {
    let c: Contract | undefined;

    if (signer) {
      c = await this._deployContractWithSigner(
        factory,
        args,
        name,
        signer,
        overrides
      );
    } else {
      c = await this._deployContract(factory, args, name, overrides);
    }

    if (network.name === "polygon_mumbai" && c) {
      await wait(10);
      await this.verifyContract(c.address, args);
    }

    if (this.ozDefenderOptions.enable)
      if (c) await this.addContractToOZDefender(c, name);

    return c;
  }

  public async deployProxy(
    name: string = "",
    factory: ContractFactory,
    args: any[] = [],
    signer: Wallet | SignerWithAddress
  ): Promise<Contract | undefined> {
    try {
      const c = await this.upgradesProvider.deployProxy(factory, args, {
        kind: "uups",
      });
      await c.deployed();

      this.reporter.registerContractAddress(name, c.address, signer.address);
      this._logSuccessDeploy(name, c);

      if (network.name === "polygon_mumbai") {
        const implAddress = await this.getImplementationAddress(c.address);

        if (!implAddress) {
          logger.error(`Contract ${name} cannot be verified`);
        } else {
          await wait(10);
          await this.verifyContract(implAddress);
        }
      }

      if (this.ozDefenderOptions.enable) {
        await this.addContractToOZDefender(c, name);
      }

      return c;
    } catch (error) {
      this.logger.error(`Error deploying contract ${name}: ${error}`);
    }
  }

  async verifyContract(address: string, constructorArguments: any[] = []) {
    try {
      await hre.run("verify:verify", {
        address,
        constructorArguments,
      });
    } catch (error) {
      this.logger.error(`Error verifying contract ${address}: ${error}`);
    }
  }

  private async _deployContractWithSigner(
    factory: ContractFactory,
    args: any[] = [],
    name: string = "",
    signer: Wallet | SignerWithAddress,
    overrides: any = {}
  ) {
    try {
      const c = await factory.connect(signer).deploy(...args, overrides);
      await c.deployed();

      this.reporter.registerContractAddress(name, c.address, signer.address);
      this._logSuccessDeploy(name, c);

      return c;
    } catch (error) {
      this.logger.error(`Error deploying contract ${name}: ${error}`);
    }
  }

  private async _deployContract(
    factory: ContractFactory,
    args: any[] = [],
    name: string = "",
    overrides: any = {}
  ) {
    try {
      const c = await factory.deploy(...args, overrides);
      await c.deployed();

      this.reporter.registerContractAddress(name, c.address);
      this._logSuccessDeploy(name, c);

      return c;
    } catch (error) {
      this.logger.error(`Error deploying contract ${name}: ${error}`);
    }
  }

  async addContractToOZDefender(contract: Contract, name: string) {
    const c: DefenderContract = this._parseContractToDefenderContract(
      contract,
      name
    );

    try {
      this.defenderClient.addContract(c);
    } catch (error) {
      this.logger.error(
        `Error adding contract ${name} to OpenZeppelin Defender: ${error}`
      );
    }
  }

  async getImplementationAddress(proxyAddress: string) {
    const implAddress = await getImplementationAddress(
      this.provider,
      proxyAddress
    );

    return implAddress;
  }

  private _parseContractToDefenderContract(contract: Contract, name: string) {
    return {
      network: this.ozDefenderOptions.network || "mumbai",
      address: contract.address,
      name,
      abi: JSON.stringify(contract.interface.format("json")),
    };
  }

  private async _logSuccessDeploy(name: string, contract: any) {
    this.logger.log(`${name} deployed to ${contract.address} ✅`);
  }

  public saveReport(path?: string): void {
    this.reporter.saveDeploymentReport(path);
  }
}

export const deployer = Deployer.getInstance();
