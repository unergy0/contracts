import hre from "hardhat";

export function notEmpty(input: string) {
  if (input.length === 0) {
    return "Cannot be empty";
  }

  return true;
}

export function numberString(input: string) {
  if (isNaN(Number(input))) {
    return "Must be a number";
  }

  return true;
}

export function validAddress(input: string) {
  if (!hre.ethers.utils.isAddress(input)) {
    return "Must be a valid address";
  }

  return true;
}

export function validPrivateKey(input: string) {
  if (!hre.ethers.utils.isHexString(input)) {
    return "Must be a valid private key";
  }

  return true;
}
