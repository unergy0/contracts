import { ethers, network } from "hardhat";

import { reporter } from "../utils";
import { verify } from "../utils/helpers";
import { logger } from "../utils/logger";

(async () => {
  const contracts = require(`../data/${network.name}/full-deployment-report.json`);
  const deployer = (await ethers.getSigners())[0];

  const CONTRACT_NAME = "SwapHelper";

  const contractFactory = await ethers.getContractFactory(CONTRACT_NAME);

  const POOL_FEE = 500;
  const SWAP_ROUTER_ADDRESS = "0xE592427A0AEce92De3Edee1F18E0157C05861564";

  const contractInstance = await contractFactory.deploy(
    POOL_FEE,
    SWAP_ROUTER_ADDRESS,
    contracts["ERC20UWatt"].address,
    contracts["ERC20USDT"].address
  );
  const contract = await contractInstance.deployed();

  logger.log(`${CONTRACT_NAME} deployed to: ${contract.address}`);

  await verify(contract.address, [], 60);

  reporter.updateAddressInDeploymentReport(
    CONTRACT_NAME,
    contract.address,
    deployer.address
  );

  process.exit(0);
})();
