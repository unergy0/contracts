import { ethers, network } from "hardhat";
import { input, select } from "@inquirer/prompts";
import dotenv from "dotenv";

import { logger } from "../utils/logger";
import { provider } from "../utils/provider";
import { validAddress } from "../utils/validations";
import { killPortProcess } from "kill-port-process";
import { TRPC_SERVER_PORT } from "../utils/constants";
import { getAdminWallets } from "../utils/getAdminsWallets";
import deploymentReport from "../full-deployment-report.json";
import { MultiSignPermission } from "../utils/multiSignPermissions";

dotenv.config();

enum Roles {
  SuperAdmin = "SuperAdmin",
  Admin = "Admin",
  Mantainer = "Mantainer",
  ExternalClaimService = "ExternalClaimService",
  Meter = "Meter",
  Contract = "Contract",
  CustomAdmin = "CustomAdmin",
}

interface Permission {
  contractAddress: string;
  permission: string;
  roles: Roles[];
}

const factory = ethers.getContractFactory;

const {
  UnergyData,
  UnergyBuyer,
  UnergyEvent,
  ProjectsManager,
  CleanEnergyAssets,
  PermissionGranter,
  UnergyLogicReserve,
} = deploymentReport;

const operationCache = new Map<string, boolean>();
let failsCount = 0;

let address: string | null = null;
let role: string | null = null;

export const addressInputOptions = {
  message: "Address to set permissions for: ",
  validate: validAddress,
};

export const roleInputOptions = {
  message: `Role to set permissions for [${Object.values(Roles).join(", ")}]: `,
  choices: Object.values(Roles).map((role) => ({
    name: role,
    value: role,
  })),
};

let admin;
let admin2;
let admin3;
let multisignSigners = [];

async function main() {
  if (network.name === "localhost") {
    const signers = await ethers.getSigners();
    admin = signers[0];
    admin2 = signers[40];
    admin3 = signers[41];

    multisignSigners = [admin, admin2, admin3];
  } else {
    admin = new ethers.Wallet(
      process.env.ADMIN_PROD_PRIVATE_KEY ?? "",
      provider(network.name)
    );

    // const signers = await getAdminWallets(2, provider(network.name));
    // multisignSigners = [admin, ...signers];
  }

  if (!address) address = await input(addressInputOptions);
  if (!role) role = await select(roleInputOptions);

  if (!ethers.utils.isAddress(address as string))
    throw new Error(`Invalid address: ${address}`);

  const PermissionGranterFactory = await factory("PermissionGranter", admin);
  const permissionGranter = PermissionGranterFactory.attach(
    PermissionGranter.address
  );

  // const multisign = new MultiSignPermission(
  //   permissionGranter,
  //   multisignSigners
  // );

  const permissions: Permission[] = [
    // * ProjectsManager
    {
      contractAddress: ProjectsManager.address,
      permission: "createProject",
      roles: [Roles.Admin],
    },
    {
      contractAddress: ProjectsManager.address,
      permission: "updateProjectRelatedProperties",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: ProjectsManager.address,
      permission: "setSignature",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    // * uWatt
    // {
    //   contractAddress: ERC20UWatt.address,
    //   permission: "mint",
    //   roles: [],
    // },
    // * UnergyEvent
    {
      contractAddress: UnergyEvent.address,
      permission: "setUWattAddr",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyEvent.address,
      permission: "beforeTransferReceipt",
      roles: [Roles.Contract],
    },
    {
      contractAddress: UnergyEvent.address,
      permission: "afterTransferReceipt",
      roles: [Roles.Contract],
    },
    // * UnergyData
    {
      contractAddress: UnergyData.address,
      permission: "setProjectsManagerAddr",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyData.address,
      permission: "setUnergyBuyerAddr",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyData.address,
      permission: "setUnergyLogicReserveAddr",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyData.address,
      permission: "setUnergyEventAddr",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyData.address,
      permission: "setUWattAddr",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyData.address,
      permission: "setCleanEnergyAssetsAddr",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyData.address,
      permission: "generatePurchaseTicket",
      roles: [Roles.Admin, Roles.CustomAdmin],
    },
    {
      contractAddress: UnergyData.address,
      permission: "setPWattToTheReserve",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyData.address,
      permission: "setExternalHolderAddress",
      roles: [Roles.Admin, Roles.Mantainer, Roles.CustomAdmin],
    },
    {
      contractAddress: UnergyData.address,
      permission: "setAccEnergyByMeter",
      roles: [Roles.Admin],
    },
    {
      contractAddress: UnergyData.address,
      permission: "setDepreciationBalance",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyData.address,
      permission: "changePurchaseTicketUsed",
      roles: [Roles.Admin],
    },
    // * UnergyLogicReserve
    {
      contractAddress: UnergyLogicReserve.address,
      permission: "energyReport",
      roles: [Roles.Meter],
    },
    {
      contractAddress: UnergyLogicReserve.address,
      permission: "invoiceReport",
      roles: [Roles.Admin],
    },
    {
      contractAddress: UnergyLogicReserve.address,
      permission: "pWattsTransfer",
      roles: [Roles.Admin, Roles.CustomAdmin],
    },
    {
      contractAddress: UnergyLogicReserve.address,
      permission: "requestSwap",
      roles: [Roles.Admin],
    },
    {
      contractAddress: UnergyLogicReserve.address,
      permission: "swapToken",
      roles: [Roles.Admin, Roles.ExternalClaimService],
    },
    {
      contractAddress: UnergyLogicReserve.address,
      permission: "updateProjectRelatedProperties",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyLogicReserve.address,
      permission: "claimUWatts",
      roles: [Roles.Admin, Roles.ExternalClaimService],
    },
    // * UnergyBuyer
    {
      contractAddress: UnergyBuyer.address,
      permission: "setOriginatorSign",
      roles: [Roles.Admin],
    },
    {
      contractAddress: UnergyBuyer.address,
      permission: "setInstallerSign",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyBuyer.address,
      permission: "changeMilestoneName",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyBuyer.address,
      permission: "updateProjectRelatedProperties",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyBuyer.address,
      permission: "setSwapFactor",
      roles: [Roles.Admin],
    },
    {
      contractAddress: UnergyBuyer.address,
      permission: "withdrawUWatts",
      roles: [],
    },
    {
      contractAddress: UnergyBuyer.address,
      permission: "updateLastUWattStatus",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyBuyer.address,
      permission: "setProjectState",
      roles: [Roles.Admin, Roles.Mantainer],
    },
    {
      contractAddress: UnergyBuyer.address,
      permission: "setInitialProjectValue",
      roles: [Roles.Admin, Roles.CustomAdmin],
    },
    // * CleanEnergyAssets
    {
      contractAddress: CleanEnergyAssets.address,
      permission: "createProjectEnergyAsset",
      roles: [Roles.Admin],
    },
    {
      contractAddress: CleanEnergyAssets.address,
      permission: "mint",
      roles: [Roles.Admin],
    },
    {
      contractAddress: CleanEnergyAssets.address,
      permission: "burn",
      roles: [Roles.Admin],
    },
    {
      contractAddress: CleanEnergyAssets.address,
      permission: "createGeneralEnergyAsset",
      roles: [Roles.Admin],
    },
  ];

  const rolesPermissions = permissions.filter((permission) =>
    permission.roles.includes(role as Roles)
  );

  console.log({ rolesPermissions });

  for (let index = 0; index < rolesPermissions.length; index++) {
    const permission = rolesPermissions[index];

    logger.log(`${index} of ${rolesPermissions.length} permissions set`);
    const opKey = `${address}-${permission.contractAddress}-${permission.permission}`;

    if (operationCache.has(opKey)) continue;

    try {
      await (
        await permissionGranter
          .connect(admin)
          .setPermission(
            address as string,
            permission.contractAddress,
            permission.permission,
            0,
            0,
            {
              maxFeePerGas: ethers.utils.parseUnits("200", "gwei"),
              maxPriorityFeePerGas: ethers.utils.parseUnits("150", "gwei"),
            }
          )
      ).wait();

      const hasPermission = await permissionGranter.getPermission(
        address,
        permission.contractAddress,
        permission.permission
      );

      if (hasPermission) {
        logger.log(`${permission.permission} permission granted succesfully`);
        operationCache.set(opKey, true);
      } else {
        logger.error(`${permission.permission} permission cannot be granted`);
      }
    } catch (error) {
      failsCount++;
      console.log(error);

      if (failsCount > 3) {
        throw new Error("Too many fails, aborting");
      } else {
        logger.error(`Something fails, retrying...`);
        main();
      }
    }

    console.clear();
  }
}

main()
  .then(() => {
    logger.log("Permissions set successfully");
    killPortProcess(TRPC_SERVER_PORT);
    process.exitCode = 0;
  })
  .catch(() => {
    logger.error("Something fails");
    killPortProcess(TRPC_SERVER_PORT);
    process.exitCode = 1;
  });
