import { logger } from "../utils/logger";
import { client } from "../utils/ozDefender";

async function main() {
  // @ts-ignore
  const ids = (await client.listContracts()).map((c) => c.contractId);
  ids.forEach(async (contractId) => {
    console.log(`Deleting contract ${contractId}`);
    await client.deleteContract(contractId);
  });

  logger.log("Contracts deleted from defender ✅");
}

main();
