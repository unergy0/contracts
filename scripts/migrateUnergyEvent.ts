import { network } from "hardhat";

import contracts from "../full-deployment-report.json";
import { PermissionObject } from "../utils/reporter";
import { factories } from "../utils/constants";
import { logger } from "../utils/logger";

if (network.name !== "polygon") throw new Error("Wrong network. Polygon only");

(async () => {
  // * Start initial config ----------------------------------------------
  const { UnergyEvent, PermissionGranter: PG } = contracts;
  const { PermissionGranterFactory } = await factories;

  const permissionGranter = PermissionGranterFactory.attach(PG.address);

  // * End initial config ------------------------------------------------

  // * Set permissions ---------------------------------------------------

  const requiredPermission = [
    {
      contract: "ProjectsManager",
      contractAddress: "0x759676E6F357e4dAE6ee80389DF529b9bf5f74C5",
      authorizedAddress: "0x2E3EE95b3B81712ADD85586EA4F050B0D519Fbe2",
      permission: {
        role: "updateProjectRelatedProperties",
        type: 1,
        value: "0",
      },
      addressType: "Contract",
    },
    {
      contract: "pWattSomer",
      contractAddress: "0xf2F55a9a9209178521efe964ab82E46896f64f27",
      authorizedAddress: "0x2E3EE95b3B81712ADD85586EA4F050B0D519Fbe2",
      permission: { role: "approveSwap", type: 1, value: "0" },
      addressType: "Contract",
    },
    {
      contract: "pWattUruaco",
      contractAddress: "0x38a2EC3D7891aB55Ee925dd800FdA3d95C202C8E",
      authorizedAddress: "0x2E3EE95b3B81712ADD85586EA4F050B0D519Fbe2",
      permission: { role: "approveSwap", type: 1, value: "0" },
      addressType: "Contract",
    },
    {
      contract: "pWattAcanto",
      contractAddress: "0x2Fb657a5994BC00dDb957707eC14b8715aF98783",
      authorizedAddress: "0x2E3EE95b3B81712ADD85586EA4F050B0D519Fbe2",
      permission: { role: "approveSwap", type: 1, value: "0" },
      addressType: "Contract",
    },
    {
      contract: "pWattPola",
      contractAddress: "0xfA640c16aCA45038b8b7Ddd458C9E6cFCf69D40f",
      authorizedAddress: "0x2E3EE95b3B81712ADD85586EA4F050B0D519Fbe2",
      permission: { role: "approveSwap", type: 1, value: "0" },
      addressType: "Contract",
    },
    {
      contract: "pWattBaraya",
      contractAddress: "0x1E4A3F6e4071E5AC72e6A61766D9d11cC36cFEbc",
      authorizedAddress: "0x2E3EE95b3B81712ADD85586EA4F050B0D519Fbe2",
      permission: { role: "approveSwap", type: 1, value: "0" },
      addressType: "Contract",
    },
    {
      contract: "pWattIBES",
      contractAddress: "0x84d370fBa06640c4786fa48a0b92F4003f83a215",
      authorizedAddress: "0x2E3EE95b3B81712ADD85586EA4F050B0D519Fbe2",
      permission: { role: "approveSwap", type: 1, value: "0" },
      addressType: "Contract",
    },
  ];

  for (let index = 0; index < requiredPermission.length; index++) {
    const permission = requiredPermission[index] as unknown as PermissionObject;

    const tx = await permissionGranter.setPermission(
      UnergyEvent.address,
      permission.contractAddress,
      permission.permission.role,
      permission.permission.type,
      0
    );
    await tx.wait();

    console.log(
      `Permission [ ${permission.permission.role} ] set for [ ${permission.authorizedAddress} ] on [ ${permission.contractAddress} ]`
    );
  }
  // * End set permissions ------------------------------------------------

  logger.log("Done ✅");

  process.exit(0);
})();
