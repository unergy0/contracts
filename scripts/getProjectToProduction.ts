import { ethers } from "hardhat";
import { ProjectHelper } from "../utils";

import { userInput } from "../utils/IO/input";
import {
  ERC20StableCoin,
  ERC20UWatt,
  ProjectsManager,
  UnergyBuyer,
  UnergyData,
  UnergyLogicReserve,
} from "../typechain-types";
import { wallet } from "../utils/wallet";

const addresses: {
  [key: string]: string | undefined;
} = {};

async function main() {
  const { data, projectsManager, logic, buyer, stable, uWatt, projectAddress } =
    await getContracts();

  const projectHelper = new ProjectHelper(
    data,
    projectsManager,
    logic,
    buyer,
    stable,
    uWatt
  );

  const project = await ethers.getContractAt("ERC20Project", projectAddress);
  const meter = await getMeter();

  const user1 = await wallet.getSignerWithBalance();
  const user2 = await wallet.getSignerWithBalance();

  console.log({
    user1: user1.address,
    user2: user2.address,
  });

  projectHelper.bringToProduction(
    project,
    {
      users: [user1.address, user2.address],
      amounts: [],
    },
    meter
  );
}
main();

async function getContracts() {
  const projectAddress = await userInput<string>(
    "Enter the address of the project you want to put into production",
    "address"
  );

  const data = await getContract<UnergyData>("data", "UnergyData");
  const projectsManager = await getContract<ProjectsManager>(
    "projectsManager",
    "ProjectsManager"
  );
  const logic = await getContract<UnergyLogicReserve>(
    "logic",
    "UnergyLogicReserve"
  );
  const buyer = await getContract<UnergyBuyer>("buyer", "UnergyBuyer");
  const stable = await getContract<ERC20StableCoin>(
    "stable",
    "ERC20StableCoin"
  );
  const uWatt = await getContract<ERC20UWatt>("uWatt", "ERC20UWatt");

  return {
    data,
    projectsManager,
    logic,
    buyer,
    stable,
    uWatt,
    projectAddress,
  };
}

async function getContract<T>(
  contractName: string,
  contractType: string
): Promise<T> {
  let contract;
  if (addresses && addresses[contractName]) {
    contract = await ethers.getContractAt(
      contractType,
      addresses[contractName] ?? ""
    );
  } else {
    const contractAddress = await userInput<string>(
      `Enter the address of the ${contractName} contract`,
      "address"
    );
    contract = await ethers.getContractAt(contractType, contractAddress);
  }
  return contract as T;
}

async function getMeter() {
  const meterPk = await userInput<string>(
    "Enter the private key of the meter",
    "privateKey"
  );

  return new ethers.Wallet(meterPk, ethers.provider);
}
