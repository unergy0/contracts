import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";
import { ethers, network } from "hardhat";
import dotenv from "dotenv";

import { provider } from "../utils/provider";
import { deployer } from "../utils";
import { factories } from "../utils/constants";
import { Wallet } from "ethers";

dotenv.config();

const w = ethers.Wallet;

const {
  ADMIN_PRIVATE_KEY,
  MAINTAINER_PRIVATE_KEY,
} = process.env;

let admin: SignerWithAddress | Wallet;
let maintainer: SignerWithAddress | Wallet;

async function main() {
  const {
    UnergyDataFactory,
    UnergyBuyerFactory,
    UnergyLogicReserveFactory,
    ERC20UWattFactory,
    StableCoinFactory,
    PermissionGranterFactory,
    UnergyEventFactory,
    CleanEnergyAssetFactory,
    ProjectsManagerFactory,
  } = await factories;

  if (network.name === "localhost") {
    const signers = await ethers.getSigners();
    admin = signers[0];
    maintainer = signers[1];
  } else {
    admin = new w(ADMIN_PRIVATE_KEY ?? "", provider(network.name));
    maintainer = new w(MAINTAINER_PRIVATE_KEY ?? "", provider(network.name));
  }

  //************/ DEFENDER /************/
  deployer.start({
    ozDefender: {
      network: "matic",
      enable: false,
    },
  });
  //************************************/

  // * PermissionGranter deployment
  const permissionGranter = await deployer.deployProxy(
    "PermissionGranter",
    PermissionGranterFactory,
    [admin.address],
    admin
  );

  // * UnergyData deployment
  const unergyData = await deployer.deployProxy(
    "UnergyData",
    UnergyDataFactory,
    [maintainer.address, permissionGranter?.address],
    admin
  );

  // * UnergyBuyer deployment and linking
  await deployer.deployProxy(
    "UnergyBuyer",
    UnergyBuyerFactory,
    [unergyData?.address, permissionGranter?.address],
    admin
  );

  // * UnergyLogicReserve deployment and linking
  await deployer.deployProxy(
    "UnergyLogicReserve",
    UnergyLogicReserveFactory,
    [unergyData?.address, permissionGranter?.address],
    admin
  );

  // * ProjectsManager deployment
  await deployer.deployProxy(
    "ProjectsManager",
    ProjectsManagerFactory,
    [unergyData!.address, permissionGranter?.address],
    admin
  );

  // * ERC20UWatt deployment
  await deployer.deployContract(
    "ERC20UWatt",
    ERC20UWattFactory,
    [unergyData!.address, permissionGranter?.address],
    admin
  );

  // * StableCoin deployment
  await deployer.deployContract(
    "ERC20StableCoin",
    StableCoinFactory,
    ["USD", "USD", admin.address ?? ""],
    admin
  );

  // * UnergyEvents deployment
  await deployer.deployContract(
    "UnergyEvent",
    UnergyEventFactory,
    [unergyData?.address, permissionGranter?.address],
    admin
  );

  // * CleanEnergyAssets deployment
  await deployer.deployContract(
    "CleanEnergyAssets",
    CleanEnergyAssetFactory,
    [permissionGranter?.address],
    admin
  );

  deployer.saveReport();
}

main()
  .then(() => {
    deployer.finish();
    console.log("Script 'deploy' executed successfully");
    process.exitCode = 0;
  })
  .catch((error) => {
    console.error(error);
    process.exitCode = 1;
  });
