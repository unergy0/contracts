import dotenv from "dotenv";
import { HardhatUserConfig, task } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "@openzeppelin/hardhat-upgrades";
import "@nomiclabs/hardhat-solhint";
import "solidity-coverage";
import "hardhat-gas-reporter";
import "hardhat-abi-exporter";
import "solidity-docgen";
import "hardhat-contract-sizer";
import "hardhat-tracer";
import { decodeError } from "./utils/decodeErrors";
import { decodeMetadataTask } from "./utils/contracts/Position/tasks";

dotenv.config();

declare global {
  interface String {
    to0xString(): string;
  }
}

String.prototype.to0xString = function () {
  return "0x" + this;
};

task("set-balance", "Set balance of an account")
  .addParam("account", "The account to set balance to")
  .addParam(
    "amount",
    "The amount to be set as balance",
    "0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
  )
  .setAction(async (taskArgs, hre) => {
    const { account, amount } = taskArgs;

    await hre.network.provider.request({
      method: "hardhat_setBalance",
      params: [account, amount],
    });

    console.log(`Balance of ${account} set to ${amount}`);
  });

task("decode-error", "Decode revert error bytes string")
  .addParam("bytes", "The bytes string to decode")
  .setAction(async (taskArgs) => {
    const decodedError = decodeError(taskArgs.bytes);

    if (decodedError) {
      console.log(`Decoded error: ${decodedError}`);
    } else {
      console.log(`Could not decode error: ${taskArgs.bytes}`);
    }
  });

task("decode-position-metadata", "Decode position metadata bytes string")
  .addParam("encodeString", "The bytes string to decode")
  .setAction(async (taskArgs, hre) => decodeMetadataTask(taskArgs, hre));

const {
  ADMIN_PRIVATE_KEY,
  MUMBAI_RPC_URL,
  ETHERSCAN_API_KEY,
  POLYGON_RPC_URL,
  DEVNODE_ADMIN_PRIVATE_KEY,
  DEVNODE_RPC_URL,
  ADMIN_PROD_PRIVATE_KEY,
  DEVNET_RPC_URL,
  POLYGON_FORK_RPC_URL,
} = process.env;

const config: HardhatUserConfig = {
  solidity: {
    version: "0.8.18",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
  mocha: {
    timeout: 10_000_000,
  },
  networks: {
    hardhat: {
      chainId: 1337,
      // forking: {
      //   url: POLYGON_RPC_URL ?? "",
      //   blockNumber: 45747311, // latest block at 07/31/2023 11:56:00 AM GMT -5
      // },
      accounts: {
        count: 50,
        accountsBalance: "1000000000000000000000000000",
      },
    },
    polygon_mumbai: {
      url: MUMBAI_RPC_URL,
      accounts: [ADMIN_PRIVATE_KEY ?? ""],
    },
    polygon: {
      url: POLYGON_RPC_URL,
      accounts: [ADMIN_PROD_PRIVATE_KEY ?? ""],
    },
    polygon_fork: {
      url: POLYGON_FORK_RPC_URL,
      accounts: [ADMIN_PROD_PRIVATE_KEY ?? ""],
    },
    dev_node: {
      url: DEVNODE_RPC_URL,
      accounts: [DEVNODE_ADMIN_PRIVATE_KEY ?? ""],
    },
    devnet: {
      url: DEVNET_RPC_URL,
      accounts: [ADMIN_PRIVATE_KEY ?? ""],
    },
  },
  etherscan: {
    apiKey: ETHERSCAN_API_KEY,
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS ? true : false,
  },
  abiExporter: {
    path: "./data/abi",
    runOnCompile: false,
    clear: true,
    flat: true,
    spacing: 2,
    only: [],
    format: "minimal",
  },
  docgen: {
    exclude: ["../node_modules"],
  },
  contractSizer: {
    runOnCompile: false,
    alphaSort: true,
  },
};

export default config;
