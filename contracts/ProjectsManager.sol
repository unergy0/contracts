// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";

import {Project, ProjectInput, ProjectState, Milestone, Signatures, Signature} from "./Types.sol";

import "./CommonUpgradeable.sol";
import "./UnergyData.sol";
import "./ERC1155CleanEnergyAssets.sol";
import "./ERC20Project.sol";
import "./UnergyEvent.sol";

/**
 * @title ProjectsManager (v2) - Last upgrade at 2023-09-24
 * @notice This contract manages the project-related operations and stores the data related to it.
 */

contract ProjectsManager is
    Initializable,
    PausableUpgradeable,
    CommonUpgradeable,
    UUPSUpgradeable
{
    using SafeERC20Upgradeable for IERC20Upgradeable;
    using CountersUpgradeable for CountersUpgradeable.Counter;

    UnergyData private unergyData;
    address public potentialOwner;

    address[] private projectsAddresses;

    CountersUpgradeable.Counter private _projectIdsCounter;

    mapping(address => Project) private projects;
    mapping(address => bool) private projectExists;
    mapping(address => address[]) private projectHolders;
    mapping(address => Milestone[]) private milestones;
    mapping(address => bool[2]) private signatures; // This mapping is not used but is requried to keep the storage layout
    mapping(address => bool) public projectConfigured;
    mapping(address => uint256) public initialTotalSupply;

    event SignatureSet(address indexed projectAddress, Signature signature);
    /**
     * @custom:upgrade-note This function was added in the upgrade v2 at 2023-09-24
     */
    event MilestoneUpdated(address indexed projectAddress, string name);
    event MilestonesUpdated(address indexed projectAddress);
    event MilestoneAdded(address indexed projectAddress, string name);
    event ProjectCreated(address indexed projectAddress);
    event ProjectConfigured(address indexed projectAddress);
    event ProjectUpdated(address indexed projectAddress);
    event ProjectHolderAdded(
        address indexed projectAddress,
        address indexed holder
    );
    event OwnerNominated(address pendingOwner);

    error ProjectDoesNotExist(address projectAddress);
    error MilestoneAlreadyReached(string name);
    error MaintenancePercentageExceeded(uint256 givenMaintenancePercentage);
    error OriginatorFeeExceeded(uint256 givenOriginatorFee);
    error ApproveSwapFailed();
    error ProjectAlreadyConfigured(address projectAddress);
    error MilestoneWeightsExceeded(uint256 milestoneWeights);
    error NotPotentialOwner(address potentialOwner);
    /**
     * @custom:upgrade-note This function was added in the upgrade v2 at 2023-09-24
     */
    error InvalidInputLength(uint256 expectedLength, uint256 givenLength);

    modifier ifProjectExists(address _projectAddress) {
        if (!projectExists[_projectAddress])
            revert ProjectDoesNotExist(_projectAddress);
        _;
    }

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }

    function initialize(
        address _unergyDataAddr,
        address _permissionGranterAddr
    )
        external
        notZeroAddress(_unergyDataAddr)
        notZeroAddress(_permissionGranterAddr)
        initializer
    {
        __Pausable_init();
        __Common_init(_permissionGranterAddr);
        __UUPSUpgradeable_init();

        unergyData = UnergyData(_unergyDataAddr);
    }

    function _authorizeUpgrade(
        address newImplementation
    ) internal override onlyOwner {}

    /**
     * @notice Allows to admin user  to create a  new project
     * @dev This function deploys an ERC20 Project and configures all necessary permissions.
     * @param _projectName the ERC20 name
     * @param _projectSymbol the ERC20 symbol
     */
    function createProject(
        ProjectInput memory _projectInput,
        string memory _projectName,
        string memory _projectSymbol
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "createProject")
    {
        uint256 projectId = _projectIdsCounter.current();
        _projectIdsCounter.increment();

        ERC20Project erc20Project = new ERC20Project(
            _projectName,
            _projectSymbol,
            address(unergyData),
            address(permissionGranter)
        );

        address erc20ProjectAddress = address(erc20Project);

        unergyData.setAssetManagerAddress(
            erc20ProjectAddress,
            _projectInput.assetManagerAddr
        );

        unergyData.setAssetManagerFeePercentage(
            erc20ProjectAddress,
            _projectInput.assetManagerFeePercentage
        );

        Project memory project = _createProject(
            _projectInput,
            erc20ProjectAddress,
            projectId
        );
        projects[project.addr] = project;
        projectsAddresses.push(project.addr);
        projectExists[project.addr] = true;

        address cleanEnergyAssetsAddr = unergyData.cleanEnergyAssetsAddress();

        CleanEnergyAssets(cleanEnergyAssetsAddr).createProjectEnergyAsset(
            _projectName,
            erc20ProjectAddress
        );

        emit ProjectCreated(project.addr);
    }

    function _createProject(
        ProjectInput memory _projectInput,
        address _erc20ProjectAddress,
        uint256 _projectId
    ) private view whenNotPaused returns (Project memory) {
        _checkMaintenancePercentage(_projectInput.maintenancePercentage);
        _checkOriginatorFee(
            _projectInput.originatorFee,
            _projectInput.totalPWatts
        );

        Project memory project = Project({
            id: _projectId,
            maintenancePercentage: _projectInput.maintenancePercentage,
            initialProjectValue: _projectInput.initialProjectValue,
            currentProjectValue: _projectInput.currentProjectValue,
            swapFactor: _projectInput.swapFactor,
            state: ProjectState.FUNDING,
            addr: _erc20ProjectAddress,
            adminAddr: _projectInput.adminAddr,
            installerAddr: _projectInput.installerAddr,
            usdDepreciated: 0,
            pWattsSupply: _projectInput.totalPWatts,
            signatures: Signatures({
                isSignedByInstaller: false,
                isSignedByOriginator: false
            }),
            originator: _projectInput.originator,
            originatorFee: _projectInput.originatorFee,
            stableAddr: _projectInput.stableAddr
        });

        return project;
    }

    function configureProject(
        address erc20ProjectAddress
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "createProject")
    {
        if (projectConfigured[erc20ProjectAddress] == true) {
            revert ProjectAlreadyConfigured(erc20ProjectAddress);
        } else {
            Project memory project = getProject(erc20ProjectAddress);
            ERC20Project erc20Project = ERC20Project(erc20ProjectAddress);

            projectConfigured[erc20ProjectAddress] = true;

            uint256 adminPWatts = project.pWattsSupply - project.originatorFee;

            erc20Project.mint(project.adminAddr, adminPWatts);
            erc20Project.mint(project.originator, project.originatorFee);

            erc20Project.transferOwnership(msg.sender);

            emit ProjectConfigured(erc20ProjectAddress);
        }
    }

    function updateProject(
        address _projectAddress,
        Project memory _project
    )
        external
        whenNotPaused
        ifProjectExists(_projectAddress)
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "updateProjectRelatedProperties"
        )
    {
        uint256 pWattSupply = ERC20Project(_project.addr).totalSupply();

        _checkOriginatorFee(_project.originatorFee, pWattSupply);
        _checkMaintenancePercentage(_project.maintenancePercentage);

        Project storage project = projects[_projectAddress];

        project.maintenancePercentage = _project.maintenancePercentage;
        project.initialProjectValue = _project.initialProjectValue;
        project.currentProjectValue = _project.currentProjectValue;
        project.swapFactor = _project.swapFactor;
        project.pWattsSupply = _project.pWattsSupply;
        project.usdDepreciated = _project.usdDepreciated;
        project.originator = _project.originator;
        project.originatorFee = _project.originatorFee;
        project.stableAddr = _project.stableAddr;
        project.installerAddr = _project.installerAddr;
        project.adminAddr = _project.adminAddr;
        project.state = _project.state;
        project.signatures = _project.signatures;

        emit ProjectUpdated(project.addr);
    }

    function setProjectState(
        address _projectAddress,
        ProjectState _projectState
    )
        external
        whenNotPaused
        ifProjectExists(_projectAddress)
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "updateProjectRelatedProperties"
        )
    {
        Project storage project = projects[_projectAddress];
        project.state = _projectState;

        if (_projectState == ProjectState.INREFUND) {
            ERC20Project erc20Project = ERC20Project(_projectAddress);
            initialTotalSupply[_projectAddress] = erc20Project.totalSupply();
        }

        emit ProjectUpdated(project.addr);
    }

    function addProjectHolder(
        address _projectAddress,
        address _holderAddress
    )
        external
        whenNotPaused
        ifProjectExists(_projectAddress)
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "updateProjectRelatedProperties"
        )
    {
        if (_holderExists(_projectAddress, _holderAddress)) return;

        projectHolders[_projectAddress].push(_holderAddress);

        emit ProjectHolderAdded(_projectAddress, _holderAddress);
    }

    function _holderExists(
        address _projectAddress,
        address _holderAddress
    ) private view returns (bool) {
        address[] memory holders = projectHolders[_projectAddress];

        for (uint256 i = 0; i < holders.length; i++) {
            if (holders[i] == _holderAddress) {
                return true;
            }
        }

        return false;
    }

    function addProjectMilestone(
        address _projectAddress,
        string memory _name,
        uint256 weight
    )
        external
        whenNotPaused
        ifProjectExists(_projectAddress)
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "updateProjectRelatedProperties"
        )
    {
        Project memory project = getProject(_projectAddress);
        Milestone[] storage projectMilestones = milestones[project.addr];

        projectMilestones.push(
            Milestone({
                name: _name,
                weight: weight,
                isReached: false,
                wasSignedByInstaller: false,
                wasSignedByOriginator: false
            })
        );

        _checkMilestoneWeights(projectMilestones);

        emit MilestoneAdded(project.addr, _name);
    }

    /**
     * @custom:upgrade-note This function was added in the upgrade v2 at 2023-09-24
     */
    function updateProjectMilestones(
        address _projectAddress,
        Milestone[] memory _milestones
    )
        external
        whenNotPaused
        ifProjectExists(_projectAddress)
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "updateProjectRelatedProperties"
        )
    {
        _checkMilestoneWeights(_milestones);
        Milestone[] storage currentMilestones = milestones[_projectAddress];

        if (currentMilestones.length != _milestones.length) {
            revert InvalidInputLength(
                currentMilestones.length,
                _milestones.length
            );
        }

        for (uint256 i; i < currentMilestones.length; i++) {
            currentMilestones[i] = _milestones[i];
        }

        emit MilestonesUpdated(_projectAddress);
    }

    function updateMilestoneAtIndex(
        address _projectAddress,
        uint256 _index,
        Milestone memory _milestone
    )
        external
        whenNotPaused
        ifProjectExists(_projectAddress)
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "updateProjectRelatedProperties"
        )
    {
        Project memory project = getProject(_projectAddress);
        Milestone[] storage projectMilestones = milestones[project.addr];

        projectMilestones[_index] = _milestone;

        _checkMilestoneWeights(projectMilestones);

        emit MilestoneUpdated(project.addr, _milestone.name);
    }

    function _checkMilestoneWeights(
        Milestone[] memory _milestones
    ) internal pure {
        uint256 acummulatedWeight;
        for (uint256 i; i < _milestones.length; i++) {
            acummulatedWeight += _milestones[i].weight;
        }
        if (acummulatedWeight > 100) {
            revert MilestoneWeightsExceeded(acummulatedWeight);
        }
    }

    function _checkMaintenancePercentage(
        uint256 _maintenancePercentage
    ) private pure {
        if (_maintenancePercentage > 50e18) {
            revert MaintenancePercentageExceeded(_maintenancePercentage);
        }
    }

    function _checkOriginatorFee(
        uint256 _originatorFee,
        uint256 _totalPWatts
    ) private pure {
        if (_originatorFee > _totalPWatts) {
            revert OriginatorFeeExceeded(_originatorFee);
        }
    }

    /**
     * @notice Sets the project signature to the given value
     * @param _projectAddress the address of the project whose signature will be modified
     * @param _signature installer or originator signature {see: Signature on Types.sol}
     */
    function setSignature(
        address _projectAddress,
        Signature _signature
    )
        external
        whenNotPaused
        ifProjectExists(_projectAddress)
        hasRoleInPermissionGranter(msg.sender, address(this), "setSignature")
    {
        Project storage project = projects[_projectAddress];

        if (_signature == Signature.INSTALLER) {
            project.signatures.isSignedByInstaller = true;
        } else {
            project.signatures.isSignedByOriginator = true;
        }

        emit SignatureSet(_projectAddress, _signature);
    }

    /**
     * @notice This function finds a project and revert if doesn't exists
     * @param _projectAddress The address of the project to look for
     * @return Project
     */
    function getProject(
        address _projectAddress
    ) public view returns (Project memory) {
        if (!projectExists[_projectAddress])
            revert ProjectDoesNotExist(_projectAddress);

        return projects[_projectAddress];
    }

    function getProjectHolders(
        address _projectAddress
    ) external view returns (address[] memory) {
        getProject(_projectAddress);

        return projectHolders[_projectAddress];
    }

    function getTotalPWatts(
        address _projectAddress
    ) external view ifProjectExists(_projectAddress) returns (uint256) {
        Project memory project = getProject(_projectAddress);

        return project.pWattsSupply;
    }

    function getProjectState(
        address _projectAddress
    ) external view ifProjectExists(_projectAddress) returns (ProjectState) {
        Project memory project = getProject(_projectAddress);

        return project.state;
    }

    function getProjectMilestones(
        address _projectAddress
    )
        external
        view
        ifProjectExists(_projectAddress)
        returns (Milestone[] memory)
    {
        Project memory project = getProject(_projectAddress);

        return milestones[project.addr];
    }

    function getSignatures(
        address _projectAddress
    ) external view ifProjectExists(_projectAddress) returns (bool[2] memory) {
        Project memory project = getProject(_projectAddress);

        return [
            project.signatures.isSignedByInstaller,
            project.signatures.isSignedByOriginator
        ];
    }

    function transferOwnership(
        address _pendingOwner
    ) public override onlyOwner whenNotPaused notZeroAddress(_pendingOwner) {
        potentialOwner = _pendingOwner;
        emit OwnerNominated(_pendingOwner);
    }

    function acceptOwnership() external {
        if (msg.sender == potentialOwner) {
            _transferOwnership(msg.sender);
            potentialOwner = address(0);
        } else {
            revert NotPotentialOwner(msg.sender);
        }
    }

    function pause() external onlyOwner whenNotPaused {
        _pause();
    }

    function unpause() external onlyOwner whenPaused {
        _unpause();
    }
}
