// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import {ProjectState} from "./Types.sol";

abstract contract UnergyEventAbs {
    function beforeTransferReceipt(
        address origin,
        address from,
        address to,
        uint256 amount
    ) public virtual;

    function afterTransferReceipt(
        address origin,
        address from,
        address to,
        uint256 amount
    ) public virtual;
}

abstract contract ERC20Abs {
    function burn(address account, uint256 amount) public virtual;

    function burn(uint256 amount) public virtual;

    function burnFrom(address owner, uint256 amount) public virtual;

    function approveSwap(
        address owner,
        address spender,
        uint256 amount
    ) public virtual;

    function allowance(
        address owner,
        address spender
    ) public virtual returns (uint256);

    function decimals() public view virtual returns (uint8);

    function mint(address account, uint256 amount) public virtual;

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) public virtual returns (bool);

    function transfer(address to, uint256 amount) public virtual returns (bool);

    function setState(ProjectState state) public virtual;

    function name() public virtual returns (string memory);

    function approve(address spender, uint256 amount) public virtual;

    function balanceOf(address account) public virtual returns (uint256);
}

abstract contract ERC1155Abs {
    function createProjectEnergyAsset(
        string memory tokenName,
        address projectAddr
    ) public virtual;

    function mint(address projectAddr, uint256 amount) public virtual;

    function burn(address projectAddr, uint256 amount) public virtual;
}
