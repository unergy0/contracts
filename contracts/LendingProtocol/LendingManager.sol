// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {AccessControl} from "@openzeppelin/contracts/access/AccessControl.sol";
import {FixedPointMathLib} from "solmate/src/utils/FixedPointMathLib.sol";
import {SafeERC20, IERC20} from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

import {IPosition} from "./IPosition.sol";
import {Position} from "./Position.sol";
import {IProjectsManager, Project, ProjectState} from "../interfaces/IProjectsManager.sol";
import {UnergyData} from "../UnergyData.sol";

/**
 * @author https://unergy.io
 *
 * @custom:definitions
 *
 * - Lender: The one who will lend the money, i.e. our retail user.
 *           It is identified because it has a NFT that identifies it as such.
 *
 * - Borrower: It could be us (Unergy) or even in the future it could be another
 *             borrower who wants to take on the origination risk instead of the user.
 *             It is identified because it holds a Borrower's NFT that associates it
 *             with the lending position.
 */

contract LendingManager is AccessControl {
    /*****************************************
     *           Type Definitions            *
     ****************************************/

    using SafeERC20 for IERC20;
    using FixedPointMathLib for uint256;

    /*****************************************
     *               Storage                 *
     ****************************************/

    bytes32 public constant ADMIN_ROLE = keccak256("ADMIN_ROLE");
    uint256 private constant WAD = 1e18;

    IERC20 public stableCoin;
    UnergyData public unergyData;
    IProjectsManager public unergyPM;
    address public uniswapV3Pool;

    address[] private positions;
    mapping(address => bool) public isPosition;

    mapping(address => uint256[2]) private _profitRate;
    mapping(address => uint256[2]) private _compensationRate;
    mapping(address => uint256[2]) private _periodRate;
    mapping(address => uint256[2]) private _additionalPeriodRate;

    /*****************************************
     *                Events                 *
     ****************************************/

    event PositionCreated(address indexed positionAddress);
    event PositionClosed(
        address indexed positionAddress,
        address indexed borrower
    );

    /*****************************************
     *                Errors                 *
     ****************************************/

    error LendingManager_InvalidValue();
    error LendingManager_InvalidLockedPeriod(
        uint256 minLockedPeriod,
        uint256 desiredLockedPeriod
    );
    error LendingManager_InvalidRate();
    error LendingManager_InsuffitientPWatts();
    error LendingManager_InvalidProjectState();
    error LendingManager_PositionNotAvailable();
    error LendingManager_PositionDoesNotExists();
    error LendingManager_PositionNotInDefaultWaitingPeriod();
    error LendingManager_CompensationMustBeGraterThanRegularProfit();

    /*****************************************
     *            Constructor 👷             *
     ****************************************/

    constructor(
        address _initialOwner,
        address _stableCoin,
        address _unergyData,
        address _uniswapV3Pool
    ) {
        _grantRole(DEFAULT_ADMIN_ROLE, _initialOwner);

        uniswapV3Pool = _uniswapV3Pool;
        stableCoin = IERC20(_stableCoin);
        unergyData = UnergyData(_unergyData);
        unergyPM = IProjectsManager(unergyData.projectsManagerAddress());
    }

    /*****************************************
     *                Public                 *
     ****************************************/

    /**
     * @dev This method is used to get the profit rate for a specific project.
     * @notice This rate is managed by the Unergy team and can be changed at any time.
     * @param _projectAddress The address of the project the rate are related to.
     */
    function getProfitRate(
        address _projectAddress
    ) public view returns (uint256[2] memory) {
        return _profitRate[_projectAddress];
    }

    /**
     * @dev This method is used to get the compensation rate for a specific project.
     * @notice This rate is managed by the Unergy team and can be changed at any time.
     * @param _projectAddress The address of the project the rate are related to.
     */
    function getCompensationRate(
        address _projectAddress
    ) public view returns (uint256[2] memory) {
        return _compensationRate[_projectAddress];
    }

    /**
     * @dev This method is used to get the period rate for a specific project.
     * @notice This rate is managed by the Unergy team and can be changed at any time.
     * @param _projectAddress The address of the project the rate are related to.
     */
    function getLoanTermsLimits(
        address _projectAddress
    ) public view returns (uint256[2] memory) {
        return _periodRate[_projectAddress];
    }

    /**
     * @dev This method is used to get the default waiting period for a specific project.
     * @notice This rate is managed by the Unergy team and can be changed at any time.
     * @param _projectAddress The address of the project the rate are related to.
     */
    function getAdditionalLoanTermLimits(
        address _projectAddress
    ) public view returns (uint256[2] memory) {
        return _additionalPeriodRate[_projectAddress];
    }

    /**
     * @dev This method is used to get the default waiting period for a specific project.
     * @notice This rate is managed by the Unergy team and can be changed at any time.
     * @param _projectAddress The address of the project the rate are related to.
     */
    function getDefaultLoadTermLimits(
        address _projectAddress
    ) public view returns (uint256[2] memory) {
        return _additionalPeriodRate[_projectAddress];
    }

    function getDailyProfit(
        uint256 _lockedAmount,
        uint256 _annualPercentage
    ) public pure returns (uint256) {
        uint256 annualProfit = _getAnualProfit(
            _lockedAmount,
            _annualPercentage
        );
        uint256 dailyProfit = annualProfit.mulDivDown(1e6, 365e6);

        return dailyProfit;
    }

    /*****************************************
     *               External                *
     ****************************************/

    /**
     * @notice Use this method to open a position as `Lender`.
     * @param _projectAddress The address of the project that will receive the funds.
     * @param _anualRevenuePercentage The annual profit rate that the project will pay to the Lender.
     * @param _anualCompensationPercentage The annual profit rate that the project will pay to the Lender if the project is in default.
     * @param _lockedPeriod The period of time that the Lender will lock the funds (in days).
     * @param _additionalPeriod The maximum time the lender will wait to receive compensation (in days).
     * @param _lockedAmount The amount of stable coins that the Lender will provide.
     **/
    function createPosition(
        address _projectAddress,
        uint256 _anualRevenuePercentage,
        uint256 _anualCompensationPercentage,
        uint256 _lockedPeriod,
        uint256 _additionalPeriod,
        uint256 _lockedAmount
    ) external returns (address positionAddress) {
        _checkProjectState(_projectAddress);
        _assertGt(_anualCompensationPercentage, _anualRevenuePercentage);
        // @dev mantain these limits can reduce the risk of someone get scammed.
        // @todo enable this validations
        // _checkCompensationRate(_anualCompensationPercentage, _projectAddress);
        // _checkProfitRate(_anualRevenuePercentage, _projectAddress);
        // _checkPeriodRate(_lockedPeriod, _projectAddress);
        // _checkWaitingPeriodRate(_additionalPeriod, _projectAddress);

        (
            IPosition.PositionMetadata memory posMeta,
            address lender
        ) = _buildPositionMetadata(
                _projectAddress,
                _anualRevenuePercentage,
                _anualCompensationPercentage,
                _lockedPeriod,
                _additionalPeriod,
                _lockedAmount
            );

        address uWatt = unergyData.uWattAddress();
        address stable = address(stableCoin);

        address unergyLogic = unergyData.unergyLogicReserveAddress();
        positionAddress = address(
            new Position(
                "Unergy Liquidity Position",
                "ULP",
                uWatt,
                stable,
                unergyLogic,
                uniswapV3Pool,
                posMeta
            )
        );

        stableCoin.safeTransferFrom(lender, positionAddress, _lockedAmount);

        isPosition[positionAddress] = true;
        positions.push(positionAddress);

        emit PositionCreated(positionAddress);
    }

    /**
     * @notice Closing a position means providing collateral for an open position.
     */
    function closePosition(address _positionAddress) external {
        IPosition.PositionMetadata memory posMeta = _getPositionMetadata(
            _positionAddress
        );

        if (posMeta.state != IPosition.PositionState.Pending) {
            revert LendingManager_PositionNotAvailable();
        }

        address borrower = msg.sender;
        address lender = posMeta.lender;

        stableCoin.safeTransferFrom(
            borrower,
            _positionAddress,
            posMeta.collateral
        );

        Position position = Position(_positionAddress);

        position.safeTransferFrom(address(this), lender, position.LENDER_ID());
        position.safeTransferFrom(
            address(this),
            borrower,
            position.BORROWER_ID()
        );

        position.approveStableSpend();
        position.updateState(IPosition.PositionState.Active);
        position.setBorrower(borrower);
        position.setStartDate(block.timestamp);

        emit PositionClosed(_positionAddress, borrower);
    }

    /**
     * @notice Cancel a position means that the Lender will withdraw the funds that he has provided.
     * @dev This method can only be called by the Lender and if the position is still Pending.
     */
    function cancelPosition(address _positionAddress) external {
        IPosition.PositionMetadata memory posMeta = _getPositionMetadata(
            _positionAddress
        );

        if (
            posMeta.state == IPosition.PositionState.Pending &&
            posMeta.lender == msg.sender
        ) {
            Position position = Position(_positionAddress);

            position.updateState(IPosition.PositionState.Canceled);
            position.refund();

            position.burn(position.LENDER_ID());
            position.burn(position.BORROWER_ID());
        } else {
            revert LendingManager_PositionNotAvailable();
        }
    }

    function getPositions() external view returns (address[] memory) {
        return positions;
    }

    function getProjectState(
        address _projectAddress
    ) external view returns (ProjectState) {
        return unergyPM.getProjectState(_projectAddress);
    }

    /*****************************************
     *         External (Protected)          *
     ****************************************/

    /**
     * @notice This limit the profit rate that can be expected by the Lender for a specific project.
     */
    function setProfitRate(
        address _projectAddress,
        uint256[2] calldata _rate
    ) external onlyRole(ADMIN_ROLE) {
        _validRate(_rate);
        _profitRate[_projectAddress] = _rate;
    }

    /**
     * @notice This limit the period that the Lender can lock the funds for a specific project.
     */
    function setLoanTermLimits(
        address _projectAddress,
        uint256[2] calldata _rate
    ) external onlyRole(ADMIN_ROLE) {
        _validRate(_rate);
        _periodRate[_projectAddress] = _rate;
    }

    /**
     * @notice This limit the default waiting period that the Lender can wait to receive compensation for a specific project.
     */
    function setAdditionalLoanTermLimits(
        address _projectAddress,
        uint256[2] calldata _rate
    ) external onlyRole(ADMIN_ROLE) {
        _validRate(_rate);
        _additionalPeriodRate[_projectAddress] = _rate;
    }

    /**
     * @notice This limit the compensation rate that can be expected by the Lender for a specific project.
     */
    function setCompensationRate(
        address _projectAddress,
        uint256[2] calldata _rate
    ) external onlyRole(ADMIN_ROLE) {
        _validRate(_rate);
        _compensationRate[_projectAddress] = _rate;
    }

    function checkDefaultTermLimits(
        address _projectAddress,
        uint256[2] calldata _rate
    ) external onlyRole(ADMIN_ROLE) {
        _validRate(_rate);
        _additionalPeriodRate[_projectAddress] = _rate;
    }

    /*****************************************
     *               Internal                *
     ****************************************/

    function _checkProfitRate(
        uint256 _value,
        address _projectAddress
    ) internal view {
        uint256[2] memory profitRate = _profitRate[_projectAddress];
        _valueInRange(profitRate, _value);
    }

    function _checkCompensationRate(
        uint256 _value,
        address _projectAddress
    ) internal view {
        uint256[2] memory compensationRate = _compensationRate[_projectAddress];
        _valueInRange(compensationRate, _value);
    }

    function _checkPeriodRate(
        uint256 _value,
        address _projectAddress
    ) internal view {
        uint256[2] memory periodRate = _periodRate[_projectAddress];
        _valueInRange(periodRate, _value);
    }

    function _checkWaitingPeriodRate(
        uint256 _value,
        address _projectAddress
    ) internal view {
        uint256[2] memory waitingPeriodRange = _additionalPeriodRate[
            _projectAddress
        ];
        _valueInRange(waitingPeriodRange, _value);
    }

    function _checkProjectState(address _projectAddress) internal view {
        ProjectState state = unergyPM.getProjectState(_projectAddress);

        if (state != ProjectState.FUNDING) {
            revert LendingManager_InvalidProjectState();
        }
    }

    function _valueInRange(
        uint256[2] memory _range,
        uint256 _value
    ) internal pure {
        uint256 bottomRateLimit = _range[0];
        uint256 upperRateLimit = _range[1];

        if (_value < bottomRateLimit || _value > upperRateLimit)
            revert LendingManager_InvalidValue();
    }

    function _validRate(uint256[2] memory _rate) internal pure {
        uint256 bottomRateLimit = _rate[0];
        uint256 upperRateLimit = _rate[1];

        if (bottomRateLimit >= upperRateLimit)
            revert LendingManager_InvalidRate();
    }

    function _getPositionMetadata(
        address _positionAddress
    ) internal view returns (IPosition.PositionMetadata memory) {
        if (!isPosition[_positionAddress]) {
            revert LendingManager_PositionDoesNotExists();
        }
        IPosition.PositionMetadata memory posMeta = Position(_positionAddress)
            .getMetadata();

        return posMeta;
    }

    function _buildPositionMetadata(
        address _projectAddress,
        uint256 _anualRevenuePercentage,
        uint256 _anualCompensationPercentage,
        uint256 _lockedPeriod,
        uint256 _additionalPeriod,
        uint256 _lockedAmount
    )
        internal
        view
        returns (
            IPosition.PositionMetadata memory positionMetadata,
            address lender
        )
    {
        lender = msg.sender;

        uint256 periodProfit = _getPeriodProfit(
            _lockedAmount,
            _anualRevenuePercentage, // R
            _lockedPeriod
        );
        uint256 collateral = _getPeriodProfit(
            _lockedAmount,
            _anualCompensationPercentage, // R*
            _additionalPeriod
        );

        positionMetadata = IPosition.PositionMetadata({
            state: IPosition.PositionState.Pending,
            lender: lender,
            borrower: address(0),
            projectAddress: _projectAddress,
            profitPercentage: _anualRevenuePercentage,
            defaultProfitPercentage: _anualCompensationPercentage,
            periodProfit: periodProfit,
            lockedPeriod: _lockedPeriod,
            additionalPeriod: _additionalPeriod,
            lockedAmount: _lockedAmount,
            collateral: collateral,
            payedCompensation: 0,
            startDate: 0
        });
    }

    function _getPeriodProfit(
        uint256 _lockedAmount,
        uint256 _annualPercentage,
        uint256 _lockedPeriod
    ) internal pure returns (uint256) {
        uint256 dailyProfit = getDailyProfit(_lockedAmount, _annualPercentage);
        uint256 periodProfit = dailyProfit * _lockedPeriod;

        return periodProfit;
    }

    function _getAnualProfit(
        uint256 _lockedAmount,
        uint256 _annualPercentage
    ) internal pure returns (uint256) {
        uint256 annualProfit = _lockedAmount
            .mulDivDown(_annualPercentage, 1e18)
            .mulDivDown(1e6, 100e6);

        return annualProfit;
    }

    function _assertGt(uint256 a, uint256 b) internal pure {
        // @todo check this
        if ((a > b) == false) {
            revert LendingManager_InvalidValue();
        }
    }
}
