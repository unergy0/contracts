// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/**
 * @author https://unergy.io
 *
 * @custom:definitions
 *
 * R: annual profitability (APR) in % to be obtained by the Lender.
 * R*: annual profitability (APR) in % that the Lender will obtain in case of delay of the Project.
 *     To make sense, it must be fulfilled that R* > R, otherwise, it would not be a compensation for
 *     the Lender, who put the money for the development of the project.
 * T: Period of time, in days, that the position will last.
 * T*: Maximum additional period of time that the position could be extended, receiving an indemnification.
 * M: Amount of USDT provided by the Lender.
 * C: Amount of USDT provided by the Borrower as collateral for the loan.
 */

interface IPosition {
    /*****************************************
     *           Type Definitions            *
     ****************************************/

    enum PositionState {
        // 0 - Inexistent: If the position has not been created yet
        Inexistent,
        // 1 - Pending: If a position has been opened by a lender but not yet closed by a borrower
        Pending,
        // 2 - Active: When a borrower provides the collateral and the funds are used to take part in a project
        Active,
        // 3 - Delayed: When the project is not operational by the appropriate date (T)
        Delayed,
        // 4 - Default: If the project is not operational by the maximum date (T + T*)
        Default,
        // 5 - Canceled: If the lender withdraws the funds before the borrower provides the collateral
        Canceled,
        // 6 - Closed: If the project is in production and the pWatts are swapped
        Closed
    }

    struct PositionMetadata {
        PositionState state;
        address lender;
        address borrower;
        address projectAddress;
        uint256 profitPercentage; // R
        uint256 defaultProfitPercentage; // R*
        uint256 lockedAmount; // M
        uint256 periodProfit;
        uint256 lockedPeriod; // T
        uint256 additionalPeriod; // T*
        uint256 collateral; // C
        uint256 payedCompensation;
        uint256 startDate;
    }

    /*****************************************
     *               External                *
     ****************************************/

    function withdrawProfit() external;

    function withdrawCompensation() external;

    function getMetadata() external view returns (PositionMetadata memory);

    function approveStableSpend() external;

    function updateState(PositionState _state) external;

    function setBorrower(address _borrower) external;

    function setStartDate(uint256 _startDate) external;
}
