// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./PermissionGranter.sol";
import {CommonAbstract} from "./interfaces/CommonAbstract.sol";

contract Common is CommonAbstract, Ownable {
    PermissionGranter public permissionGranter;

    constructor(address permissionGranterAddr) {
        if (permissionGranterAddr != address(0)) {
            permissionGranter = PermissionGranter(permissionGranterAddr);
        }
    }

    function setPermissionGranterAddr(
        address permissionGranterAddr
    )
        public
        override
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setPermissionGranterAddr"
        )
        notZeroAddress(permissionGranterAddr)
    {
        permissionGranter = PermissionGranter(permissionGranterAddr);

        emit SetPermissionGranter(permissionGranterAddr);
    }

    function getPermissionGranterAddress()
        public
        view
        override
        returns (address)
    {
        return address(permissionGranter);
    }

    // This pattern is used to avoid a code copy for each function using the modifier
    // by using this pattern we replace de code copy with a function call wich produces a smaller bytecode
    function _hasRoleInPermissionGranter(
        address _caller,
        address _contract,
        string memory _functionName
    ) internal override {
        bool permissionState = permissionGranter.getAndUpdatePermission(
            _caller,
            _contract,
            _functionName
        );

        _checkPermission(permissionState, _functionName, _caller);
    }

    function _hasMeterPermissionOverProjectContract(
        address _caller,
        address _logicContract,
        address _projectContract
    ) internal view override {
        bool hasPermission = permissionGranter.getMeterPermission(
            _caller,
            _logicContract,
            _projectContract
        );

        _checkPermission(hasPermission, "energyReport", _caller);
    }

    modifier notZeroAddress(address _address) {
        _notZeroAddress(_address);
        _;
    }

    function _notZeroAddress(address _address) internal pure override {
        if (_address == address(0)) revert ZeroAddressNotAllowed();
    }

    function _checkPermission(
        bool _hasPermission,
        string memory _functionName,
        address _caller
    ) internal pure override {
        if (!_hasPermission) {
            revert PermissionGranter.NotAuthorizedToCallFunction(
                _functionName,
                _caller
            );
        }
    }
}
