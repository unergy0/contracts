// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";

import {PurchaseTicket, UnergyEventVersion} from "./Types.sol";

import "./CommonUpgradeable.sol";

/**
 * @title UnergyData.
 * @notice This contract serves as the protocol storage, where the data needed
 * by the system to function is stored.
 */

contract UnergyData is
    Initializable,
    PausableUpgradeable,
    CommonUpgradeable,
    UUPSUpgradeable
{
    address public projectsManagerAddress;
    address public unergyBuyerAddress;
    address public unergyLogicReserveAddress;
    address public unergyEventAddress;
    address public uWattAddress;
    address public cleanEnergyAssetsAddress;
    address public potentialOwner;
    uint256 public depreciationBalance;
    address public maintainerAddress;

    mapping(address => mapping(address => uint256)) public accEnergyByMeter;
    mapping(address => mapping(address => bool)) public isExternalHolderAddress;
    mapping(address => mapping(address => PurchaseTicket)) private buyTickets;
    mapping(address => uint256) public projectFundingValue;

    address public unergyEventV2Address;

    mapping(address => address) public assetManagerAddress;
    mapping(address => uint256) public assetManagerFeePercentage;

    uint256 public swapFeePercentage;
    address public stakingProtocolAddress;

    mapping(address => mapping(uint256 => uint256))
        public offChainMilestonePayment;
    mapping(address => mapping(uint256 => uint256))
        public offChainPaymentReport;

    event ProjectsManagerAdressUpdated(address projectsManagerAddress);
    event UnergyBuyerAddressUpdated(address unergyBuyerAddress);
    event UnergyLogicReserveAddressUpdated(address unergyLogicReserveAddress);
    event UnergyEventAddressUpdated(address unergyEventAddress);
    event UWattAddressUpdated(address uWattAddress);
    event CleanEnergyAssetAddressUpdated(address cleanEnergyAssetsAddress);
    event MaintainerAddressUpdated(address maintainerAddress);
    event CleanEnergyAssetsAddressUpdated(address cleanEnergyAssetsAddress);
    event OwnerNominated(address pendingOwner);

    error NotPotentialOwner(address potentialOwner);
    error AssetManagerFeePercentageExceeded(uint256 assetManagerFeePercentage);
    error swapFeePercentageExceeded(uint256 swapFeePercentage);

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }

    function initialize(
        address _maintenanceAddr,
        address _permissionGranterAddr
    )
        public
        notZeroAddress(_maintenanceAddr)
        notZeroAddress(_permissionGranterAddr)
        initializer
    {
        __Pausable_init();
        __Common_init(_permissionGranterAddr);
        __UUPSUpgradeable_init();

        maintainerAddress = _maintenanceAddr;
    }

    function _authorizeUpgrade(
        address newImplementation
    ) internal override onlyOwner {}

    function setDepreciationBalance(
        uint256 _depreciationBalance
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setDepreciationBalance"
        )
    {
        depreciationBalance = _depreciationBalance;
    }

    function setAccEnergyByMeter(
        address projectAddr,
        address meterAddr,
        uint256 accEnergy
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setAccEnergyByMeter"
        )
    {
        accEnergyByMeter[projectAddr][meterAddr] = accEnergy;
    }

    function setPresentProjectFundingValue(
        address projectAddr,
        uint256 newProjectFundingValue
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setPresentProjectFundingValue"
        )
    {
        projectFundingValue[projectAddr] = newProjectFundingValue;
    }

    /// @param projectAddr The address of the project to buy pWatts from
    /// @param pWattPrice The price of the pWatt
    /// @param expiration The amount of time the ticket is valid
    /// @param user The user that will buy the pWatts
    function generatePurchaseTicket(
        address projectAddr,
        uint256 pWattPrice,
        uint256 expiration,
        address user
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "generatePurchaseTicket"
        )
    {
        PurchaseTicket memory buyTicket = PurchaseTicket(
            projectAddr,
            pWattPrice,
            block.timestamp + expiration,
            user,
            false //will be set to true when the ticket has been used
        );

        buyTickets[projectAddr][user] = buyTicket;
    }

    function redeemTicket(
        address projectAddr,
        address user
    ) external view whenNotPaused returns (PurchaseTicket memory) {
        return buyTickets[projectAddr][user];
    }

    function changePurchaseTicketUsed(
        address projectAddr,
        address user
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "changePurchaseTicketUsed"
        )
    {
        PurchaseTicket memory buyTicket = buyTickets[projectAddr][user];
        buyTicket.used = true;
        buyTickets[projectAddr][user] = buyTicket;
    }

    function setExternalHolderAddress(
        address userAddress,
        address projectAddr,
        bool isExternalHolder
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setExternalHolderAddress"
        )
    {
        isExternalHolderAddress[userAddress][projectAddr] = isExternalHolder;
    }

    function setAssetManagerAddress(
        address projectAddress,
        address newAssetManagerAddress
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setAssetManagerAddress"
        )
    {
        assetManagerAddress[projectAddress] = newAssetManagerAddress;
    }

    function setAssetManagerFeePercentage(
        address projectAddress,
        uint256 newassetManagerFeePercentage
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setAssetManagerFeePercentage"
        )
    {
        if (newassetManagerFeePercentage > 15e18) {
            revert AssetManagerFeePercentageExceeded(
                newassetManagerFeePercentage
            );
        }
        assetManagerFeePercentage[
            projectAddress
        ] = newassetManagerFeePercentage;
    }

    function setSwapFeePercentage(
        uint256 newSwapFeePercentage
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setSwapFeePercentage"
        )
    {
        if (newSwapFeePercentage > 5e18) {
            revert swapFeePercentageExceeded(newSwapFeePercentage);
        }
        swapFeePercentage = newSwapFeePercentage;
    }

    function setOffChainMilestonePayment(
        address _projectAddress,
        uint256 _milestoneIndex,
        uint256 _amountPaid
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setOffChainMilestonePayment"
        )
    {
        offChainMilestonePayment[_projectAddress][
            _milestoneIndex
        ] += _amountPaid;
    }

    function setOffChainPaymentReport(
        address _projectAddress,
        uint256 _milestoneIndex,
        uint256 _amountPaid
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setOffChainPaymentReport"
        )
    {
        offChainPaymentReport[_projectAddress][_milestoneIndex] = _amountPaid;
    }

    function setStakingProtocolAddress(
        address newStakingProtocolAddress
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setStakingProtocolAddress"
        )
    {
        stakingProtocolAddress = newStakingProtocolAddress;
    }

    function setProjectsManagerAddr(
        address projectsManagerAddr
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setProjectsManagerAddr"
        )
        notZeroAddress(projectsManagerAddr)
    {
        projectsManagerAddress = projectsManagerAddr;
        emit ProjectsManagerAdressUpdated(projectsManagerAddress);
    }

    function setUnergyBuyerAddr(
        address unergyBuyerAddr
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setUnergyBuyerAddr"
        )
        notZeroAddress(unergyBuyerAddr)
    {
        unergyBuyerAddress = unergyBuyerAddr;
        emit UnergyBuyerAddressUpdated(unergyBuyerAddress);
    }

    function setUnergyLogicReserveAddr(
        address unergyLogicReserveAddr
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setUnergyLogicReserveAddr"
        )
        notZeroAddress(unergyLogicReserveAddr)
    {
        unergyLogicReserveAddress = unergyLogicReserveAddr;
        emit UnergyLogicReserveAddressUpdated(unergyLogicReserveAddress);
    }

    function setUnergyEventAddr(
        address unergyEventAddr,
        UnergyEventVersion version
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setUnergyEventAddr"
        )
        notZeroAddress(unergyEventAddr)
    {
        if (version == UnergyEventVersion.V1) {
            unergyEventAddress = unergyEventAddr;
        } else if (version == UnergyEventVersion.V2) {
            unergyEventV2Address = unergyEventAddr;
        } else {
            revert("Invalid version");
        }

        emit UnergyEventAddressUpdated(unergyEventAddress);
    }

    function setUWattAddr(
        address uWattAddr
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "setUWattAddr")
        notZeroAddress(uWattAddr)
    {
        uWattAddress = uWattAddr;
        emit UWattAddressUpdated(uWattAddr);
    }

    function setCleanEnergyAssetsAddr(
        address cleanEnergyAssetsAddr
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setCleanEnergyAssetsAddr"
        )
        notZeroAddress(cleanEnergyAssetsAddr)
    {
        cleanEnergyAssetsAddress = cleanEnergyAssetsAddr;
        emit CleanEnergyAssetAddressUpdated(cleanEnergyAssetsAddress);
    }

    function setMaintainerAddr(
        address maintainerAddr
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setMaintainerAddr"
        )
        notZeroAddress(maintainerAddr)
    {
        maintainerAddress = maintainerAddr;
        emit MaintainerAddressUpdated(maintainerAddr);
    }

    function getPresentProjectFundingValue(
        address projectAddr
    ) external view returns (uint256) {
        return (projectFundingValue[projectAddr]);
    }

    function getAccEnergyByMeter(
        address projectAddr,
        address meterAddr
    ) external view returns (uint256) {
        return (accEnergyByMeter[projectAddr][meterAddr]);
    }

    function transferOwnership(
        address _pendingOwner
    ) public override onlyOwner whenNotPaused notZeroAddress(_pendingOwner) {
        potentialOwner = _pendingOwner;
        emit OwnerNominated(_pendingOwner);
    }

    function acceptOwnership() external {
        if (msg.sender == potentialOwner) {
            _transferOwnership(msg.sender);
            potentialOwner = address(0);
        } else {
            revert NotPotentialOwner(msg.sender);
        }
    }

    function pause() external onlyOwner whenNotPaused {
        _pause();
    }

    function unpause() external onlyOwner whenPaused {
        _unpause();
    }
}
