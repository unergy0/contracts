// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Votes.sol";

import {UnergyEventAbs} from "./Abstracts.sol";
import "./UnergyData.sol";
import "./Common.sol";

/**
 * @title ERC20UWatt.
 * @notice This contract handles the logic for our stable currency collateralized in clean energy.
 */

contract ERC20UWatt is ERC20, ERC20Votes, Common {
    UnergyData private unergyData;
    address public potentialOwner;

    event UnergyEventAddressUpdated(address unergyEventAddr);
    event OwnerNominated(address pendingOwner);

    error TransferToHimself(address origin, address destinationAddress);

    constructor(
        address unergyDataAddress_,
        address permissionGranterAddres_
    )
        ERC20("Unergy Watt", "uWatt")
        ERC20Permit("Unergy Watt")
        Ownable()
        Common(permissionGranterAddres_)
    {
        unergyData = UnergyData(unergyDataAddress_);
    }

    function mint(
        address account,
        uint256 amount
    ) external hasRoleInPermissionGranter(_msgSender(), address(this), "mint") {
        _mint(account, amount);
    }

    function transfer(
        address to,
        uint256 amount
    ) public virtual override returns (bool) {
        address sender = _msgSender();
        if (sender == to) {
            revert TransferToHimself(sender, to);
        }
        _transfer(sender, to, amount);
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) public virtual override returns (bool) {
        address spender = _msgSender();
        if (from == to) {
            revert TransferToHimself(from, to);
        }
        _spendAllowance(from, spender, amount);
        _transfer(from, to, amount);
        return true;
    }

    function burn(uint256 amount) external {
        _burn(_msgSender(), amount);
    }

    function burnFrom(address account, uint256 amount) external {
        _spendAllowance(account, _msgSender(), amount);
        _burn(account, amount);
    }

    function decimals() public pure override(ERC20) returns (uint8) {
        return 18;
    }

    function _beforeTokenTransfer(
        address _from,
        address _to,
        uint256 _amount
    ) internal virtual override {
        super._beforeTokenTransfer(_from, _to, _amount);

        UnergyEventAbs(unergyData.unergyEventAddress()).beforeTransferReceipt(
            address(this),
            _from,
            _to,
            _amount
        );
    }

    function transferOwnership(
        address _pendingOwner
    ) public override onlyOwner notZeroAddress(_pendingOwner) {
        potentialOwner = _pendingOwner;
        emit OwnerNominated(_pendingOwner);
    }

    function acceptOwnership() external {
        require(
            msg.sender == potentialOwner,
            "You must be nominated as potential owner before you can accept ownership"
        );
        _transferOwnership(msg.sender);
        potentialOwner = address(0);
    }

    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal override(ERC20, ERC20Votes) {
        super._afterTokenTransfer(from, to, amount);
    }

    function _mint(
        address to,
        uint256 amount
    ) internal override(ERC20, ERC20Votes) {
        super._mint(to, amount);
    }

    function _burn(
        address account,
        uint256 amount
    ) internal override(ERC20, ERC20Votes) {
        super._burn(account, amount);
    }
}
