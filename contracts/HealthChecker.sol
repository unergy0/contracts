// SPDX-License-Identifier: MIT
pragma solidity 0.8.18;

import "@openzeppelin/contracts/access/AccessControl.sol";

contract HealthChecker is AccessControl {
    bytes32 public constant HEALTH_CHECKER_ROLE =
        keccak256("HEALTH_CHECKER_ROLE");

    event HealthCheck();

    constructor(address initialHealthChecker) {
        _grantRole(HEALTH_CHECKER_ROLE, initialHealthChecker);
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    function healthCheck() external onlyRole(HEALTH_CHECKER_ROLE) {
        emit HealthCheck();
    }
}
