// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";

import {PermissionType, Permission} from "./Types.sol";

/**
 * @title PermissionGranter v4.
 * @notice This contract handles the permissions of the contracts that are used in the Unergy Protocol.
 */

contract PermissionGranter is
    Initializable,
    AccessControlUpgradeable,
    UUPSUpgradeable
{
    bytes32 public constant PROTOCOL_CONTRACT_ROLE =
        keccak256("PROTOCOL_CONTRACT_ROLE");

    mapping(address => mapping(address => mapping(bytes32 => Permission)))
        private permissions;
    mapping(bytes32 => bool) private meterPermissions;

    event PermissionGranted(
        address indexed _address,
        address indexed _contract,
        string _fname
    );
    event PermissionRevoked(
        address indexed _address,
        address indexed _contract,
        string _fname
    );

    error NotAuthorizedToCallFunction(string functionName, address caller);
    error ZeroAddressNotAllowed();
    error ArraysLengthMismatch();

    modifier notZeroAddress(address _address) {
        _notZeroAddress(_address);
        _;
    }

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }

    function initialize(
        address _admin
    ) external notZeroAddress(_admin) initializer {
        __UUPSUpgradeable_init();
        __AccessControl_init();

        _grantRole(DEFAULT_ADMIN_ROLE, _admin);
    }

    function _authorizeUpgrade(
        address newImplementation
    ) internal override onlyRole(DEFAULT_ADMIN_ROLE) {}

    /// @notice This function is used to allow a `caller` to execute a function
    /// @param _address The address of the caller
    /// @param _contract The contract over the caller is allowed to execute the function
    /// @param _fname The name of the function the caller is allowed to execute
    function setPermission(
        address _address,
        address _contract,
        string calldata _fname,
        PermissionType _permissionType,
        uint256 _permissionValue
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _setPermission(
            _address,
            _contract,
            _fname,
            _permissionType,
            _permissionValue
        );
    }

    function setPermissionsBatch(
        address[] calldata _addresses,
        address[] calldata _contracts,
        string[] calldata _fnames,
        PermissionType[] calldata _permissionTypes,
        uint256[] calldata _permissionValues
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        if (
            _addresses.length != _contracts.length ||
            _contracts.length != _fnames.length ||
            _fnames.length != _permissionTypes.length ||
            _permissionTypes.length != _permissionValues.length
        ) revert ArraysLengthMismatch();

        for (uint256 i = 0; i < _addresses.length; i++) {
            _setPermission(
                _addresses[i],
                _contracts[i],
                _fnames[i],
                _permissionTypes[i],
                _permissionValues[i]
            );
        }
    }

    function _setPermission(
        address _address,
        address _contract,
        string memory _fname,
        PermissionType _permissionType,
        uint256 _permissionValue
    ) internal {
        bytes32 role = keccak256(abi.encodePacked(_contract, _fname));
        permissions[_address][_contract][role] = Permission({
            _type: _permissionType,
            _value: _permissionValue,
            _timesUsed: 0
        });

        emit PermissionGranted(_address, _contract, _fname);
    }

    /**
     * @notice This function is used to allow a meter account to execute the energyReport function of the logic contract
     */
    function setMeterPermission(
        address _meterAddress,
        address _logicContract,
        address _projectContract
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _setMeterPermission(_meterAddress, _logicContract, _projectContract);
    }

    function setMeterPermissionsBatch(
        address[] calldata _meterAddresses,
        address _logicContract,
        address[] calldata _projectContracts
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        if (_meterAddresses.length != _projectContracts.length)
            revert ArraysLengthMismatch();

        for (uint256 i = 0; i < _meterAddresses.length; i++) {
            _setMeterPermission(
                _meterAddresses[i],
                _logicContract,
                _projectContracts[i]
            );
        }
    }

    function _setMeterPermission(
        address _meterAddress,
        address _logicContract,
        address _projectContract
    ) internal {
        bytes32 permissionHash = keccak256(
            abi.encodePacked(_meterAddress, _logicContract, _projectContract)
        );

        meterPermissions[permissionHash] = true;
    }

    function revokeMeterPermission(
        address _meterAddress,
        address _logicContract,
        address _projectContract
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        bytes32 permissionHash = keccak256(
            abi.encodePacked(_meterAddress, _logicContract, _projectContract)
        );

        meterPermissions[permissionHash] = false;
    }

    /**
     * @dev This function does not relies on the `getAndUpdatePermission` because the permission is always permanent
     */
    function getMeterPermission(
        address _meterAddress,
        address _logicContract,
        address _projectContract
    ) external view returns (bool) {
        bytes32 permissionHash = keccak256(
            abi.encodePacked(_meterAddress, _logicContract, _projectContract)
        );

        return meterPermissions[permissionHash];
    }

    /// @notice This function is used to revoke the permission of a `caller` to execute a function
    /// @param _address { see: this.setPermission }
    /// @param _contract { see: this.setPermission }
    /// @param _fname { see: this.setPermission }
    function revokePermission(
        address _address,
        address _contract,
        string memory _fname,
        uint256 _permissionValue
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        bytes32 role = keccak256(abi.encodePacked(_contract, _fname));

        permissions[_address][_contract][role] = Permission(
            PermissionType.UNAUTHORIZED,
            _permissionValue,
            0 // Times used
        );

        emit PermissionRevoked(_address, _contract, _fname);
    }

    /// @notice The functions returns a boolean indicating if a `caller` is allowed to execute a function
    /// @param _address { see: this.setPermission }
    /// @param _contract { see: this.setPermission }
    /// @param _fname { see: this.setPermission }
    function getPermission(
        address _address,
        address _contract,
        string calldata _fname
    ) public view returns (bool) {
        bytes32 role = keccak256(abi.encodePacked(_contract, _fname));
        Permission memory permission = permissions[_address][_contract][role];

        if (permission._type == PermissionType.UNAUTHORIZED) return false;

        if (permission._type == PermissionType.PERMANENT) return true;

        if (
            permission._type == PermissionType.TIMELOCK &&
            block.timestamp < permission._value
        ) return true;

        if (
            permission._type == PermissionType.ONETIME &&
            permission._timesUsed < 1
        ) return true;

        if (
            permission._type == PermissionType.EXECUTIONS &&
            permission._timesUsed < permission._value
        ) return true;

        return false;
    }

    function getAndUpdatePermission(
        address _address,
        address _contract,
        string memory _fname
    ) external onlyRole(PROTOCOL_CONTRACT_ROLE) returns (bool) {
        bytes32 role = keccak256(abi.encodePacked(_contract, _fname));

        Permission storage permission = permissions[_address][_contract][role];

        if (permission._type == PermissionType.UNAUTHORIZED) return false;

        if (permission._type == PermissionType.PERMANENT) return true;

        if (
            permission._type == PermissionType.TIMELOCK &&
            block.timestamp < permission._value
        ) return true;

        if (
            permission._type == PermissionType.ONETIME &&
            permission._timesUsed < 1
        ) {
            permission._timesUsed++;
            return true;
        }

        if (
            permission._type == PermissionType.EXECUTIONS &&
            permission._timesUsed < permission._value
        ) {
            permission._timesUsed++;
            return true;
        }

        return false;
    }

    function _notZeroAddress(address _address) internal pure {
        if (_address == address(0)) revert ZeroAddressNotAllowed();
    }
}
