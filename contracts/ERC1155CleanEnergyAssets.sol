// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/security/Pausable.sol";

import "./Common.sol";

/**
 * @title CleanEnergyAssets.
 * @notice This contract handles the logic to keep track of energy generation
 * and generate Renewable Energy Certificates (RECs).
 */

contract CleanEnergyAssets is ERC1155, Pausable, Common {
    uint256 public tokenId;
    /// @dev is the energy limit to create a REC (Renewable Energy Certificate)
    uint256 public constant recDecimals = 18;
    uint256 public constant energyDecimals = 18;
    uint256 public energyLimit = 1 * 10 ** energyDecimals;

    mapping(uint256 => string) public tokenNameByTokenID;
    mapping(address => uint256) public tokenIdByProjectAddress;
    mapping(uint256 => bool) public isREC;
    mapping(address => uint256) public mintedEnergyByProject;
    mapping(address => uint256) public RECIdByAddress;
    mapping(address => uint256) public burnedAssets;

    event URIUpdated(string newURI);

    error ZeroOrNegativeAmount();
    error NotREC(uint256 tokenId);
    error TokenNotCreatedYet(address projectAddress);

    constructor(
        address _permissionGranterAddr
    ) ERC1155("") Common(_permissionGranterAddr) {
        _setApprovalForAll(address(this), msg.sender, true);
        _createGeneralEnergyAsset("Energy");
    }

    function _createGeneralEnergyAsset(string memory _tokenName) internal {
        tokenNameByTokenID[tokenId] = _tokenName;
        tokenId += 1;
    }

    /**
     * @notice This function creates the project energy asset and their respective REC.
     * @dev This function will be called when a new project is created.
     * @param _tokenName Name of the general clean energy asset.
     * @param _projectAddr New Project Address
     */
    function createProjectEnergyAsset(
        string calldata _tokenName,
        address _projectAddr
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "createProjectEnergyAsset"
        )
    {
        tokenNameByTokenID[tokenId] = _tokenName;
        tokenIdByProjectAddress[_projectAddr] = tokenId;
        tokenId += 1;
        //Project REC's
        tokenNameByTokenID[tokenId] = string.concat("REC ", _tokenName);
        isREC[tokenId] = true;
        RECIdByAddress[_projectAddr] = tokenId;
        tokenId += 1;
    }

    /**
     * @notice This function creates energy assets of a project and
     * if the accumulated energy exceeds the limit, new RECs are created.
     * @dev This function will be called from a new energy Report.
     * @param _projectAddr New Project Address
     * @param _amount Name of the general clean energy asset.
     */
    function mint(
        address _projectAddr,
        uint256 _amount
    )
        public
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "mint")
    {
        if (_amount <= 0) revert ZeroOrNegativeAmount();

        uint256 id = tokenIdByProjectAddress[_projectAddr];

        if (id == 0) revert TokenNotCreatedYet(_projectAddr);

        _mint(address(this), id, _amount, ""); //Mint project energy token
        _mint(address(this), 0, _amount, ""); //Mint general energy token

        mintedEnergyByProject[_projectAddr] += _amount;

        if (mintedEnergyByProject[_projectAddr] >= energyLimit) {
            uint256 recId = RECIdByAddress[_projectAddr];

            uint256 amountToMint = mintedEnergyByProject[_projectAddr] /
                energyLimit;

            _mint(address(this), recId, amountToMint * 10 ** recDecimals, "");
            mintedEnergyByProject[_projectAddr] -= energyLimit * amountToMint;
        }
    }

    function burn(
        address _projectAddr,
        uint256 _amount
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "burn")
    {
        if (_amount <= 0) revert ZeroOrNegativeAmount();

        uint256 id = tokenIdByProjectAddress[_projectAddr];

        _burn(address(this), id, _amount); //Burn project energy token
        _burn(address(this), 0, _amount); //Burn general energy token
    }

    function burnREC(uint256 _tokenId, uint256 amount) public whenNotPaused {
        if (!isREC[_tokenId]) revert NotREC(_tokenId);

        _burn(msg.sender, _tokenId, amount);
    }

    function withdrawRECs(
        uint256 _tokenId,
        address _receiver,
        uint256 _amount
    ) external whenNotPaused onlyOwner {
        if (!isREC[_tokenId]) revert NotREC(_tokenId);

        safeTransferFrom(address(this), _receiver, _tokenId, _amount, "");
    }

    function setURI(string calldata _newUri) public whenNotPaused onlyOwner {
        _setURI(_newUri);
        emit URIUpdated(_newUri);
    }

    function setEnergyLimit(
        uint256 _energyLimit
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "setEnergyLimit")
    {
        if (_energyLimit <= 0) revert ZeroOrNegativeAmount();

        energyLimit = _energyLimit;
    }

    function onERC1155Received(
        address,
        address,
        uint256,
        uint256,
        bytes memory
    ) external virtual returns (bytes4) {
        return this.onERC1155Received.selector;
    }

    function onERC1155BatchReceived(
        address,
        address,
        uint256[] memory,
        uint256[] memory,
        bytes memory
    ) external virtual returns (bytes4) {
        return this.onERC1155BatchReceived.selector;
    }

    function pause() external onlyOwner {
        _pause();
    }

    function unpause() external onlyOwner {
        _unpause();
    }

    function _afterTokenTransfer(
        address /* operator */,
        address from,
        address to,
        uint256[] memory ids, //transferred token ids array
        uint256[] memory amounts,
        bytes memory /* data */
    ) internal virtual override {
        uint256 id = ids[0]; //the burn function only sends an unique token id to this function

        if (to == address(0)) {
            if (id != 0) {
                uint256 amount = amounts[0];
                burnedAssets[from] += amount;
            }
        }
    }

    function _beforeTokenTransfer(
        address operator,
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) internal override whenNotPaused {}
}
