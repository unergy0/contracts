// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/MathUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";

import {ERC20Abs} from "./Abstracts.sol";
import {Milestone, Signature, Project, ProjectState} from "./Types.sol";
import "./ProjectsManager.sol";
import "./CommonUpgradeable.sol";
import "./UnergyData.sol";
import {ERC20UWatt} from "./ERC20UWatt.sol";

/**
 * @title UnergyBuyer.
 * @notice This contract handles interactions with installer users, manages their payments,
 * and also holds pWatt to generate profits for investors.
 */

contract UnergyBuyer is
    Initializable,
    PausableUpgradeable,
    CommonUpgradeable,
    UUPSUpgradeable
{
    using SafeERC20Upgradeable for IERC20Upgradeable;
    UnergyData public unergyData;
    address public potentialOwner;
    bool public isRefunding;

    event HolderUpdated(address projectAddr, address holder);
    event OwnerNominated(address pendingOwner);
    event StableCoinWithdraw(
        address projectAddr,
        address reciver,
        uint256 amount
    );
    event Refund(
        address indexed projectAddr,
        address indexed recipient,
        uint256 amount
    );
    event UnconventionalIncomeReported(
        address projectAddress,
        uint256 amount,
        string description
    );

    error ProjectIsNotCancelled(address projectAddr);
    error UserDoesntHavePWatts(address projectAddr, address user);
    error InvalidIndex(uint256 maxValid, uint256 index);
    error RefundInProcess();
    error EmptyString();
    error ZeroValueNotAllowed();
    error OriginatorCannotMakeRefund(address _user);
    error CanNotCallPayUWattsReward(address caller);
    error WeightsShouldBeEqual100(uint256 accumulatedWeight);
    error ProjectIsNotFullySigned();
    error MilestoneNotSignedByInstaller(uint256 _milestoneIndex);
    error NotPotentialOwner(address potentialOwner);
    error OffChainPaymentNotConfirmed();
    error ConfirmationAndAcknowledgeMismatch();
    error NotEnoughBalanceForMakePayment(
        uint256 presentProjectFundingValue,
        uint256 paymentValue
    );
    error AmountExceedsRequired(
        uint256 rawPaymentValue,
        uint256 offChainMilestonePayment
    );

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }

    function initialize(
        address _unergyDataAddr,
        address _permissionGranterAddr
    )
        public
        notZeroAddress(_unergyDataAddr)
        notZeroAddress(_permissionGranterAddr)
        initializer
    {
        __Pausable_init();
        __Common_init(_permissionGranterAddr);
        __UUPSUpgradeable_init();

        unergyData = UnergyData(_unergyDataAddr);
    }

    function _authorizeUpgrade(
        address newImplementation
    ) internal override onlyOwner {}

    function setInstallerSign(
        address _projectAddr
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setInstallerSign"
        )
    {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Milestone[] memory milestones = ProjectsManager(projectsManagerAddress)
            .getProjectMilestones(_projectAddr);

        uint256 acummulatedWeight;

        for (uint256 i; i < milestones.length; i++) {
            acummulatedWeight += milestones[i].weight;
        }
        if (acummulatedWeight != 100) {
            revert WeightsShouldBeEqual100(acummulatedWeight);
        }

        uint256 finishedMilestones;
        for (uint256 i; i < milestones.length; i++) {
            if (milestones[i].isReached == false) {
                milestones[i].isReached = true;
                milestones[i].wasSignedByInstaller = true;

                ProjectsManager(projectsManagerAddress).updateMilestoneAtIndex(
                    _projectAddr,
                    i,
                    milestones[i]
                );

                break;
            } else {
                finishedMilestones += 1;
            }
        }

        if (finishedMilestones == milestones.length - 1) {
            ProjectsManager(projectsManagerAddress).setSignature(
                _projectAddr,
                Signature.INSTALLER
            );
            _setState(_projectAddr, ProjectState.INSTALLED);
        }
    }

    /**
     * @notice When the Originator signs it means that an off-chain payment has been made to the
     * installer for a project milestone. When the installer signs it means that they acknowledge
     * an off-chain payment for the given project milestone.
     * @param _projectAddr The project whose cost will be used to calculate the payment
     * @param _milestoneIndex the milestone referred to
     * @param _amount the off-chain amount paid
     */
    function offChainMilestonePaymentReport(
        address _projectAddr,
        uint256 _milestoneIndex,
        uint256 _amount
    ) external whenNotPaused {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Project memory project = ProjectsManager(projectsManagerAddress)
            .getProject(_projectAddr);

        if (
            msg.sender != project.originator &&
            msg.sender != project.installerAddr
        ) {
            revert PermissionGranter.NotAuthorizedToCallFunction(
                "offChainMilestonePaymentReport",
                msg.sender
            );
        }

        uint256 stableDecimals = (10 **
            ERC20Abs(project.stableAddr).decimals());
        Milestone[] memory milestones = ProjectsManager(projectsManagerAddress)
            .getProjectMilestones(_projectAddr);

        uint256 rawPaymentValue = MathUpgradeable.mulDiv(
            project.initialProjectValue * stableDecimals,
            milestones[_milestoneIndex].weight,
            100 * stableDecimals
        );

        uint256 accOffChainMilestonePayment = unergyData
            .offChainMilestonePayment(_projectAddr, _milestoneIndex);

        if (_amount > rawPaymentValue - accOffChainMilestonePayment) {
            revert AmountExceedsRequired(
                rawPaymentValue,
                _amount + accOffChainMilestonePayment
            );
        }

        if (msg.sender == project.originator) {
            return
                unergyData.setOffChainPaymentReport(
                    _projectAddr,
                    _milestoneIndex,
                    _amount
                );
        }

        if (msg.sender == project.installerAddr) {
            if (
                unergyData.offChainPaymentReport(
                    _projectAddr,
                    _milestoneIndex
                ) == 0
            ) {
                revert OffChainPaymentNotConfirmed();
            }

            if (
                unergyData.offChainPaymentReport(
                    _projectAddr,
                    _milestoneIndex
                ) == _amount
            ) {
                return
                    unergyData.setOffChainMilestonePayment(
                        _projectAddr,
                        _milestoneIndex,
                        _amount
                    );
            }
            revert ConfirmationAndAcknowledgeMismatch();
        }
    }

    /// @notice When the originator signs it means that the project milestone was validated by the originator
    /// @param _projectAddr The address of the project
    /// @param _milestoneIndex The milestone index to be signed
    function setOriginatorSign(
        address _projectAddr,
        uint256 _milestoneIndex
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setOriginatorSign"
        )
    {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Milestone[] memory milestones = ProjectsManager(projectsManagerAddress)
            .getProjectMilestones(_projectAddr);

        if (milestones.length <= _milestoneIndex) {
            revert InvalidIndex(milestones.length, _milestoneIndex);
        }
        if (!milestones[_milestoneIndex].wasSignedByInstaller) {
            revert MilestoneNotSignedByInstaller(_milestoneIndex);
        }

        if (milestones[_milestoneIndex].wasSignedByOriginator == false) {
            Project memory project = ProjectsManager(projectsManagerAddress)
                .getProject(_projectAddr);

            milestones[_milestoneIndex].wasSignedByOriginator = true;

            _installerPayment(
                project,
                milestones[_milestoneIndex],
                _milestoneIndex
            );

            ProjectsManager(projectsManagerAddress).updateMilestoneAtIndex(
                _projectAddr,
                _milestoneIndex,
                milestones[_milestoneIndex]
            );
        }

        if (_wasFullySigned(milestones))
            ProjectsManager(projectsManagerAddress).setSignature(
                _projectAddr,
                Signature.ORIGINATOR
            );
    }

    function wasFullySigned(address _projectAddr) external view returns (bool) {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Milestone[] memory milestones = ProjectsManager(projectsManagerAddress)
            .getProjectMilestones(_projectAddr);

        return _wasFullySigned(milestones);
    }

    function _wasFullySigned(
        Milestone[] memory milestones
    ) internal pure returns (bool) {
        for (uint256 i; i < milestones.length; i++) {
            if (
                !milestones[i].wasSignedByInstaller ||
                !milestones[i].wasSignedByOriginator
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * @notice Transfer funds to the installer based on the weight of each milestone and
     * project financing income.
     * @param project The project whose cost will be used to calculate the payment
     * @param milestone The milestone whose weight will be used to calculate the payment
     */
    function _installerPayment(
        Project memory project,
        Milestone memory milestone,
        uint256 _milestoneIndex
    ) internal {
        IERC20Upgradeable stableCoin = IERC20Upgradeable(project.stableAddr);

        uint256 presentProjectFundingValue = unergyData
            .getPresentProjectFundingValue(project.addr);
        uint256 stableDecimals = (10 **
            ERC20Abs(project.stableAddr).decimals());
        uint256 offChainMilestonePayment = unergyData.offChainMilestonePayment(
            project.addr,
            _milestoneIndex
        );

        uint256 rawPaymentValue = MathUpgradeable.mulDiv(
            project.initialProjectValue * stableDecimals,
            milestone.weight,
            100 * stableDecimals
        );

        uint256 paymentValue = rawPaymentValue - offChainMilestonePayment;

        if (presentProjectFundingValue < paymentValue) {
            revert NotEnoughBalanceForMakePayment(
                presentProjectFundingValue,
                paymentValue
            );
        }

        uint256 newPresentProjectFundingValue = presentProjectFundingValue -
            paymentValue;
        unergyData.setPresentProjectFundingValue(
            project.addr,
            newPresentProjectFundingValue
        );

        stableCoin.safeTransfer(project.installerAddr, paymentValue);
    }

    function setInitialProjectValue(
        address _projectAddr,
        uint256 _newInitialProjectValue
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setInitialProjectValue"
        )
    {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Project memory project = ProjectsManager(projectsManagerAddress)
            .getProject(_projectAddr);
        project.initialProjectValue = _newInitialProjectValue;

        ProjectsManager(projectsManagerAddress).updateProject(
            _projectAddr,
            project
        );
    }

    function setCurrentProjectValue(
        address _projectAddr,
        uint256 _projectValue
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setCurrentProjectValue"
        )
    {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Project memory project = ProjectsManager(projectsManagerAddress)
            .getProject(_projectAddr);
        project.currentProjectValue = _projectValue;

        ProjectsManager(projectsManagerAddress).updateProject(
            _projectAddr,
            project
        );
    }

    function setSwapFactor(
        address _projectAddr,
        uint256 _swapfactor
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "setSwapFactor")
    {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Project memory project = ProjectsManager(projectsManagerAddress)
            .getProject(_projectAddr);
        project.swapFactor = _swapfactor;

        ProjectsManager(projectsManagerAddress).updateProject(
            _projectAddr,
            project
        );
    }

    function setProjectState(
        address _projectAddr,
        ProjectState _state
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "setProjectState")
    {
        _setState(_projectAddr, _state);
    }

    function _setState(address _projectAddr, ProjectState _state) internal {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Project memory project = ProjectsManager(projectsManagerAddress)
            .getProject(_projectAddr);

        if (_state == ProjectState.PRODUCTION) {
            if (
                project.signatures.isSignedByInstaller == true &&
                project.signatures.isSignedByOriginator == true
            ) {
                ProjectsManager(projectsManagerAddress).setProjectState(
                    _projectAddr,
                    _state
                );
            } else {
                revert ProjectIsNotFullySigned();
            }
        } else {
            ProjectsManager(projectsManagerAddress).setProjectState(
                _projectAddr,
                _state
            );
        }
    }

    function payUWattReward(
        address _to,
        uint256 _amount
    )
        external
        whenNotPaused
        notZeroAddress(unergyData.unergyLogicReserveAddress())
    {
        if (msg.sender == unergyData.unergyLogicReserveAddress()) {
            IERC20Upgradeable(address(unergyData.uWattAddress())).safeTransfer(
                _to,
                _amount
            );
        } else {
            revert CanNotCallPayUWattsReward(msg.sender);
        }
    }

    /**
     * @notice This function is used to report unconventional incomes related to a project.
     * @param _projectAddress The address of the project
     * @param _amount The amount to report
     * @param _description This must be a short description of the reason for the report.
     */
    function reportUnconventionalIncome(
        address _projectAddress,
        uint256 _amount,
        string memory _description
    )
        external
        whenNotPaused
        notZeroAddress(_projectAddress)
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "reportUnconventionalIncome"
        )
    {
        if (bytes(_description).length == 0) revert EmptyString();
        if (_amount == 0) revert ZeroValueNotAllowed();

        uint256 presentProjectFundingValue = unergyData
            .getPresentProjectFundingValue(_projectAddress);

        IERC20Upgradeable stableCoin = IERC20Upgradeable(
            ProjectsManager(unergyData.projectsManagerAddress())
                .getProject(_projectAddress)
                .stableAddr
        );

        stableCoin.safeTransferFrom(msg.sender, address(this), _amount);

        unergyData.setPresentProjectFundingValue(
            _projectAddress,
            presentProjectFundingValue + _amount
        );

        emit UnconventionalIncomeReported(
            _projectAddress,
            _amount,
            _description
        );
    }

    function externalRefund(address _projectAddress) public whenNotPaused {
        _refund(_projectAddress, msg.sender);
    }

    function refund(
        address _projectAddress,
        address _user
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "refund")
    {
        _refund(_projectAddress, _user);
    }

    function _refund(address _projectAddress, address _user) internal {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Project memory project = ProjectsManager(projectsManagerAddress)
            .getProject(_projectAddress);

        if (_user == project.originator) {
            revert OriginatorCannotMakeRefund(_user);
        }

        if (project.state != ProjectState.INREFUND) {
            revert ProjectIsNotCancelled(_projectAddress);
        }

        uint256 userPWattBalance = IERC20Upgradeable(_projectAddress).balanceOf(
            _user
        );

        if (userPWattBalance == 0) {
            revert UserDoesntHavePWatts(_projectAddress, _user);
        }

        if (isRefunding) revert RefundInProcess();
        isRefunding = true;

        ERC20Abs(_projectAddress).burn(_user, userPWattBalance);

        uint256 presentProjectFundingValue = unergyData
            .getPresentProjectFundingValue(_projectAddress);
        uint256 projectDecimals = ERC20Abs(_projectAddress).decimals();

        uint256 initialTotalSupply = ProjectsManager(projectsManagerAddress)
            .initialTotalSupply(_projectAddress);

        uint256 adminBalance = ERC20Abs(_projectAddress).balanceOf(
            project.adminAddr
        );

        uint256 pWattPercentage = MathUpgradeable.mulDiv(
            userPWattBalance,
            100 * (10 ** projectDecimals),
            initialTotalSupply - project.originatorFee - adminBalance
        );

        uint256 refundAmount = MathUpgradeable.mulDiv(
            pWattPercentage,
            presentProjectFundingValue,
            100 * (10 ** projectDecimals)
        );

        _withdrawStableCoin(_projectAddress, _user, refundAmount);

        isRefunding = false;

        emit Refund(_projectAddress, _user, refundAmount);
    }

    function withdrawUWatts()
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "withdrawUWatts")
    {
        IERC20Upgradeable(address(unergyData.uWattAddress())).safeTransfer(
            msg.sender,
            ERC20UWatt(unergyData.uWattAddress()).balanceOf(address(this))
        );
    }

    /**
     * @notice This function is used to withdraw remaining stable coins from
     *  the purchase of pWatts or to return stable coins from the purchase of
     *  pWatts due to project cancellation.
     */
    function withdrawStableCoin(
        address _projectAddress,
        address _receiver,
        uint256 _amount
    )
        public
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "withdrawStableCoin"
        )
    {
        _withdrawStableCoin(_projectAddress, _receiver, _amount);
    }

    function _withdrawStableCoin(
        address _projectAddress,
        address _receiver,
        uint256 _amount
    ) internal {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Project memory project = ProjectsManager(projectsManagerAddress)
            .getProject(_projectAddress);

        IERC20Upgradeable(project.stableAddr).safeTransfer(_receiver, _amount);

        emit StableCoinWithdraw(_projectAddress, _receiver, _amount);
    }

    function setUnergyDataAddr(
        address _unergyDataAddr
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setUneryDataAddress"
        )
        notZeroAddress(_unergyDataAddr)
    {
        unergyData = UnergyData(_unergyDataAddr);
    }

    function transferOwnership(
        address _pendingOwner
    ) public override onlyOwner whenNotPaused notZeroAddress(_pendingOwner) {
        potentialOwner = _pendingOwner;
        emit OwnerNominated(_pendingOwner);
    }

    function acceptOwnership() external {
        if (msg.sender == potentialOwner) {
            _transferOwnership(msg.sender);
            potentialOwner = address(0);
        } else {
            revert NotPotentialOwner(msg.sender);
        }
    }

    function pause() external whenNotPaused onlyOwner {
        _pause();
    }

    function unpause() external onlyOwner {
        _unpause();
    }
}
