// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {PermissionGranter} from "../PermissionGranter.sol";

/**
 * @title CommonAbstract
 * @author Unergy
 * @notice As we are mantaining two versions of the same contract, one upgradeable and one not,
 *         and difers only in the addition of a storage gap and the `initialize` function, we
 *         have created this abstract contract to ensure that both contracts have the same interface.
 */

abstract contract CommonAbstract {
    event SetPermissionGranter(address permissionGranterAddr);

    error ZeroAddressNotAllowed();

    modifier hasRoleInPermissionGranter(
        address _caller,
        address _contract,
        string memory _functionName
    ) {
        _hasRoleInPermissionGranter(_caller, _contract, _functionName);
        _;
    }

    modifier hasMeterPermissionOverProjectContract(
        address _caller,
        address _logicContract,
        address _projectContract
    ) {
        _hasMeterPermissionOverProjectContract(
            _caller,
            _logicContract,
            _projectContract
        );
        _;
    }

    function setPermissionGranterAddr(
        address _permissionGranterAddr
    ) public virtual;

    function getPermissionGranterAddress()
        public
        view
        virtual
        returns (address);

    function _checkPermission(
        bool _hasPermission,
        string memory _functionName,
        address _caller
    ) internal pure virtual;

    function _notZeroAddress(address _address) internal pure virtual;

    function _hasRoleInPermissionGranter(
        address _caller,
        address _contract,
        string memory _functionName
    ) internal virtual;

    function _hasMeterPermissionOverProjectContract(
        address _caller,
        address _logicContract,
        address _projectContract
    ) internal view virtual;
}
